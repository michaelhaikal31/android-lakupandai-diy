package com.lakupandai.acs.lakupandaidiy.transfer.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;

/**
 * Created by Erdy on 12/07/2018.
 */
public class MenuTransfer extends HomeActivity {
    private ImageView llHomeMenuTransfer, llTransferBankJatimMenuTransfer, llTransferBankLainMenuTransfer, llTransferRekeningBankLainMenuTransfer;
    private Context mContexts;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_transfer);
        initUI();
    }

    private void initUI() {
        mContexts = this;

        llHomeMenuTransfer = (ImageView) findViewById(R.id.llHomeMenuTransfer);
        llHomeMenuTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        llTransferBankJatimMenuTransfer = (ImageView) findViewById(R.id.llTransferBankJatimMenuTransfer);
        llTransferBankJatimMenuTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContexts, MenuTransferDetail.class);
                startActivity(i);
            }
        });

        llTransferBankLainMenuTransfer = (ImageView) findViewById(R.id.llTransferBankLainMenuTransfer);
        llTransferBankLainMenuTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContexts, MenuTransferLainDetail.class);
                startActivity(i);
            }
        });

//        llTransferRekeningBankLainMenuTransfer = findViewById(R.id.llTransferRekeningBankLainMenuTransfer);
//        llTransferRekeningBankLainMenuTransfer.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(mContexts, MenuTransferAntarBankDetail.class);
//                startActivity(i);
//                finish();
//            }
//        });
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
