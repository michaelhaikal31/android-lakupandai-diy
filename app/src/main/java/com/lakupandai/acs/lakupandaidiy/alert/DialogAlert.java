package com.lakupandai.acs.lakupandaidiy.alert;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.util.Constant;


/**
 * Created by ACS-Design on 8/16/2017.
 */
public class DialogAlert extends DialogFragment implements View.OnClickListener{
    TextView tvRM;
    Button btOK;

    public static DialogAlert createInstance(){

        return new DialogAlert();
    }

    public Activity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dlg = super.onCreateDialog(savedInstanceState);
        dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlg.setCanceledOnTouchOutside(false);
        return dlg;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.dialog_alert, container);

        view.findViewById(R.id.btOK).setOnClickListener(this);
        tvRM = view.findViewById(R.id.tvRM);
        tvRM.setText(Constant.ALERT_RM);

        return view;
    }

    @Override
    public void onClick(View view) {
//
        switch (view.getId()){
            case R.id.btOK :
                    dismissAllowingStateLoss();
                break;
            default:
                throw new IllegalArgumentException("Unrecognized id");
        }

    }
}
