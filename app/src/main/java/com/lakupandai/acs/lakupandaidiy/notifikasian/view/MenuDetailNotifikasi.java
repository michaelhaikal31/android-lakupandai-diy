package com.lakupandai.acs.lakupandaidiy.notifikasian.view;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.notifikasian.view.MenuNotifikasi;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;

import static com.lakupandai.acs.lakupandaidiy.notifikasian.view.MenuNotifikasi.img;
import static com.lakupandai.acs.lakupandaidiy.notifikasian.view.MenuNotifikasi.rrn;
import static com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction.ACTION_NOTIFIKASI;
import static com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction.URL;

/**
 * Created by Erdy on 12/05/2018.
 */
public class MenuDetailNotifikasi extends HomeActivity {
    private String noHPAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    private Activity activity;
    private ImageView llHomeMenuNotifikasi, ivResponse;
    private Button btDownload, btShare;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_detail_notifikasi);
        initUI();
    }

    private void initUI() {
        activity = this;
        ivResponse = (ImageView) findViewById(R.id.ivResponse);
        String urel = img;
        Picasso.with(getApplicationContext()).load(urel).into(ivResponse);

        btDownload = (Button) findViewById(R.id.btDownload);
        btDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDownload(img, rrn);
            }
        });

        btShare = (Button) findViewById(R.id.btShare);
        btShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
               // sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, MenuNotifikasi.title+"\n"+MenuNotifikasi.message+"\n"+ img);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, rmResult);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        llHomeMenuNotifikasi = (ImageView) findViewById(R.id.llHomeMenuNotifikasi);
        llHomeMenuNotifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void startDownload(String imageUrl, String nama) {
        String filename = nama+".jpg";
        String downloadUrlOfImage = imageUrl;
        File direct =
                new File(Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                        .getAbsolutePath() + "/");


        if (!direct.exists()) {
            direct.mkdir();
//            Log.d(LOG_TAG, "dir created for first time");
        }

        DownloadManager dm = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri downloadUri = Uri.parse(downloadUrlOfImage);
        DownloadManager.Request request = new DownloadManager.Request(downloadUri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(filename)
                .setMimeType("image/jpeg")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES,
                        File.separator + filename);
        dm.enqueue(request);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        new executeNotifikasi(MenuDetailNotifikasi.this).execute();
    }

    class executeNotifikasi extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeNotifikasi(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MenuDetailNotifikasi.this);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
//                String dateDefault = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                noHPAgen=Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN,MenuDetailNotifikasi.this);
//                json.put(Constant.START_DATE, dateDefault);
//                json.put(Constant.END_DATE, dateDefault);
                json.put(ConstantTransaction.PAGE_JSON, "0");
                json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                json.put(ConstantTransaction.ACTION, ACTION_NOTIFIKASI);
                System.out.println("json "+json.toString());
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                resultfromJson = jsonParser.HttpRequestPost(URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuDetailNotifikasi.this);
//                System.out.println("Result json: "+resultfromJson);
                resultfromJsonDecrypt = tripleDES.decrypt(resultfromJson);
//                System.out.println("RESPONSE DATA: " + resultfromJson);
//                System.out.println("RESPONSE DATA: " + resultfromJsonDecrypt);

                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;

            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(MenuDetailNotifikasi.this, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.SERVER_ERROR)) {
                Function.showAlert(MenuDetailNotifikasi.this, ConstantTransaction.SERVER_ERROR);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                //SendSms.splitSMS(resultEnc, mContext);
                Function.showAlert(MenuDetailNotifikasi.this, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Nilai Result JSONParser Decrypt : " +resultfromJsonDecrypt.replaceAll("\\\\",""));
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);

                    //lastLogin = jsonObject.getString(Constant.LAST_LOGIN);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        JSONObject jsonA = new JSONObject(rmResult);
                        try{
                            Constant.jsonBnotifikasi = jsonA.getJSONArray("notification");
                            Constant.sizePerPagenotifikasi = jsonA.getInt("sizePerPage");
                            Constant.totalPagesnotifikasi = jsonA.getInt("totalPages");
                            Constant.totalMutasinotifikasi = jsonA.getInt("totalNotification");
                            Constant.pagenotifikasi = jsonA.getInt("page");
                            Constant.rcnotifikasi = jsonA.getInt("rc");
                        }catch (Exception e){
                            Constant.jsonBnotifikasi = new JSONArray();
                            Constant.sizePerPagenotifikasi = 10;
                            Constant.totalPagesnotifikasi = 999999;
                            Constant.totalMutasinotifikasi = 999999;
                            Constant.pagenotifikasi = 0;
                            Constant.rcnotifikasi = 0;
                        }

//                        System.out.println("JSONArray : "+ jsonB.toString());
                        Intent i = new Intent(MenuDetailNotifikasi.this, MenuNotifikasi.class);
                        startActivity(i);
//                        finish();
                    }else {
                        Function.showAlert(MenuDetailNotifikasi.this, rmResult);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                    Function.showAlert(mContext, strResponse);
                    Function.showAlert(MenuDetailNotifikasi.this, e.toString());
                }
            }
        }

    }

}
