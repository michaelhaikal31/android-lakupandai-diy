package com.lakupandai.acs.lakupandaidiy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.saldo.model.Mutasi;

import java.util.ArrayList;

/**
 * Created by Erdy on 12/04/2018.
 */
public class MutasiAdapter extends ArrayAdapter<Mutasi> {

    private MutasiListener listener;
    private static class ViewHolder {
        TextView judul, date, action, saldo;
    }

    public MutasiAdapter(Context context, int simple_list_item_1, ArrayList<Mutasi> users,MutasiListener listener) {
        super(context, R.layout.adapter_mutasi, users);
        this.listener=listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Mutasi user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.adapter_mutasi, parent, false);
            viewHolder.judul = (TextView) convertView.findViewById(R.id.tvJudul);
            viewHolder.date = (TextView) convertView.findViewById(R.id.tvDate);
            viewHolder.action = (TextView) convertView.findViewById(R.id.tvAction);
            viewHolder.saldo = (TextView) convertView.findViewById(R.id.tvSaldo);
            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.judul.setText(user.judul);
        viewHolder.date.setText(user.date);
        viewHolder.action.setText(user.action);
        viewHolder.saldo.setText(user.saldo);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onMutasiClicked(user.resi);
            }
        });
        // Return the completed view to render on screen
        return convertView;
    }

    public interface MutasiListener{
        void onMutasiClicked(String id);
    }

}
