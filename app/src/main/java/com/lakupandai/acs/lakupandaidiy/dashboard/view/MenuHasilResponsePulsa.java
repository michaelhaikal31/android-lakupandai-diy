package com.lakupandai.acs.lakupandaidiy.dashboard.view;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.print.PrintHelper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.adapter.ResponAdapter;
import com.lakupandai.acs.lakupandaidiy.pembelian.view.MenuPembelian;
import com.lakupandai.acs.lakupandaidiy.setortunai.model.Respon;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.PrintPic;
import com.lakupandai.acs.lakupandaidiy.util.PrinterCommands;
import com.lakupandai.acs.lakupandaidiy.util.Utils;
import com.lakupandai.acs.lakupandaidiy.util.myWidget;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

public class MenuHasilResponsePulsa extends HomeActivity {
    private Button llHomeMenuHasilResponse, btShare;
    private myWidget listRespon;
    private Context mContext;
    private TextView txtJudulMenuHasilResponse, txtFooterMenuLayanan, txtJudulMenuHasilResponsePulsa, nameAgent;
    private String rmResult, titleMenu;
    private ImageView buttonBack, printstruct;
    private ArrayList<Respon> myListItems = new ArrayList<>();
    private ArrayList<Respon> myListItems2 = new ArrayList<>();
    private ResponAdapter adapter;
    private BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothSocket bluetoothSocket;
    private BluetoothDevice bluetoothDevice;
    private OutputStream outputStream;
    private InputStream inputStream;
    private Thread thread;
    private byte[] readBuffer;
    private int readBufferPosition;
    private volatile boolean stopWorker;
    private StringBuilder contentSbMain = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_hasil_response_pulsa);
        initUI();
    }

    private void initUI() {
        mContext = this;
        txtJudulMenuHasilResponse =  findViewById(R.id.txtJudulResponsePulsa);
        listRespon = findViewById(R.id.listViewPulsa);
        txtFooterMenuLayanan =  findViewById(R.id.txtFooterMenuLayanan);
        String footer= getString(R.string.footer_hasilResponseFooter);
        String footer2 = getString(R.string.footer_hasilResponseFooter2);
        txtFooterMenuLayanan.setText(footer+Constant.layanan_title+" "+footer2);
        nameAgent = findViewById(R.id.nameAgent);
        nameAgent.setText(Constant.usernameAgen);
        txtJudulMenuHasilResponsePulsa = findViewById(R.id.txtJudulMenuHasilResponsePulsa);
        txtJudulMenuHasilResponsePulsa.setText(Constant.layanan_title);
        buttonBack = findViewById(R.id.llHomeMenuKonfirmasipulsa);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Bundle bundle = getIntent().getExtras();
        rmResult = bundle.getString(ConstantTransaction.BUNDLE_RM);
        titleMenu = bundle.getString(ConstantTransaction.BUNDLE_TITLE);

        try {
//        JSONObject jsonObject = new JSONObject(rmResult);
//        JSONArray jsonArray = jsonObject.getJSONArray("data");
//            System.out.println("asd" + jsonObject);
            String periode[]=null;
            int sizeResult = Constant.dataResult.length();
            for (int a = 0; a < sizeResult; a++) {
                    if (a == 0) {
                        txtJudulMenuHasilResponse.setText(Constant.dataResult.getJSONObject(a).getString("value"));
                        contentSbMain.append(Constant.dataResult.getJSONObject(a).getString("value") + "\n");
                    } else {
                        myListItems.add(new Respon(Constant.dataResult.getJSONObject(a).getString("header"), Constant.dataResult.getJSONObject(a).getString("value")));
                        final int pos = a - 1;

                        if(myListItems.get(pos).getHeader().equals("Periode")) {
                            periode = myListItems.get(pos).getValue().split("\n");
                            myListItems.get(pos).setValue("");
                            for (int x = 0; x < periode.length; x++) {
                                final String[] value = periode[x].split("-");
                                final String date = value[0].substring(0, 4) + "-" + value[0].substring(4);
                                myListItems.add(new Respon(date, value[1].trim()));;
                            }

                        }

//
                    }
//                        contentSbMain.append(Constant.dataResult.getJSONObject(a).getString("header")+" : "+Constant.dataResult.getJSONObject(a).getString("value") + "\n");

            }
            String str ="";
            int MaxSpace = 14;
            myListItems2.addAll(myListItems);
            for(int a=0;a<myListItems2.size();a++){
                for (int i = 0; i <= MaxSpace; i++) {
                    if (i > myListItems2.get(a).getHeader().length()) {
                        if(myListItems2.get(a).getHeader().equals("Periode")){
                            str = String.format("%" + (i - myListItems2.get(a).getHeader().length()) + "s", " ");
                        }else {
                            if (myListItems2.get(a).getHeader().isEmpty()) {
                                str = String.format("%" + (i - myListItems2.get(a).getHeader().length()) + "s", " ");
                            } else {
                                str = String.format("%" + (i - myListItems2.get(a).getHeader().length()) + "s", ":");
                            }
                        }
                    }
                }
                if(myListItems2.get(a).getValue().length()>18){
                   myListItems2.add(a+1,new Respon("",myListItems2.get(a).getValue().substring(18)));
                   //a--;
                    contentSbMain.append(myListItems2.get(a).getHeader() + str + myListItems2.get(a).getValue().substring(0,18) + "\n");
                }else{
                    contentSbMain.append(myListItems2.get(a).getHeader() + str + myListItems2.get(a).getValue() + "\n");
                }


            }
        }catch (Exception e){
            System.out.println("asd exception [MenuHasilResponsePulsa Oncreate] "+e.getMessage());
        }
        String[] periode;

        adapter = new ResponAdapter(getApplicationContext(), myListItems);
        listRespon.setAdapter(adapter);
        llHomeMenuHasilResponse = (Button) findViewById(R.id.llHomeMenuHasilResponse);
        llHomeMenuHasilResponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuHasilResponsePulsa.this, MainMenu.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        btShare = (Button) findViewById(R.id.btShare);
        btShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               generateResponse();
            }
        });
//        if (!Constant.metode.equals("Debit")) {
//            printstruct = findViewById(R.id.printstruct);
//            printstruct.setVisibility(View.VISIBLE);
//            printstruct.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    PrintStruct();
//                }
//            });
//        }

        printstruct = findViewById(R.id.printstruct);
        printstruct.setVisibility(View.VISIBLE);
        printstruct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Function.showListenerFinishDialogMessage(MenuHasilResponsePulsa.this, getLayoutInflater(), "Pastikan anda mengaktifkan bluetooth smartphone anda lalu hubungkan dengan printer bluetooth di pengaturan smartphone anda", new Function.InputSenderDialogListener() {
                    @Override
                    public void onOK(String value) {
                        PrintStruct();
                    }

                    @Override
                    public void onCancel(String value) {

                    }
                });
            }
        });
        if(getIntent().getExtras().getBoolean("notif")){
            btShare.setVisibility(View.GONE);
            llHomeMenuHasilResponse.setVisibility(View.GONE);
        }
    }

    private void PrintStruct() {
        try {
            FindBluetoothDevice();
            openBluetoothPrinter();
            printData();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void generateResponse(){
        String result="";
        try{
            JSONArray jsonArray;
            if(getIntent().getExtras().getBoolean("notif")){
                jsonArray = new JSONObject(rmResult).getJSONArray("data");
            }else{
                jsonArray = Constant.dataResult;
            }
            System.out.println("asd jsonArray "+jsonArray);
            final int size = jsonArray.length();
            JSONObject jsonObject;
            for(int i=0;i<size;i++){
                jsonObject =jsonArray.getJSONObject(i);
                result = result + jsonObject.getString("header")+" : "+jsonObject.getString("value")+"\n";
            }
        }catch (Exception e){
            System.out.println("asd exception generateResponse [Menuhasilresponse] " +e.getMessage());
        }
        final Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, titleMenu);
        sharingIntent.putExtra(Intent.EXTRA_TEXT,result);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    void FindBluetoothDevice() {
        try {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (bluetoothAdapter == null) {
                Toast.makeText(getBaseContext(), "No Bluetooth Adapter found", Toast.LENGTH_SHORT).show();
            }
            if (bluetoothAdapter.isEnabled()) {
                Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBT, 0);
            }
            Set<BluetoothDevice> pairedDevice = bluetoothAdapter.getBondedDevices();
            if (pairedDevice.size() > 0) {
                for (BluetoothDevice pairedDev : pairedDevice) {
                    // My Bluetoth printer name is BTP_F09F1A
                    if (pairedDev.getName().equals("BlueTooth Printer")) {
                        bluetoothDevice = pairedDev;
                        Toast.makeText(getBaseContext(), "Bluetooth Printer Attached: " + pairedDev.getName(), Toast.LENGTH_SHORT).show();
                        break;
                    }else {
                        Toast.makeText(getBaseContext(), "Please Connect To Bluetooth Printer", Toast.LENGTH_SHORT).show();
                    }
                }
            }


        } catch (Exception ex) {
            Toast.makeText(getBaseContext(), "Couldn't connect to the printer", Toast.LENGTH_SHORT).show();
            ex.printStackTrace();
        }

    }

    void openBluetoothPrinter() throws IOException {
        try {

            //Standard uuid from string //
            UUID uuidSting = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(uuidSting);
            bluetoothSocket.connect();
            outputStream = bluetoothSocket.getOutputStream();
            inputStream = bluetoothSocket.getInputStream();

            beginListenData();

        } catch (Exception ex) {

        }
    }

    void beginListenData() {
        try {

            final Handler handler = new Handler();
            final byte delimiter = 10;
            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            thread = new Thread(new Runnable() {
                @Override
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                        try {
                            int byteAvailable = inputStream.available();
                            if (byteAvailable > 0) {
                                byte[] packetByte = new byte[byteAvailable];
                                inputStream.read(packetByte);

                                for (int i = 0; i < byteAvailable; i++) {
                                    byte b = packetByte[i];
                                    if (b == delimiter) {
                                        byte[] encodedByte = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedByte, 0,
                                                encodedByte.length
                                        );
                                        final String data = new String(encodedByte, "US-ASCII");
                                        readBufferPosition = 0;
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getBaseContext(), data, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            stopWorker = true;
                        }
                    }

                }
            });

            thread.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > ratioBitmap) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    void printData() throws IOException {
        try {
            Bitmap bmp1 = BitmapFactory.decodeResource(getResources(),
                    R.drawable.logolakupanda);
            Bitmap bmp = resize(bmp1,395,276);
            byte[] command = Utils.decodeBitmap(bmp);
            outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
            outputStream.write(command);

//            PrintHelper photoPrinter = new PrintHelper(this);
//            photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
//            Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
//                    R.drawable.logolakupanda);
//            byte[] command = Utils.decodeBitmap(bitmap);

            outputStream.write(PrinterCommands.ESC_HORIZONTAL_CENTERS );
            String msg =" "+"\n";
            outputStream.write(msg.getBytes());
            outputStream.write(PrinterCommands.ESC_ALIGN_LEFT );
            StringBuilder contentSbheader	= new StringBuilder();
            contentSbheader.append("Agen : "+Constant.usernameAgen + "\n");
            contentSbheader.append("ID Agen : "+Constant.idAgen + "\n");
            outputStream.write(contentSbheader.toString().getBytes());
            outputStream.write(PrinterCommands.FEED_LINE);

            outputStream.write(contentSbMain.toString().getBytes());
            outputStream.write(PrinterCommands.FEED_LINE);

            outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
            StringBuilder contentSbFooter = new StringBuilder();
            long date = System.currentTimeMillis();
            SimpleDateFormat mhformat = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");
            String strDate = mhformat.format(date);
            contentSbFooter.append(getString(R.string.footer_strcutResponsePembayaran)+strDate);
            outputStream.write(contentSbFooter.toString().getBytes());
            outputStream.write(PrinterCommands.FEED_LINE);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
