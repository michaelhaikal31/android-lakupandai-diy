package com.lakupandai.acs.lakupandaidiy.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.DecimalFormat;

/**
 * Created by Erdy on 12/04/2018.
 */

public class NumberTextWatcherForThousand implements TextWatcher {
    EditText editText;


    public NumberTextWatcherForThousand(EditText editText) {
        this.editText = editText;


    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    public static String formatAmount(double val) {
        String ret = "";
        DecimalFormat decFrm = new DecimalFormat();
        decFrm.applyPattern("##,###,###,###");
        ret = decFrm.format(val).replace(',', '.');
        return ret;
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

}
