package com.lakupandai.acs.lakupandaidiy.transfer.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponse;
import com.lakupandai.acs.lakupandaidiy.saldo.view.MenuCekStatusTerakhirResultTransfer;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.sms.SmsReceiver;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.SpinnerDialog;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Erdy on 12/07/2018.
 */
public class MenuRequestTransferKonfirmasi extends HomeActivity implements SmsReceiver.SmsListener {
    private Activity activity;
    private ImageView llBackMenuRequestTransferKonfirmasi, llHomeMenuRequestTransferKonfirmasi, llMenuRequestTransferKonfirmasiPopup;
    private EditText etPinAgenMenuRequestTransferKonfirmasi;
    private Button btnKirimMenuRequestTransferKonfirmasi;
    private String rmBundle, tokenBundle, pinAgen, noHPAgen, noHpNasabah, nominal, noRek, token, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    private TextView txtIsiMenuRequestTransferKonfirmasi;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();


    TextView txtValueNamaDari, txtTextValueNoHPDari, txtValueNamaKe, txtValueNoRekKe, txtValueJumlah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_request_transfer_konfirmasi);
        initUI();
    }

    private void initUI() {
        activity = this;

//        final IntentFilter filter = new IntentFilter();
//        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
//        filter.setPriority(1000);
//        receiver = new SmsReceiver(this);
//        registerReceiver(receiver, filter);

        txtValueNamaDari = findViewById(R.id.txtValueNamaDari);
        txtTextValueNoHPDari = findViewById(R.id.txtTextValueNoHPDari);
        txtValueNamaKe = findViewById(R.id.txtValueNamaKe);
        txtValueNoRekKe = findViewById(R.id.txtValueNoRekKe);
        txtValueJumlah = findViewById(R.id.txtValueJumlah);

        Bundle bundle = getIntent().getExtras();
        rmBundle = bundle.getString(ConstantTransaction.BUNDLE_RM);
        tokenBundle = bundle.getString(ConstantTransaction.BUNDLE_TOKEN);
//        txtIsiMenuRequestTransferKonfirmasi = (TextView) findViewById(R.id.txtIsiMenuRequestTransferKonfirmasi);
//        txtIsiMenuRequestTransferKonfirmasi.setText(rmBundle);


        setResponseValue(rmBundle);

        etPinAgenMenuRequestTransferKonfirmasi = (EditText) findViewById(R.id.etPinAgenMenuRequestTransferKonfirmasi);

        llHomeMenuRequestTransferKonfirmasi = (ImageView) findViewById(R.id.llHomeMenuRequestTransferKonfirmasi);
        llHomeMenuRequestTransferKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnKirimMenuRequestTransferKonfirmasi = (Button) findViewById(R.id.btnKirimMenuRequestTransferKonfirmasi);
        btnKirimMenuRequestTransferKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etPinAgenMenuRequestTransferKonfirmasi.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etPinAgenMenuRequestTransferKonfirmasi.getText().toString().length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else {
                    pinAgen = etPinAgenMenuRequestTransferKonfirmasi.getText().toString();
                    new executeRequestTransferKonfirmasi(activity).execute();
                }
            }
        });
    }

    private void setResponseValue(String rmBundle) {
        txtValueNamaDari = findViewById(R.id.txtValueNamaDari);
        txtTextValueNoHPDari = findViewById(R.id.txtTextValueNoHPDari);
        txtValueNamaKe = findViewById(R.id.txtValueNamaKe);
        txtValueNoRekKe = findViewById(R.id.txtValueNoRekKe);
        txtValueJumlah = findViewById(R.id.txtValueJumlah);

        try {
            JSONObject jsonRM = new JSONObject(rmBundle);
            JSONArray arrayData = jsonRM.getJSONArray("data");
            JSONArray jsonArray = new JSONArray(arrayData.toString());
            HashMap<String, String> list = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = jsonObject1.optString("header");
                String value2 = jsonObject1.optString("value");
                list.put(value1, value2);
            }

            String layanan = list.get("Layanan");
            String destinationAccount = list.get("No Rek. Penerima");
            String destinationName = list.get("Nama Penerima");
            String amount = list.get("Jumlah");
            String fromName = list.get("Nama Pengirim");
            String fromMsisdn = list.get("No HP Pengirim");


            txtValueNamaDari.setText(": " + fromName);
            txtValueNamaDari.setTypeface(null, Typeface.BOLD);
            txtTextValueNoHPDari.setText(": " + fromMsisdn);
            txtTextValueNoHPDari.setTypeface(null, Typeface.BOLD);
            txtValueNamaKe.setText(": " + destinationName);
            txtValueNamaKe.setTypeface(null, Typeface.BOLD);
            txtValueNoRekKe.setText(": " + destinationAccount);
            txtValueNoRekKe.setTypeface(null, Typeface.BOLD);
            txtValueJumlah.setText(": "+amount);
            txtValueJumlah.setTypeface(null, Typeface.BOLD);

        } catch (Exception e) {
            Function.showAlert(activity, e.getMessage());
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregisterReceiver(receiver);
    }

    @Override
    public void smsReceived(String msgFrom, String msgBody) {
//        System.out.println("From: " + msgFrom);
//        System.out.println("Body: " + msgBody);

//        handler.removeCallbacks(runnable);
//        spinnerDialog.dismissAllowingStateLoss();
//        String hasilRCSms = msgBody.substring(4, 6);
//        String hasilRMSms = msgBody.substring(7, msgBody.length());
//
//
//        if (hasilRCSms.equals(Constant.SUCCESS_CODE)) {
//            Intent i = new Intent(mContext, MenuHasilResponse.class);
//            Bundle bundle = new Bundle();
//            bundle.putString(Constant.BUNDLE_TITLE, Constant.BUNDLE_TITLE_TRANSFER);
//            bundle.putString(Constant.BUNDLE_RC, hasilRCSms);
//            bundle.putString(Constant.BUNDLE_RM, hasilRMSms);
//            i.putExtras(bundle);
//            startActivity(i);
//            finish();
//        } else {
//            Function.showAlert(mContext, hasilRMSms);
//        }
    }

    class executeRequestTransferKonfirmasi extends AsyncTask<String, JSONObject, String[]> {
        Context context;

        private executeRequestTransferKonfirmasi(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String[] doInBackground(String... params) {
            JSONObject json = new JSONObject();
            final String[] temp = new String[2];
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN_REQUEST_TRANSFER_KONFIRMASI, noHPAgen);
                json.put(ConstantTransaction.TOKEN_REQUEST_TRANSFER_KONFIRMASI, tokenBundle);
                json.put(ConstantTransaction.PIN_REQUEST_TRANSFER_KONFIRMASI, pinAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_REQUEST_TRANSFER_KONFIRMASI);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuRequestTransferKonfirmasi.this);
                System.out.println("Response Send " + resultEnc);
                System.out.println("Response Send Decrypt " + resultDec);
                temp[0] = resultfromJson;
                temp[1] = resultEnc;
                return temp;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return temp;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result[0].contains(ConstantTransaction.CONNECTION_LOST)) {

                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result[0].contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result[0]);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        DeviceSession.setTempRequestBody(getBaseContext(),result[1]);
                        jsonObject = new JSONObject(rmResult);
                        final JSONArray data = jsonObject.getJSONArray("data");
                        final int size = data.length();
                        for(int i=0;i<size;i++){
                            if(data.getJSONObject(i).getString("header").equals("Keterangan")){
                                Function.showListenerOkDialogMessage(MenuRequestTransferKonfirmasi.this, getLayoutInflater(), data.getJSONObject(i).getString("value"), new Function.InputSenderDialogListener() {
                                    @Override
                                    public void onOK(String value) {
                                        finish();
                                    }

                                    @Override
                                    public void onCancel(String value) {

                                    }
                                });
                            }
                        }

                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
