package com.lakupandai.acs.lakupandaidiy.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.lakupandai.acs.lakupandaidiy.util.Constant;

/**
 * Created by Erdy on 12/06/2018.
 */
public class SmsReceiver extends BroadcastReceiver {
    public SmsListener smsListener;

    public SmsReceiver() {

    }

    public SmsReceiver(SmsListener smsListener) {
        this.smsListener = smsListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        DataSMS(context, intent);
    }

    private void DataSMS(Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) ;
        {
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs = null;
            if (bundle != null) {
                try {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++) {
                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        String msgBody = currentMessage.getMessageBody();
                        String msgFrom = currentMessage.getOriginatingAddress();
                        System.out.println("Isi Message: " + msgBody);
                        System.out.println("Pengirim: " + msgFrom);
//                        System.out.println(msgFrom);
//                        System.out.println(msgBody);
//                        msgFrom = msgs[i].getOriginatingAddress();
//                        msgBody = msgs[i].getMessageBody();
                        // if (msgBody.contains(Constant.PARAMETER_SMS)) {
//                            if (msgBody.length() == 6) {
//                                smsListener.smsReceived(msgFrom, msgBody);
//                            } else if (msgBody.length() > 6) {
//                                smsListener.smsReceived(msgFrom, msgBody.substring(msgBody.length() - 6));
//                            } else {
//                                // whatever is appropriate in this case
//                                throw new IllegalArgumentException("word has less than 3 characters!");
//                            }
                        // smsListener.smsReceived(msgFrom, msgBody.substring(msgBody.length() - 6));
                        if (msgBody.contains(Constant.PARAMETER_SMS)) {
                            smsListener.smsReceived(msgFrom, msgBody);
                        }

                        //}

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public interface SmsListener {
        public void smsReceived(String msgFrom, String msgBody);
    }

}
