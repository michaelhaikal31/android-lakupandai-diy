package com.lakupandai.acs.lakupandaidiy.transfer.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.Toast;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.NumberTextWatcherForThousand;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Erdy on 12/07/2018.
 */
public class MenuRequestTransferAntarBank extends HomeActivity {
    private Activity activity;
    private ImageView llHome;
    private Spinner spSumber, spRekTujuan;
    private TableRow tblNoHpNasabah;
    private EditText etNoHPNasabah, etNoRekTujuan, etJumlah;
    private Button btKirim;
    private String current;
    private String noHPAgen, noHpNasabah, nominal, noRek, token, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    String[] bankCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_request_transfer_antar_bank);
        initUI();
    }

    private void initUI() {
        activity = this;
        tblNoHpNasabah = findViewById(R.id.tblNoHpNasabah);
        spSumber = findViewById(R.id.spSumber);
        String[] arrayB = new String[]{"Agen", "Nasabah"};
        ArrayAdapter<String> adapterB = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line, arrayB);
        spSumber.setAdapter(adapterB);
        spSumber.setClipToPadding(true);

        spSumber.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
                                       long arg3) {
                // TODO Auto-generated method stub
                if (spSumber.getSelectedItem().toString().equals("Agen")) {
                    tblNoHpNasabah.setVisibility(View.GONE);
                } else {
                    tblNoHpNasabah.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        spRekTujuan = findViewById(R.id.spRekTujuan);

        try {
            JSONObject jsonA = new JSONObject(MenuTransferAntarBankDetail.rmResult);

            JSONArray ArrayB = jsonA.getJSONArray("data");
            bankCode = new String[ArrayB.length()];
            String[] bankName = new String[ArrayB.length()];
            for (int i = 0; ArrayB.length() > i; i++) {
                JSONObject jsonC = new JSONObject(ArrayB.getString(i));
                bankCode[i] = jsonC.getString("bankCode");
                bankName[i] = jsonC.getString("bankName");

            }

            ArrayAdapter<String> adapterdenom = new ArrayAdapter<String>(activity,
                    android.R.layout.simple_dropdown_item_1line, bankName);
            spRekTujuan.setAdapter(adapterdenom);
            spRekTujuan.setClipToPadding(true);

            spRekTujuan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
                                           long arg3) {
                    // TODO Auto-generated method stub
                    //Toast.makeText(activity, bankCode[pos], Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                    // TODO Auto-generated method stub

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


        llHome = (ImageView) findViewById(R.id.llHome);
        llHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etJumlah = (EditText) findViewById(R.id.etJumlah);
        etJumlah.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (!s.toString().equals(current)) {
                        etJumlah.removeTextChangedListener(this);
                        String cleanString = s.toString().replaceAll("[.]", "");
                        String formatted = null;
                        if (!cleanString.isEmpty()) {
                            double parsed = Double.parseDouble(cleanString);

                            formatted = NumberTextWatcherForThousand.formatAmount(parsed);
                        }
                        current = formatted;
                        etJumlah.setText(formatted);
                        if (!cleanString.isEmpty()) {
                            etJumlah.setSelection(formatted.length());
                        }
                        etJumlah.addTextChangedListener(this);
                    }
                } catch (Exception e) {
                    Toast.makeText(activity, ConstantError.ALERT_NOMINAL_INPUT_FALSE, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btKirim = (Button) findViewById(R.id.btKirim);
        btKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etJumlah.getText().toString().isEmpty() || etNoHPNasabah.getText().toString().isEmpty() || etNoRekTujuan.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etNoHPNasabah.getText().toString().length() < 10) {
                    Function.showAlert(activity, ConstantError.ALERT_NUMBER_LESS_THAN_10);
                } else if (etNoRekTujuan.getText().toString().length() < 10) {
                    Function.showAlert(activity, ConstantError.ALERT_NO_REK_LESS_THAN_10);
                } else if (Integer.parseInt(Function.removePoint(etJumlah.getText().toString())) < 1) {
                    Function.showAlert(activity, ConstantError.ALERT_NOMINAL_LESS_THAN_1);
                } else {
                    nominal = Function.removePoint(etJumlah.getText().toString());
                    noHpNasabah = etNoHPNasabah.getText().toString();
                    noRek = etNoRekTujuan.getText().toString();
                    new executeRequestTransfer(activity).execute();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        // unregisterReceiver(receiver);
    }

    class executeRequestTransfer extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeRequestTransfer(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN_REQUEST_TRANSFER, noHPAgen);
                json.put(ConstantTransaction.MSISDN_NASABAH_REQUEST_TRANSFER, noHpNasabah);
                json.put(ConstantTransaction.NOMINAL_REQUEST_TRANSFER, nominal);
                json.put(ConstantTransaction.NO_REK_REQUEST_TRANSFER, noRek);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_REQUEST_TRANSFER);
                Log.v(ConstantTransaction.LOG_REQUEST_TRANSFER, ConstantTransaction.MESSAGE_LOG_REQUEST_TRANSFER);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
//                System.out.println(resultDec);
                System.out.println("Response Send "+resultEnc);
                System.out.println("Response Send Decrypt "+resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuRequestTransferAntarBank.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive "+result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt "+resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    token = jsonObject.getString(ConstantTransaction.TOKEN_REQUEST_TRANSFER);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Intent i = new Intent(activity, MenuRequestTransferKonfirmasi.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.BUNDLE_TOKEN, token);
                        bundle.putString(ConstantTransaction.BUNDLE_RC, rcResult);
                        bundle.putString(ConstantTransaction.BUNDLE_RM, rmResult);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
