package com.lakupandai.acs.lakupandaidiy.util;

public class ConstantError {
    //ALERT DIALOG
    public static String MESSAGE = "Pesan";
    public static String MESSAGE_LOGOUT = "Anda Yakin Ingin Keluar?";
    public static String MESSAGE_SMS = "Data Akan Dikirim Lewat SMS Karena Masalah Jaringan, Ijinkan?";
    public static String MESSAGE_YES = "Ya";
    public static String MESSAGE_NO = "Tidak";
    public static String MESSAGE_OK = "OK";
    public static String MESSAGE_FAILED = "Gagal Mengirim Data Harap Dicoba Kembali";
    public static String MESSAGE_ROOT = "Aplikasi tidak bisa digunakan bila Root Device Aktif";


    public static String ALERT_FIELD_EMPTY = "Mohon Lengkapi Semua Kolom";
    public static String ALERT_KTP_LESS_THAN_16 = "NIK Harus 16 Angka";
    public static String ALERT_PIN_LESS_THAN_6 = "Pin Harus 6 Angka";
    public static String ALERT_TOKEN_LESS_THAN_6 = "Token / OTP Harus 6 Angka";
    public static String ALERT_OTP_EMPTY="OTP tidak boleh kosong";
    public static String ALERT_NUMBER_LESS_THAN_10 = "Nomor HP Minimal 10 Angka";
    public static String ALERT_DATE_LESS_THAN_10 = "Format Tanggal Salah";
    public static String ALERT_USERNAME_EMPTY = "Username Tidak Boleh Kosong";
    public static String ALERT_KTP_NOT_VALID = "Isi Input NIK dengan Benar";
    public static String ALERT_NUMBER_NOT_VALID = "Isi Input NIK dengan Benar";
//    public static String ALERT_TARIK_TUNAI_LESS_THAN_100 = "Tarik Tunai Minimal 100 Rupiah";
//    public static String ALERT_SETOR_TUNAI_LESS_THAN_100 = "Setor Tunai Minimal 100 Rupiah";
    public static String ALERT_CONF_CODE_EMPTY = "Mohon Isi Token";
    public static String ALERT_CONF_CODE_LESS_THAN_6 = "Token Harus 6 Digit";
    public static String ALERT_NOMINAL_INPUT_FALSE = "Input Nominal Salah";
    public static String ALERT_NO_REK_LESS_THAN_10 = "Nomor Rekening Minimal 10 Angka";
    public static String ALERT_NOMINAL_LESS_THAN_1 = "Jumlah Nominal tidak boleh kurang dari 1 Rupiah";
    public static String ALERT_LAYANAN = "Silahkan Pilih Layanan";
    public static String ALERT_ID_PELANGGAN = "ID Pembayaran / Pelanggan Tidak Boleh Kosong";
    public static String ALERT_DEBIT_REKENING = "Rekening Ponsel Tidak Boleh Kosong";
    public static String ALERT_CONNECT = "Silahkan Periksa Koneksi Anda";

    public static String ALERT_REQPERMISSION_READPHONESTATE = "Mohon Ijinkan untuk mengakses Data Smartphone Anda";


}
