package com.lakupandai.acs.lakupandaidiy.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.notifikasian.view.MenuNotifikasi;

import static com.lakupandai.acs.lakupandaidiy.firebase.MyFirebaseMessagingService.NOTIFICATION_CHANNEL_ID;

public class NotificationHelper {

    public static void CreateNotification(Context context,String title,String desc){
        final Intent intent = new Intent(context, MenuNotifikasi.class);
        PendingIntent pi = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

//            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder notif = new Notification.Builder(context)
//                    .setContentIntent(pendingIntent)
                .setContentIntent(pi)
                //.addAction(R.drawable.logodiy, "View", pi)
                .setContentTitle(title)
                .setContentText(desc)
                .setSmallIcon(R.drawable.logo_bpd_notif)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.logodiy))
                //.setLargeIcon(result)
                //.setStyle(new Notification.BigPictureStyle().bigPicture(result))
                .setSound(defaultSoundUri);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            notif.setSmallIcon(R.drawable.ic_logo_bpd_diy);
            notif.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.logo_bpd_notif));
        }
//            if(status==1){
//
//            }else{
//                notif.setContentIntent(pi)
//                        .addAction(R.drawable.logodjulie, "View", pi);
//            }

        NotificationManager notificationManager =
                (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert notificationManager != null;
            notif.setChannelId(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationManager.notify(0, notif.build());
    }
}
