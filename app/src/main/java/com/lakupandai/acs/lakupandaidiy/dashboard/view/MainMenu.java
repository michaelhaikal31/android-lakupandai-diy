package com.lakupandai.acs.lakupandaidiy.dashboard.view;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.adapter.GridAdapter;
import com.lakupandai.acs.lakupandaidiy.aktivasi.MenuAktivasiRekBaru;
import com.lakupandai.acs.lakupandaidiy.alert.DialogInputPin;
import com.lakupandai.acs.lakupandaidiy.dashboard.adapter.SlidingImageAdapter;
import com.lakupandai.acs.lakupandaidiy.dashboard.model.MenuDashboard;
import com.lakupandai.acs.lakupandaidiy.notifikasian.view.MenuNotifikasi;
import com.lakupandai.acs.lakupandaidiy.pembayaran.view.MenuPembayaran;
import com.lakupandai.acs.lakupandaidiy.pembelian.view.MenuPembelian;
import com.lakupandai.acs.lakupandaidiy.saldo.view.MenuCekSaldo;
import com.lakupandai.acs.lakupandaidiy.saldo.view.MenuMutasiRekeningDetail;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.setortunai.view.MenuSetorTunai;
import com.lakupandai.acs.lakupandaidiy.tariktunai.view.MenuTarikTunai;
import com.lakupandai.acs.lakupandaidiy.transfer.view.MenuTransfer;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.ImageCache;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.SliderTimer;
import com.lakupandai.acs.lakupandaidiy.util.SpeedSlowScroller;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Timer;

public class MainMenu extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private int NUM_PAGES;
    private ArrayList<String> listNama;
    private ArrayList<MenuDashboard> listMenu;
    int currentPage = -1;
    private Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 1000;
    private GridAdapter adapter;
    private GridView gvHistory;
    private ViewPager mDemoSlider;
    private ImageView ivNotifikasi;
    boolean logout = false;
    private TabLayout tabDots;

    private String noHPAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    private Context mContext;

    public static ArrayList<String> layanan, sublayanan, denom;
    private String LAYANAN_TITLE = "";
    private static String LAYANAN_TITLE_1;
    private ProgressDialog progressDialog;
    private TripleDES tripleDES;
    private JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        ivNotifikasi = findViewById(R.id.ivNotifikasi);
        ivNotifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(), "Notifikasi", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainMenu.this, MenuNotifikasi.class));
            }
        });


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initList();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);
        ImageView imageView = hView.findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DialogInputPin callCenterDialog = new DialogInputPin();
                callCenterDialog.show(getFragmentManager(), "Masukan PIN Agen");
            }
        });

        TextView txNamaProfil = hView.findViewById(R.id.txNamaProfil);
        noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, mContext);
        txNamaProfil.setText(noHPAgen);
        adapter = new GridAdapter(MainMenu.this, listMenu);
        gvHistory = findViewById(R.id.gvHistory);
        mDemoSlider = findViewById(R.id.slider);
        tabDots = findViewById(R.id.sliderDot);
        tabDots.setupWithViewPager(mDemoSlider);
        gvHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String a = String.valueOf(position);
                if (a.equals("0")) {
                    Intent i = new Intent(MainMenu.this, MenuAktivasiRekBaru.class);
                    startActivity(i);
                } else if (a.equals("1")) {
                    Intent i = new Intent(MainMenu.this, MenuCekSaldo.class);
                    startActivity(i);
                } else if (a.equals("2")) {
                    Intent i = new Intent(MainMenu.this, MenuSetorTunai.class);
                    startActivity(i);
                } else if (a.equals("3")) {
                    Intent i = new Intent(MainMenu.this, MenuTarikTunai.class);
                    startActivity(i);
                } else if (a.equals("4")) {
                    Intent i = new Intent(MainMenu.this, MenuTransfer.class);
                    startActivity(i);
                } else if (a.equals("5")) {
                    LAYANAN_TITLE = "pembelian";
                    new MainMenu.executeLayanan(MainMenu.this).execute();
                } else if (a.equals("6")) {
                    LAYANAN_TITLE = "pembayaran";
                    new MainMenu.executeLayanan(MainMenu.this).execute();
                } else if (a.equals("7")) {
//                    Toast.makeText(getApplicationContext(), "7", Toast.LENGTH_SHORT).show();
                } else if (a.equals("8")) {
//                    Toast.makeText(getApplicationContext(), "8", Toast.LENGTH_SHORT).show();
                }
            }
        });

        gvHistory.setAdapter(adapter);
        //setDynamicHeight(gvHistory);
        slider();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.sideAktivasi) {
            Intent i = new Intent(MainMenu.this, MenuAktivasiRekBaru.class);
            startActivity(i);
        } else if (id == R.id.sideInformasiRekening) {
            Intent i = new Intent(MainMenu.this, MenuCekSaldo.class);
            startActivity(i);
        } else if (id == R.id.sideSetor) {
            Intent i = new Intent(MainMenu.this, MenuSetorTunai.class);
            startActivity(i);
        } else if (id == R.id.sideTarik) {
            Intent i = new Intent(MainMenu.this, MenuTarikTunai.class);
            startActivity(i);
        } else if (id == R.id.sideTransfer) {
            Intent i = new Intent(MainMenu.this, MenuTransfer.class);
            startActivity(i);
        } else if (id == R.id.sidePembelian) {
            LAYANAN_TITLE = "pembelian";
            new MainMenu.executeLayanan(MainMenu.this).execute();
        } else if (id == R.id.sidePembayaran) {
            LAYANAN_TITLE = "pembayaran";
            new MainMenu.executeLayanan(MainMenu.this).execute();
//            Intent i = new Intent(MainMenu.this, MenuPembayaran.class);
//            startActivity(i);
//            finish();
        } else if (id == R.id.sideCekStatus) {
//            Toast.makeText(getApplicationContext(), "7", Toast.LENGTH_SHORT).show();
            new MainMenu.executeMutasi(MainMenu.this).execute();
        } else if (id == R.id.sideKotakMasuk) {
            Intent i = new Intent(MainMenu.this, MenuNotifikasi.class);
            i.putExtra("kotak_masuk", true);
            startActivity(i);
//            Toast.makeText(getApplicationContext(), "8", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.nav_logout) {
            onBackPressed();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    class executeMutasi extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeMutasi(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainMenu.this);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                String dateDefault = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, MainMenu.this);
                json.put(ConstantTransaction.START_DATE, dateDefault);
                json.put(ConstantTransaction.END_DATE, dateDefault);
                json.put(ConstantTransaction.PAGE_JSON, "0");
                json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_MUTASI);
                //Log.v(Constant.LOG_LOGIN, Constant.MESSAGE_LOG_LOGIN);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);

                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MainMenu.this);
//                System.out.println("Result json: "+resultfromJson);
                resultfromJsonDecrypt = tripleDES.decrypt(resultfromJson);
//                System.out.println("RESPONSE DATA: " + resultfromJson);
//                System.out.println("RESPONSE DATA: " + resultfromJsonDecrypt);

                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;

            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(MainMenu.this, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.SERVER_ERROR)) {
                Function.showAlert(MainMenu.this, ConstantTransaction.SERVER_ERROR);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                //SendSms.splitSMS(resultEnc, mContext);
                Function.showAlert(MainMenu.this, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
//                    System.out.println("Nilai Result JSONParser Decrypt : " +resultfromJsonDecrypt.replaceAll("\\\\",""));
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);

                    //lastLogin = jsonObject.getString(Constant.LAST_LOGIN);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        JSONObject jsonA = new JSONObject(rmResult);
                        try {
                            Constant.jsonB = jsonA.getJSONArray("mutasi");
                            Constant.sizePerPage = jsonA.getInt("sizePerPage");
                            Constant.totalPages = jsonA.getInt("totalPages");
                            Constant.totalMutasi = jsonA.getInt("totalMutasi");
                            Constant.page = jsonA.getInt("page");
                            Constant.rc = jsonA.getInt("rc");
                        } catch (Exception e) {
                            Constant.jsonB = new JSONArray();
                            Constant.sizePerPage = 10;
                            Constant.totalPages = 999999;
                            Constant.totalMutasi = 999999;
                            Constant.page = 0;
                            Constant.rc = 0;
                        }
                        Intent i = new Intent(MainMenu.this, MenuMutasiRekeningDetail.class);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(MainMenu.this, rmResult);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Function.showAlert(MainMenu.this, e.toString());
                }
            }
        }
    }

    class executeLayanan extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeLayanan(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainMenu.this);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, MainMenu.this);
                json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                json.put(ConstantTransaction.TIPE, LAYANAN_TITLE);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_GET_MENU);
                tripleDES = new TripleDES();

                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Response Send " + resultEnc);
                System.out.println("Response Send Decrypt " + resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MainMenu.this);
                resultfromJsonDecrypt = tripleDES.decrypt(resultfromJson);
                return resultfromJsonDecrypt;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJsonDecrypt;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(MainMenu.this, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.SERVER_ERROR)) {
                Function.showAlert(MainMenu.this, ConstantTransaction.SERVER_ERROR);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(MainMenu.this, ConstantTransaction.CONNECTION_ERROR);
            } else if (result.contains(ConstantTransaction.SERVER_RETURN_EMPTY_DATA)) {
                Function.showAlert(MainMenu.this, "Maaf, layanan belum tersedia");
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT).replaceAll("\\\\", "");


                    JSONObject jsonA = new JSONObject(rmResult);//INI VERSI SEBENARNYA

                    JSONObject songs = jsonA.getJSONObject("data");
                    if (LAYANAN_TITLE.equals("pembelian")) {
                        layanan = new ArrayList<>();
                        sublayanan = new ArrayList<>();
                        Iterator x = songs.keys();
                        while (x.hasNext()) {
                            String key = (String) x.next();
                            layanan.add(key);
                            sublayanan.add(songs.getString(key));
                        }
                        Intent i = new Intent(MainMenu.this, MenuPembelian.class);
                        startActivity(i);
                    } else {
                        layanan = new ArrayList<>();
                        sublayanan = new ArrayList<>();
                        Iterator x = songs.keys();
                        while (x.hasNext()) {
                            String key = (String) x.next();
                            layanan.add(key);
                            sublayanan.add(songs.getString(key));
                        }
                        Intent i = new Intent(MainMenu.this, MenuPembayaran.class);
                        startActivity(i);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Function.showAlert(MainMenu.this, e.toString());
                }
            }
        }
    }

    class executeLogout extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainMenu.this);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }


    }

    private void initList() {
        listMenu = new ArrayList<>();
        listMenu.add(new MenuDashboard(R.drawable.ic_buka_rekening_baru, "Aktivasi Rekening Baru"));
        listMenu.add(new MenuDashboard(R.drawable.ic_informasi_rekening, "Informasi Rekening"));
        listMenu.add(new MenuDashboard(R.drawable.ic_setor_tunai, "Setor Tunai"));
        listMenu.add(new MenuDashboard(R.drawable.ic_tarik_tunai, "Tarik Tunai"));
        listMenu.add(new MenuDashboard(R.drawable.ic_transfer, "Transfer"));
        listMenu.add(new MenuDashboard(R.drawable.ic_pembelian, "Pembelian"));
        listMenu.add(new MenuDashboard(R.drawable.ic_pembayaran, "Pembayaran"));
    }


//    private void slider() {
//        final ImageCache imageCache = ImageCache.getInstance();
//        final HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
//
//        file_maps.put("Laku Pandai", R.drawable.slide_1);
//        file_maps.put("Mobile Banking", R.drawable.slide_2);
//
//        for (final String name : file_maps.keySet()) {
//            TextSliderView textSliderView = new TextSliderView(MainMenu.this);
//            // initialize a SliderLayout
//            textSliderView
////                    .description(name)
//                    .image(file_maps.get(name))
//                    .setScaleType(BaseSliderView.ScaleType.Fit)
//                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
//                        @Override
//                        public void onSliderClick(BaseSliderView slider) {
//                            int akhir = file_maps.get(name).toString().length();
//                            int awal = akhir - 1;
//                            String hasil = (file_maps.get(name).toString()).substring(awal, akhir);
////                            Toast.makeText(getActivity(), "Text : "+hasil+", ID : "+ getId(), Toast.LENGTH_SHORT).show();
//                        }
//                    });
//
//            //add your extra information
//            textSliderView.bundle(new Bundle());
//            textSliderView.getBundle()
//                    .putString("extra", name);
//            mDemoSlider.addSlider(textSliderView);
//        }
//    }

    private void slider() {
        final List<Bitmap> list = new ArrayList<>();
        final ImageCache imageCache = ImageCache.getInstance();
        Bitmap bm;
        final int length = DeviceSession.getJumlahSlideImage(this);
        if(length!=0) {
            for (int i = 0; i < length; i++) {
                bm = imageCache.getImageFromWarehouse("image_slider" + i);
                if (bm != null) {
                    list.add(bm);
                } else {
                    list.add(drawableToBitmap(getResources().getDrawable(R.drawable.ads)));
                    break;
                }
            }
            mDemoSlider.setAdapter(new SlidingImageAdapter(this, list));
            try {
                Field mScroller = ViewPager.class.getDeclaredField("mScroller");
                mScroller.setAccessible(true);
                SpeedSlowScroller scroller = new SpeedSlowScroller(this);
                mScroller.set(mDemoSlider, scroller);
                Timer timer = new Timer();
                timer.scheduleAtFixedRate(new SliderTimer(mDemoSlider, list.size(), this), 4000, 6000);
            } catch (Exception ignored) {
                System.out.println("asd exception timer slider " + ignored.getMessage());
            }
        }else{
            mDemoSlider.setVisibility(View.GONE);
        }
    }

    private Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;
        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }
        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }
        final Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    private void alertLogout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainMenu.this);
        builder.setTitle(ConstantTransaction.MESSAGE);
        builder.setMessage(ConstantTransaction.MESSAGE_LOGOUT);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setNegativeButton(ConstantTransaction.MESSAGE_YES, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                logout = true;
                finish();
            }
        }).setPositiveButton(ConstantTransaction.MESSAGE_NO, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertLogin = builder.create();
        alertLogin.show();

    }

    @Override
    public void onBackPressed() {
        alertLogout();
    }


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (DeviceSession.getNewNotif(this)) {
            ivNotifikasi.setImageDrawable(getResources().getDrawable(R.drawable.ic_notification_yellow));
        } else {
            ivNotifikasi.setImageDrawable(getResources().getDrawable(R.drawable.ic_notification));
        }
    }
}
