package com.lakupandai.acs.lakupandaidiy.setortunai.model;

/**
 * Created by Erdy on 12/04/2018.
 */
public class Respon {
    public String header;
    public String value;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Respon(String header, String value){
        this.header = header;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Respon{" +
                "header='" + header + '\'' +
                ", value='" + value + '\'' +
                '}';
    }


}
