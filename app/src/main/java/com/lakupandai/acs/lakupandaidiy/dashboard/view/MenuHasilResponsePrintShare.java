package com.lakupandai.acs.lakupandaidiy.dashboard.view;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.RT_Printer.BluetoothPrinter.BLUETOOTH.BluetoothPrintDriver;
import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.bluetooth.DeviceListActivity;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Erdy on 12/04/2018.
 */
public class MenuHasilResponsePrintShare extends HomeActivity {


    private ImageView llHomeMenuHasilResponse;
    private Activity activity;
    private TextView txtJudulMenuHasilResponse, txtHasilResponsePesanMenuHasilResponse;
    private String rmResult, titleMenu;
    private Button btnPrintMenuHasilResponse, btnShareMenuHasilResponse;
    public String SelectedBDAddress;
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;

    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_hasil_response_print_share);
        initUI();
    }

    private void initUI() {
        mContext = this;

        Bundle bundle = getIntent().getExtras();
        rmResult = bundle.getString(ConstantTransaction.BUNDLE_RM);
        titleMenu = bundle.getString(ConstantTransaction.BUNDLE_TITLE);


        btnShareMenuHasilResponse = (Button) findViewById(R.id.btnShareMenuHasilResponse);
        btnShareMenuHasilResponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Status Transaksi Terakhir");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, rmResult);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
        btnPrintMenuHasilResponse = (Button) findViewById(R.id.btnPrintMenuHasilResponse);
        btnPrintMenuHasilResponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mBluetoothAdapter.isEnabled()) {
                    Function.showAlert(activity, Constant.TURN_ON_BLUETOOTH);
                } else {
                    Intent serverIntent = null;
                    try {
                        serverIntent = new Intent(MenuHasilResponsePrintShare.this, DeviceListActivity.class);
                        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        txtJudulMenuHasilResponse = (TextView) findViewById(R.id.txtJudulMenuHasilResponse);
        txtHasilResponsePesanMenuHasilResponse = (TextView) findViewById(R.id.txtHasilResponsePesanMenuHasilResponse);

        txtJudulMenuHasilResponse.setText(titleMenu);
        txtHasilResponsePesanMenuHasilResponse.setText(rmResult);

        llHomeMenuHasilResponse = (ImageView) findViewById(R.id.llHomeMenuHasilResponse);
        llHomeMenuHasilResponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MainMenu.class);
                startActivity(i);
                finish();
            }
        });
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        System.out.println("REQ CODESS: " + requestCode);
//        System.out.println("RESULT CODESS: " + resultCode);
//        System.out.println("Data " + data);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // ������һ���豸֮ǰ�ȹر��������������豸֮���л�ʱ�����
                    BluetoothPrintDriver.close();
                    // ��ȡ�豸 MAC address
                    SelectedBDAddress = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    // ��������
                    if (!BluetoothPrintDriver.OpenPrinter(SelectedBDAddress)) {
                        BluetoothPrintDriver.close();
                        //BloothPrinterActivity.this.setTitle(R.string.bluetooth_connect_fail);
//	            		mTitle.setText("����ʧ��");
                        return;
                    } else {


                        InputStream in = null;
                        try {
                            in = getResources().getAssets().open("bpddiy320.png");

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        InputStream in1 = null;
                        try {
                            in1 = getResources().getAssets().open("lakupandai240.png");

                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                        BluetoothPrintDriver.Begin();

                        BufferedInputStream bis = new BufferedInputStream(in);
                        Bitmap bitmap = BitmapFactory.decodeStream(bis);

                        BufferedInputStream bis1 = new BufferedInputStream(in1);
                        Bitmap bitmap1 = BitmapFactory.decodeStream(bis1);

                        BluetoothPrintDriver.AddAlignMode((byte) 1);
                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.printByteData(printDraw(bitmap));

                        BluetoothPrintDriver.ClearData();


                        DateFormat date = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
                        Date datenow = new Date();
                        String hhnow = date.format(datenow);
                        //System.out.println("Waktu sekarang: " + hhnow);

//                        String textRow1 = " Jl. Basuki Rachmat No. 98-104";
//                        String textRow1aa = "Surabaya 60271";
//                        String textRow1a = "Telp.(031) 5310090 - 99";
//                        String textRow1b = "Fax.(031) 5310838";
//                        String textRow1c = "Email:info@bankjatim.co.id";

                        String bank = "Bank BPD DIY";
                        String textRow1 = "Jl. Tentara Pelajar No. 7 \nYogyakarta";
                        String textRow1aa = "(0274) 561614";
                        String textRow1a = "Telp.(031) 5310090 - 99";
                        String textRow1b = "Fax.(031) 5310838";
                        String textRow1c = "Email:info@bankdiy.co.id";

                        String textRow2 = "LAKUPANDAI";
                        String textRow3 = "Tanggal Cetak: " + hhnow;
                        //String textRow4 = "================================";
                        String textRow4 = "--------------------------------";
                        String textRow5 = hhnow;
                        String textRow7 = "SLIP INI ADALAH BUKTI TRANSAKSI";
                        String textRow8 = "YANG BERLAKU SESUAI DENGAN";
                        String textRow9 = "SYARAT DAN KETENTUAN YANG ";
                        String textRow10 = "BERLAKU DI BANK DIY\n\n\n\n";
                        String textRowIsi = rmResult+"\n";
                        //String textRowIsi = Constant.textToShare;

                        //0=left 1=center 2=right
//                        BluetoothPrintDriver.AddAlignMode((byte) 1);
//                        BluetoothPrintDriver.ImportData(bank);
//                        BluetoothPrintDriver.LF();
//                        BluetoothPrintDriver.excute();
//                        BluetoothPrintDriver.ClearData();

                        BluetoothPrintDriver.ImportData(textRow1);
                        BluetoothPrintDriver.LF();
                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.ClearData();

                        BluetoothPrintDriver.AddAlignMode((byte) 1);
                        BluetoothPrintDriver.ImportData(textRow1aa);
                        BluetoothPrintDriver.LF();
                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.ClearData();

                        BluetoothPrintDriver.AddAlignMode((byte) 1);

                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.printByteData(printDraw(bitmap1));
                        BluetoothPrintDriver.ClearData();

                        BluetoothPrintDriver.AddBold((byte) 0x00);
                        BluetoothPrintDriver.ImportData(textRow4);
                        BluetoothPrintDriver.LF();
                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.ClearData();

                        BluetoothPrintDriver.AddBold((byte) 0x01);
                        BluetoothPrintDriver.AddAlignMode((byte) 0);
                        BluetoothPrintDriver.ImportData(textRow5);
                        BluetoothPrintDriver.LF();
                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.ClearData();

                        BluetoothPrintDriver.AddBold((byte) 0x00);
                        BluetoothPrintDriver.ImportData(textRowIsi);
                        // BluetoothPrintDriver.LF();
                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.ClearData();

                        BluetoothPrintDriver.ImportData(textRow4);
                        BluetoothPrintDriver.LF();
                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.ClearData();

                        BluetoothPrintDriver.AddBold((byte) 0x00);
                        BluetoothPrintDriver.AddAlignMode((byte) 1);
                        BluetoothPrintDriver.ImportData(textRow7);
                        BluetoothPrintDriver.LF();
                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.ClearData();

                        BluetoothPrintDriver.AddAlignMode((byte) 1);
                        BluetoothPrintDriver.AddBold((byte) 0x00);
                        BluetoothPrintDriver.ImportData(textRow8);
                        BluetoothPrintDriver.LF();
                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.ClearData();

                        BluetoothPrintDriver.AddAlignMode((byte) 1);
                        BluetoothPrintDriver.AddBold((byte) 0x00);
                        BluetoothPrintDriver.ImportData(textRow9);
                        BluetoothPrintDriver.LF();
                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.ClearData();

                        BluetoothPrintDriver.AddAlignMode((byte) 1);
                        BluetoothPrintDriver.AddBold((byte) 0x00);
                        BluetoothPrintDriver.ImportData(textRow10);
                        BluetoothPrintDriver.LF();
                        BluetoothPrintDriver.excute();
                        BluetoothPrintDriver.ClearData();

                    }
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    ;//                connectDevice(data, false);
//                    System.out.println("INSECURE");
//                    System.out.println("RESULT OK");
                } else {
//                    System.out.println("INSECURE");
//                    System.out.println("RESULT NOT OK");
                }
                break;
        }
    }

    public byte[] printDraw(Bitmap bitmap) {
        byte[] arrby = new byte[8 + bitmap.getWidth() / 8 * bitmap.getHeight()];
        byte[] bitbuf = new byte[8 + bitmap.getWidth() / 8 * bitmap.getHeight()];
        arrby[0] = 29;
        arrby[1] = 118;
        arrby[2] = 48;
        arrby[3] = 0;
        arrby[4] = (byte) (bitmap.getWidth() / 8);
        arrby[5] = 0;
        arrby[6] = (byte) (bitmap.getHeight() % 256);
        arrby[7] = (byte) (bitmap.getHeight() / 256);
        int n = 7;
        int n2 = 0;
        block0:
        while (n2 < bitmap.getHeight()) {
            int n3 = 0;
            do {
                if (n3 >= bitmap.getWidth() / 8) break;
                int n4 = bitmap.getPixel(0 + n3 * 8, n2) == -1 ? 0 : 1;
                int n5 = bitmap.getPixel(1 + n3 * 8, n2) == -1 ? 0 : 1;
                int n6 = bitmap.getPixel(2 + n3 * 8, n2) == -1 ? 0 : 1;
                int n7 = bitmap.getPixel(3 + n3 * 8, n2) == -1 ? 0 : 1;
                int n8 = bitmap.getPixel(4 + n3 * 8, n2) == -1 ? 0 : 1;
                int n9 = bitmap.getPixel(5 + n3 * 8, n2) == -1 ? 0 : 1;
                int n10 = bitmap.getPixel(6 + n3 * 8, n2) == -1 ? 0 : 1;
                int n11 = bitmap.getPixel(7 + n3 * 8, n2) == -1 ? 0 : 1;
                int n12 = n11 + (n4 * 128 + n5 * 64 + n6 * 32 + n7 * 16 + n8 * 8 + n9 * 4 + n10 * 2);
                bitbuf[n3] = (byte) n12;
                ++n3;
            } while (true);
            int n13 = 0;
            do {
                int n14;
                if (n13 >= (n14 = bitmap.getWidth() / 8)) {
                    ++n2;
                    continue block0;
                }
                arrby[++n] = bitbuf[n13];
                ++n13;
            } while (true);
//			break;
        }
        return arrby;
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(mContext, MainMenu.class);
        startActivity(i);
        finish();
    }


}
