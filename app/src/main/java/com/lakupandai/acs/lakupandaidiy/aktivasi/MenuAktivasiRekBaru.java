package com.lakupandai.acs.lakupandaidiy.aktivasi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONObject;

/**
 * Created by Erdy on 12/03/2018.
 */
public class MenuAktivasiRekBaru extends HomeActivity {
    private EditText etNomorKTPMenuAktivasiRekBaru;
    private Button btnKirimMenuAktivasiRekBaru;
    private ImageView llHomeMenuAktivasiRekBaru;
    private Activity activity;
    public static String noKTP, nama, tanggalLahir, pinAgen, noHPAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_aktivasi_rek_baru);
        initUI();
    }

    private void initUI() {
        activity = this;
        etNomorKTPMenuAktivasiRekBaru = (EditText) findViewById(R.id.etNomorKTPMenuAktivasiRekBaru);

        llHomeMenuAktivasiRekBaru = (ImageView) findViewById(R.id.llHomeMenuAktivasiRekBaru);
        llHomeMenuAktivasiRekBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnKirimMenuAktivasiRekBaru = (Button) findViewById(R.id.btnKirimMenuAktivasiRekBaru);
        btnKirimMenuAktivasiRekBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noKTP = etNomorKTPMenuAktivasiRekBaru.getText().toString();
                if (noKTP.isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (noKTP.length() < 16 && noKTP.length() > 0) {
                    Function.showAlert(activity, ConstantError.ALERT_KTP_LESS_THAN_16);
                } else {
                    new executeAktivasiRekBaru(activity).execute();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    class executeAktivasiRekBaru extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeAktivasiRekBaru(Context mContext) {
            this.context = mContext;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.NO_KTP_REK_BARU, noKTP);
                json.put(ConstantTransaction.MSISDN_AGEN_REK_BARU, noHPAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.CHECK_NIK);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Json Send: " + json.toString());
                System.out.println("Json encry "+resultEnc);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuAktivasiRekBaru.this);
                System.out.println("Json Receive: " + resultfromJson);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    System.out.println("Json Receive : " + jsonObject);

                    int rm = rmResult.length();
                    String tglLahir = rmResult.substring(rm - 8, rm);
                    String tgl = tglLahir.length() < 2 ? tglLahir : tglLahir.substring(0, 2);
                    String bulan = tglLahir.length() < 2 ? tglLahir : tglLahir.substring(2, 4);
                    String tahun = tglLahir.length() < 2 ? tglLahir : tglLahir.substring(4, tglLahir.length());
                    tanggalLahir = tgl + "/" + bulan + "/" + tahun;
                    nama = rmResult.substring(0, rm - 9);
                    System.out.println("Nama : " + nama + "\nTanggal : " + tanggalLahir);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Intent i = new Intent(activity, MenuAktivasiRekBaruDetail.class);
                        startActivity(i);
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());

                }
            }
        }
    }

}
