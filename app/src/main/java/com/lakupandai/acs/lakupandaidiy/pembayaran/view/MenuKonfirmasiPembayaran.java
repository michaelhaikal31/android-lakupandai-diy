package com.lakupandai.acs.lakupandaidiy.pembayaran.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.adapter.ResponAdapter;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponseNew;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponsePulsa;
import com.lakupandai.acs.lakupandaidiy.firebase.MyFirebaseMessagingService;
import com.lakupandai.acs.lakupandaidiy.pembelian.view.MenuKonfirmasiPembelianPulsa;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.setortunai.model.Respon;
import com.lakupandai.acs.lakupandaidiy.tariktunai.view.MenuKonfirmasiTarikTunai;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.NotificationHelper;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Erdy on 12/06/2018.
 */
public class MenuKonfirmasiPembayaran extends HomeActivity {
    private Activity activity;
    private Button btnKirimPembayaran;
    private ListView listRespon;
    private EditText etPin, etOTPPembayaran;
    private ImageView llHomeMenuKonfirmasiPembayaran;
    private String noHPAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;

    private TextView tvMetode, tvResendOTP;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();
    private TextView txtProgressBar;
    private TextView txtCaption1;
    private TextView txtCaption2;
    private ProgressBar progressBar;
    private CountDownTimer countDownTimer;
    ArrayList<Respon> myListItems = new ArrayList<>();
    ResponAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_konfirmasi_pembayaran);
        initUI();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void initUI() {
        activity = this;
        etPin = (EditText) findViewById(R.id.etPin);
        etOTPPembayaran = (EditText) findViewById(R.id.etOTPPembayaran);
        listRespon = (ListView) findViewById(R.id.listRespon);

//        JSONArray jsonA = null;
//        System.out.println("rmResult "+ MenuPembayaran.dataResult);
        txtProgressBar = findViewById(R.id.txtProgressBar);
        txtCaption1 = findViewById(R.id.textView16);
        txtCaption2 = findViewById(R.id.textView17);

        JSONObject jsonC;
        myListItems = new ArrayList<>();

        try {
            System.out.println("asd mortne "+Constant.dataResult);
            JSONArray jsonArray = Constant.dataResult;

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                myListItems.add(new Respon(jsonObject1.getString("header"), jsonObject1.getString("value")));
            }

        }catch (JSONException e){
            System.out.println("asd JSON EXCEPTION "+e.toString());
        }

        adapter = new ResponAdapter(getApplicationContext(), myListItems);
        listRespon.setAdapter(adapter);

        tvMetode = findViewById(R.id.tvMetode);
        tvMetode.setText("Metode Pembayaran\n" + Constant.metode);
        if (Constant.metode.equals("Debit")) {
            etOTPPembayaran.setVisibility(View.VISIBLE);
            txtCaption2.setVisibility(View.VISIBLE);
            txtCaption1.setVisibility(View.VISIBLE);
            txtProgressBar.setVisibility(View.VISIBLE);
            startCountdown();
        } else {
            etOTPPembayaran.setVisibility(View.GONE);
            txtCaption2.setVisibility(View.GONE);
            txtCaption1.setVisibility(View.GONE);
            txtProgressBar.setVisibility(View.GONE);
        }

        btnKirimPembayaran = (Button) findViewById(R.id.btnKirimPembayaran);
        btnKirimPembayaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(etOTPPembayaran.getText().toString().length()==0 && Constant.metode.equals("Debit")){
                    Function.showAlert(activity,ConstantError.ALERT_FIELD_EMPTY);
                }else if (etOTPPembayaran.getText().toString().length() < 6 && Constant.metode.equals("Debit")) {
                    Function.showAlert(activity, ConstantError.ALERT_TOKEN_LESS_THAN_6);
                }else if (etPin.getText().toString().length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                }  else {
                    new executeKonfirmasiPembayaran(activity,etOTPPembayaran.getText().toString(),etPin.getText().toString()).execute();
                }
            }
        });

        llHomeMenuKonfirmasiPembayaran = (ImageView) findViewById(R.id.llHomeMenuKonfirmasiPembayaran);
        llHomeMenuKonfirmasiPembayaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
    private void startCountdown() {
        txtCaption1.setVisibility(View.VISIBLE);
        txtProgressBar.setVisibility(View.VISIBLE);
        txtCaption2.setTextColor(Color.parseColor("#808080"));
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int second = (int) ((millisUntilFinished / 1000));
                //txtProgressBar.setText(second);
                txtProgressBar.setText(String.valueOf(second));
            }

            @Override
            public void onFinish() {
                txtCaption1.setVisibility(View.GONE);
                txtProgressBar.setVisibility(View.INVISIBLE);
                txtProgressBar.setText("");
                txtCaption2.setText("Kirim ulang OTP");
                txtCaption2.setTextColor(getResources().getColor(R.color.colorPrimary));

                txtCaption2.setTypeface(null, Typeface.BOLD);
                txtCaption2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new executeResendOTP().execute();
                        System.out.println("asd [menukonfirPembayaran  ressend otp clicked | data param body ] "+ DeviceSession.getTempRequestBody(MenuKonfirmasiPembayaran.this));
                        startCountdown();
                    }
                });
                }
        };

        countDownTimer.start();

    }
    class executeKonfirmasiPembayaran extends AsyncTask<String, JSONObject, String> {
        Context context;
        String otp;
        String pin;

        private executeKonfirmasiPembayaran(Context mContext,String otp,String pin) {
            this.context = mContext;
            this.otp = otp;
            this.pin = pin;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                if (Constant.metode.equals("Debit")) {
                    json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                    json.put(ConstantTransaction.PIN_PEMBAYARAN, pin);
                    json.put(ConstantTransaction.OTP_PEMBAYARAN, otp);
                    json.put(ConstantTransaction.KODE_PRODUK, MenuPembayaran.productCode);
                    json.put(ConstantTransaction.TIPE, (Constant.metode).toLowerCase());
                    json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_KONFIRMASI_PEMBAYARAN);
                } else {
                    json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                    json.put(ConstantTransaction.PIN_PEMBAYARAN, pin);
                    json.put(ConstantTransaction.KODE_PRODUK, MenuPembayaran.productCode);
                    json.put(ConstantTransaction.OTP_PEMBAYARAN, MenuPembayaran.otp);
                    json.put(ConstantTransaction.TIPE, (Constant.metode).toLowerCase());
                    json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_KONFIRMASI_PEMBAYARAN);
                }
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Response Send " + resultEnc);
                System.out.println("Response Send Decrypt " + resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuKonfirmasiPembayaran.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            System.out.println("asd response [pembayaran] "+tripleDES.decrypt(result));
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (jsonObject.getString("RC").equalsIgnoreCase("00")) {
                        DeviceSession.setTempRequestBody(MenuKonfirmasiPembayaran.this,"");
                        DeviceSession.setTempRequestUrl(MenuKonfirmasiPembayaran.this,"");
                        JSONObject jsonDATA = new JSONObject(rmResult);
                        final JSONArray notifikasi = new JSONArray(jsonDATA.getString(ConstantTransaction.DATA));
                        Constant.dataResult = notifikasi;
                        if (Constant.metode.equals("Tunai")) {
                            String resi = "";
                            String nominal = "";
                            String layanan = "";
                            String tanggal = "";
                            final int size = notifikasi.length();
                            for (int i = 0; i < size; i++) {
                                if (notifikasi.getJSONObject(i).getString("header").contains("Resi")) {
                                    resi = notifikasi.getJSONObject(i).getString("value");
                                }
                                if (notifikasi.getJSONObject(i).getString("header").contains("Jumlah Bayar")) {
                                    nominal = notifikasi.getJSONObject(i).getString("value");
                                }
                                if (notifikasi.getJSONObject(i).getString("header").contains("Layanan")) {
                                    layanan = notifikasi.getJSONObject(i).getString("value");
                                }
                                if (notifikasi.getJSONObject(i).getString("header").contains("Waktu")) {
                                    tanggal = notifikasi.getJSONObject(i).getString("value");
                                }
                            }

                            JSONArray data = new JSONArray();
                            if (DeviceSession.getJsonNotif(MenuKonfirmasiPembayaran.this).isEmpty()) {
                                data.put(new JSONObject()
                                        .put("notifikasi", notifikasi)
                                        .put("date", tanggal)
                                        .put("nominal", nominal)
                                        .put("layanan", layanan)
                                        .put("isRead", false)
                                        .put("resi", resi));
                                DeviceSession.setJsonNotif(MenuKonfirmasiPembayaran.this, data.toString());
                            } else {
                                System.out.println("asd notif else");
                                data = new JSONArray(DeviceSession.getJsonNotif(MenuKonfirmasiPembayaran.this));
                                data.put(new JSONObject()
                                        .put("notifikasi", notifikasi)
                                        .put("date", tanggal)
                                        .put("nominal", nominal)
                                        .put("layanan", layanan)
                                        .put("isRead", false)
                                        .put("resi", resi));
                                DeviceSession.setJsonNotif(MenuKonfirmasiPembayaran.this, data.toString());
                            }
                            DeviceSession.setNewNotif(MenuKonfirmasiPembayaran.this, true);
                            NotificationHelper.CreateNotification(MenuKonfirmasiPembayaran.this, "Pembayaran " + layanan, "Tekan untuk melihat transaksi di kotak masuk");
                        }
                        final Intent i = new Intent(activity, MenuHasilResponsePulsa.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.BUNDLE_TITLE, ConstantTransaction.BUNDLE_TITLE_PEMBAYARAN + " anda\nBERHASIL");
                        bundle.putString(ConstantTransaction.BUNDLE_RM, rmResult);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    }else if(jsonObject.getString("RC").equalsIgnoreCase("70") || jsonObject.getString("RC").equalsIgnoreCase("21")){
                        Function.showAlert(activity,rmResult);
                    } else {
                        Function.showListenerOkDialogMessage(activity, getLayoutInflater(), rmResult, new Function.InputSenderDialogListener() {
                            @Override
                            public void onOK(String value) {
                                finish();
                            }

                            @Override
                            public void onCancel(String value) {

                            }
                        });

                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    class executeResendOTP extends AsyncTask<String, Void, String> {
        Context context;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MenuKonfirmasiPembayaran.this);
            progressDialog.setMessage("mengirim ulang OTP..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... result) {
            return jsonParser.HttpRequestPost(ConstantTransaction.URL, DeviceSession.getTempRequestBody(MenuKonfirmasiPembayaran.this), ConstantTransaction.TimeOutConnection, MenuKonfirmasiPembayaran.this);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {

                        Function.showAlert(MenuKonfirmasiPembayaran.this,"OTP berhasil dikirim, silahkan masukan OTP");
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
