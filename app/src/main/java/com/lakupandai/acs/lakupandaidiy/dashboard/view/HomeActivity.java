package com.lakupandai.acs.lakupandaidiy.dashboard.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.lakupandai.acs.lakupandaidiy.login.view.MenuLogin;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;

/**
 * Created by Erdy on 12/03/2018.
 */
public class HomeActivity extends Activity {
    Context mContext;
    static final String ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    static final String ACTION_SMS = "android.provider.Telephony.SMS_RECEIVED";
    public Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == ConstantTransaction.DISPLAY_DATA) {
                MessageTimeout();
            }
        }
    };

    private void MessageTimeout() {
        if (!((Activity) mContext).isFinishing()) {
            new android.app.AlertDialog.Builder(mContext).setTitle(ConstantTransaction.MESSAGE).setMessage(ConstantTransaction.MESSAGE_TIMEOUT).
                    setPositiveButton(ConstantTransaction.MESSAGE_OK, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(mContext, MenuLogin.class);
                            startActivity(i);
                            finish();
                        }
                    }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    Intent i = new Intent(mContext, MenuLogin.class);
                    startActivity(i);
                    finish();
                }
            }).setIcon(android.R.drawable.ic_dialog_alert).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mHandler.sendEmptyMessageDelayed(ConstantTransaction.DISPLAY_DATA, ConstantTransaction.TimeOutApps);
        IntentFilter filter = new IntentFilter(ACTION_SMS);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        //System.out.println("AA:" + activeNetworkInfo);
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
