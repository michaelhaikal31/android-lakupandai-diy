package com.lakupandai.acs.lakupandaidiy.notifikasian.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.adapter.NotifikasiAdapter;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponsePulsa;
import com.lakupandai.acs.lakupandaidiy.notifikasian.model.Notifikasi;

import com.lakupandai.acs.lakupandaidiy.pembayaran.view.MenuKonfirmasiPembayaran;
import com.lakupandai.acs.lakupandaidiy.pembayaran.view.MenuPembayaran;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.lakupandai.acs.lakupandaidiy.util.Constant.jsonBnotifikasi;
import static com.lakupandai.acs.lakupandaidiy.util.Constant.page;
import static com.lakupandai.acs.lakupandaidiy.util.Constant.pagenotifikasi;
import static com.lakupandai.acs.lakupandaidiy.util.Constant.rcnotifikasi;
import static com.lakupandai.acs.lakupandaidiy.util.Constant.sizePerPagenotifikasi;
import static com.lakupandai.acs.lakupandaidiy.util.Constant.totalMutasinotifikasi;



public class MenuNotifikasi extends AppCompatActivity {

    private ListView lvNotif;
    private ArrayList<Notifikasi> listNotifikasi;
    public static String rrn;
    public static String img;
    private ImageView btnHome;
    private JSONArray arrayNotif;
    private TextView txtTitle;
    private ProgressDialog progressDialog;
    private String LAYANAN_TITLE="list_notifikasi";
    private NotifikasiAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_notifikasi);
        lvNotif = findViewById(R.id.listNotifikasi);
        btnHome = findViewById(R.id.llHomeMenuNotifikasi);
        txtTitle = findViewById(R.id.txtTitle);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        lvNotif.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                System.out.println("asd notif post "+i);
//                try {
//                    Constant.dataResult = arrayNotif.getJSONObject(arrayNotif.length()-1-i).getJSONArray("notifikasi");
//                    Constant.metode ="Bebit";
//                    arrayNotif.getJSONObject(arrayNotif.length()-1-i).put("isRead",true);
//                    DeviceSession.setJsonNotif(MenuNotifikasi.this,arrayNotif.toString());
//                    checkForUnreadableNotif();
//
//
//                }catch (Exception e){
//                    System.out.println("asd show detail notif "+e.getMessage());
//                    Function.showAlert(MenuNotifikasi.this,"Terjadi kesalahan saat menampilkan pesan !");
//                }
                new executeGetDetailNotif(listNotifikasi.get(i).getResi(),i).execute();
            }
        });
        if(getIntent().getExtras()!=null && getIntent().getExtras().getBoolean("kotak_masuk")){
            txtTitle.setText("Kotak Masuk");
        }
        DeviceSession.setNewNotif(this,false);
        System.out.println("asd new notif "+DeviceSession.getNewNotif(this));
        new executeGetListNotif().execute();

    }

    class executeGetListNotif extends AsyncTask<String,Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MenuNotifikasi.this);
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            final JSONParser jsonParser = new JSONParser();
            final JSONObject jsonParam = new JSONObject();
            try {
                jsonParam.put(ConstantTransaction.MSISDN_AGEN_LOGIN, Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, MenuNotifikasi.this));
                jsonParam.put(ConstantTransaction.PAGE_JSON,"0");
                jsonParam.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_GET_LIST_NOTIF);
                final TripleDES tripleDES = new TripleDES();
                System.out.println("asd data kirim [MenuNotifikasi] "+jsonParam);
                final String encryptedParam = tripleDES.encrypt(jsonParam.toString());
                final String response = jsonParser.HttpRequestPost(ConstantTransaction.URL, encryptedParam, ConstantTransaction.TimeOutConnection, MenuNotifikasi.this);
                return response;
            }catch (Exception e){
                System.out.println("asd exception[menu notifikasi] "+e.getMessage());
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                progressDialog.dismiss();
                result = new TripleDES().decrypt(result);
                System.out.println("asd response detail trx[MennuNotifikasi] "+result);
                if(result!=null){
                    System.out.println("asd response [menu notifikasi] "+result);
                    generateNotifikasi(result);
                }
            }catch (Exception e){
                System.out.println("asd exception post execute [MenuNotifikasi] "+e.getMessage());
            }
        }


    }

    class executeGetDetailNotif extends AsyncTask<String,Void,String>{
        private String resi;
        private int position;
        public executeGetDetailNotif(String resi,int pos) {
            this.resi = resi;
            this.position=pos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            final JSONParser jsonParser = new JSONParser();
            final JSONObject jsonParam = new JSONObject();
            try {
                jsonParam.put(ConstantTransaction.MSISDN_AGEN_LOGIN, Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, MenuNotifikasi.this));
                jsonParam.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_GET_DETAIL_TRX);
                jsonParam.put(ConstantTransaction.NO_RESI,resi);
                final TripleDES tripleDES = new TripleDES();
                System.out.println("asd data kirim [MenuNotifikasi] "+jsonParam);
                final String encryptedParam = tripleDES.encrypt(jsonParam.toString());
                final String response = jsonParser.HttpRequestPost(ConstantTransaction.URL, encryptedParam, ConstantTransaction.TimeOutConnection, MenuNotifikasi.this);
                return response;
            }catch (Exception e){
                System.out.println("asd exception[menu notifikasi] "+e.getMessage());
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            try {
                result = new TripleDES().decrypt(result);
                System.out.println("asd response detail[menu notfikasi] "+result);
                if (!result.isEmpty()) {
                    listNotifikasi.get(position).setAlreadyRead(true);
                    adapter.notifyDataSetChanged();
                    JSONObject jsonDATA = new JSONObject(result);
                    final JSONObject data = new JSONObject(jsonDATA.getString(ConstantTransaction.RM_RESULT));
                    Constant.layanan_title="Detail Notifikasi";
                    Constant.dataResult = data.getJSONArray(ConstantTransaction.DATA);
                    Intent intent = new Intent(MenuNotifikasi.this, MenuHasilResponsePulsa.class);
                    intent.putExtra("notif",true);
                    startActivity(intent);
                }
            }catch (Exception e){
                System.out.println("asd exception [MenuNotifikasi]"+e.getMessage());
            }
        }


    }

//    private void generatePeriode(String array) {
//
//        try {
//            JSONArray jsonArray = new JSONArray(array);
//            JSONObject jsonObject;
//            for(int i =0;i<jsonArray.length();i++){
//                jsonObject = jsonArray.getJSONObject(i);
//                if(jsonObject.getString("header").equals("Periode")){
//                    String value = jsonObject.getString("value");
//                    jsonArray.put(i,jsonObject.put("value",""));
//                    jsonArray.put("")
//                }
//            }
//        }catch (Exception e){
//
//        }
//    }

    private void generateNotifikasi(String jsonNotif) {
        try {

            JSONObject jsonObject = new JSONObject(jsonNotif);
            jsonObject = new JSONObject(jsonObject.getString("RM"));
            System.out.println("asd jsonObject "+jsonObject.toString());
            arrayNotif = jsonObject.getJSONArray("data");
            final int size = arrayNotif.length();
            listNotifikasi  =  new ArrayList<>();
            JSONObject notifikasi;
            for(int i=0;i<size;i++){
                notifikasi = arrayNotif.getJSONObject(i);
                listNotifikasi.add(new Notifikasi(notifikasi.getString("layanan"),notifikasi.getString("date"),notifikasi.getString("totalAmount"),notifikasi.getString("resi"),notifikasi.getBoolean("isRead")));
            }
            adapter = new NotifikasiAdapter(MenuNotifikasi.this,listNotifikasi);
            lvNotif.setAdapter(adapter);

        }catch (Exception e){
            System.out.println("asd exception [MenuNotifikasi] "+e.getMessage());
        }
    }

    private void checkForUnreadableNotif(){
        try {
        final int size = arrayNotif.length();
        boolean unReadableNotif=false;
            for (int i = 0; i < size; i++) {
                if (!arrayNotif.getJSONObject(i).getBoolean("isRead")){
                    unReadableNotif=true;
                    break;
                }
            }
            if(unReadableNotif){
                DeviceSession.setNewNotif(this,true);
            }else{
                DeviceSession.setNewNotif(this,false);
            }
        }catch (Exception e){
            System.out.println("asd exception checkUnredableNotif[menunotifikasi] "+e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
