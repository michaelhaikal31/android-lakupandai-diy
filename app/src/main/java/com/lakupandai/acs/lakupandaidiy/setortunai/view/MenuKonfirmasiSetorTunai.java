package com.lakupandai.acs.lakupandaidiy.setortunai.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.adapter.ResponAdapter;
import com.lakupandai.acs.lakupandaidiy.pembayaran.view.MenuKonfirmasiPembayaran;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.setortunai.model.Respon;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.NotificationHelper;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erdy on 12/04/2018.
 */
public class MenuKonfirmasiSetorTunai extends Activity {

    private Activity activity;
    private Button btnKirimMenuKonfirmasiSetorTunai;
    private ListView listRespon;
    private EditText etPinAgenMenuKonfirmasiSetorTunai;
    private ImageView llHomeMenuKonfirmasiSetorTunai;
    private String noHPAgen, pinAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;

    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    ArrayList<Respon> myListItems = new ArrayList<>();
    ResponAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_konfirmasi_setor_tunai);
        initUI();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregisterReceiver(receiver);
    }

    private void initUI() {
        activity = this;
        etPinAgenMenuKonfirmasiSetorTunai = (EditText) findViewById(R.id.etPinAgenMenuKonfirmasiSetorTunai);

        listRespon = findViewById(R.id.listRespon);
        JSONObject jsonC;
        myListItems = new ArrayList<>();
        for (int a=0;a< Constant.dataResult.length(); a++){
            try {
                jsonC = Constant.dataResult.getJSONObject(a);
                myListItems.add(new Respon(jsonC.getString("header"), jsonC.getString("value")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter = new ResponAdapter(getApplicationContext(), myListItems);
        listRespon.setAdapter(adapter);

        btnKirimMenuKonfirmasiSetorTunai = (Button) findViewById(R.id.btnKirimMenuKonfirmasiSetorTunai);
        btnKirimMenuKonfirmasiSetorTunai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etPinAgenMenuKonfirmasiSetorTunai.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etPinAgenMenuKonfirmasiSetorTunai.getText().toString().length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else {
                    pinAgen = etPinAgenMenuKonfirmasiSetorTunai.getText().toString();
                    new executeKonfirmasiSetorTunai(activity).execute();
                }
            }
        });

        llHomeMenuKonfirmasiSetorTunai = (ImageView) findViewById(R.id.llHomeMenuKonfirmasiSetorTunai);
        llHomeMenuKonfirmasiSetorTunai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    class executeKonfirmasiSetorTunai extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeKonfirmasiSetorTunai(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN_KONF_SETOR_TUNAI, noHPAgen);
                json.put(ConstantTransaction.TOKEN_KONF_SETOR_TUNAI, MenuSetorTunai.token);
                json.put(ConstantTransaction.PIN_KONF_SETOR_TUNAI, pinAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_KONFIRMASI_SETOR_TUNAI);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Response Send "+resultEnc);
                System.out.println("Response Send Decrypt "+resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuKonfirmasiSetorTunai.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive "+result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt "+resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Function.showAlertHome(activity, rmResult);
                        DeviceSession.setNewNotif(MenuKonfirmasiSetorTunai.this, true);
                        NotificationHelper.CreateNotification(MenuKonfirmasiSetorTunai.this, "Setor tunai berhasil ", "Tekan untuk melihat transaksi di kotak masuk");
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(activity, MenuSetorTunai.class);
        startActivity(i);
        finish();
    }
}
