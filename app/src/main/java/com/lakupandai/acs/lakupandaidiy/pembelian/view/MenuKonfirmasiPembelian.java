package com.lakupandai.acs.lakupandaidiy.pembelian.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.adapter.ResponAdapter;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponseNew;
import com.lakupandai.acs.lakupandaidiy.setortunai.model.Respon;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by Erdy on 12/06/2018.
 */
public class MenuKonfirmasiPembelian extends HomeActivity {
    private Activity activity;
    private Button btnKirimPembelian;
    private ListView listRespon;
    private EditText etPin, etOTPPembelian;
    private ImageView llHomeMenuKonfirmasiPembelian;
    private String noHPAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    private TableRow tblRowMenuKonfirmasiTarikTunai1, trResendOTP;
    private TextView tvMetode, tvResendOTP;
    private String otp;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    ArrayList<Respon> myListItems = new ArrayList<>();
    ResponAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_konfirmasi_pembelian);
        initUI();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregisterReceiver(receiver);
    }

    private void initUI() {
        activity = this;
        etPin = (EditText) findViewById(R.id.etPin);
        etOTPPembelian = (EditText) findViewById(R.id.etOTPPembelian);
        listRespon = (ListView) findViewById(R.id.listRespon);

        tvResendOTP = findViewById(R.id.tvResendOTP);
        tvResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new executeResendOTP(activity).execute();
            }
        });

        JSONObject jsonC;
        myListItems = new ArrayList<>();
        for (int a = 0; a < Constant.dataResult.length(); a++) {
            try {
                jsonC = Constant.dataResult.getJSONObject(a);
                String convertValue;
                if (jsonC.getString("header").equals("Desc")) {
                    if(jsonC.getString("value").contains(",")){
                        String value = jsonC.getString("value");
//                        System.out.println("Value 1 "+value);
//                        String[] value_split = value.split(Pattern.quote(","));
//                        convertValue = value_split[1].substring(1);
//                        System.out.println("Value 2 "+convertValue);
                        convertValue = value;
                    }else{
                        String value = jsonC.getString("value");
                        convertValue = value;
                    }

                    //Respon respon = new Respon(jsonC.getString("header"),convertValue);

                    //myListItems.add(new Respon(jsonC.getString("header"), convertValue.replace("]","")));

                }else if(jsonC.getString("header").equals("otp")){
                    otp = jsonC.getString("value");
                }else {
                    myListItems.add(new Respon(jsonC.getString("header"), jsonC.getString("value")));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter = new ResponAdapter(getApplicationContext(), myListItems);
        listRespon.setAdapter(adapter);

        tvMetode = findViewById(R.id.tvMetode);
        tvMetode.setText("Metode Pembelian\n" +  Constant.metode);
        tblRowMenuKonfirmasiTarikTunai1 = findViewById(R.id.tblRowMenuKonfirmasiTarikTunai1);
        trResendOTP = findViewById(R.id.trResendOTP);
        if ( Constant.metode.equals("Debit")) {
            tblRowMenuKonfirmasiTarikTunai1.setVisibility(View.VISIBLE);
            trResendOTP.setVisibility(View.VISIBLE);
        } else {
            tblRowMenuKonfirmasiTarikTunai1.setVisibility(View.GONE);
            trResendOTP.setVisibility(View.GONE);
        }

        btnKirimPembelian = (Button) findViewById(R.id.btnKirimPembelian);
        btnKirimPembelian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etPin.getText().toString().length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else if (etOTPPembelian.getText().toString().length() < 6 &&  Constant.metode.equals("debit")) {
                    Function.showAlert(activity, ConstantError.ALERT_TOKEN_LESS_THAN_6);
                } else {
                    new executeKonfirmasiPembelian(activity,etOTPPembelian.getText().toString(),etPin.getText().toString()).execute();
                }
            }
        });

        llHomeMenuKonfirmasiPembelian = (ImageView) findViewById(R.id.llHomeMenuKonfirmasiPembelian);
        llHomeMenuKonfirmasiPembelian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    class executeKonfirmasiPembelian extends AsyncTask<String, JSONObject, String> {
        Context context;
        String otp;
        String pin;


        private executeKonfirmasiPembelian(Context mContext,String otp,String pin) {
            this.context = mContext;
            this.otp = otp;
            this.pin = pin;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                if ( Constant.metode.equals("Debit")){
                    json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                    json.put(ConstantTransaction.PIN_PEMBAYARAN, pin);
                    json.put(ConstantTransaction.OTP_PEMBAYARAN, otp);
                    json.put(ConstantTransaction.KODE_PRODUK, com.lakupandai.acs.lakupandaidiy.pembelian.view.MenuPembelian.productCode);
                    json.put(ConstantTransaction.TIPE, ( Constant.metode).toLowerCase());
                    json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_KONFIRMASI_PEMBELIAN);
                } else {
                    json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                    json.put(ConstantTransaction.PIN_PEMBAYARAN, pin);
                    json.put(ConstantTransaction.KODE_PRODUK, MenuPembelian.productCode);
                    json.put(ConstantTransaction.OTP_PEMBAYARAN, MenuPembelian.otp);
                    json.put(ConstantTransaction.TIPE, (Constant.metode).toLowerCase());
                    json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_KONFIRMASI_PEMBELIAN);

                }
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Response Send " + resultEnc);
                System.out.println("Response Send Decrypt " + resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuKonfirmasiPembelian.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            System.out.println("asd [MenuKonfrimasiPembelian on post execute]");
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("asd response [MenuKonfirmasiPembelian] " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        System.out.println("asd [MenuKonfirmasiPembelian] success code");
                        Function.showAlertHome(activity, rmResult);
//                        JSONObject jsonDATA = new JSONObject(rmResult);
//                        String data = jsonDATA.getString(ConstantTransaction.DATA);
//                        Constant.dataResult = new JSONArray(data);
//                        Intent i = new Intent(activity, MenuHasilResponseNew.class);
//                        Bundle bundle = new Bundle();
//                        bundle.putString(ConstantTransaction.BUNDLE_TITLE, ConstantTransaction.BUNDLE_TITLE_PEMBELIAN + " " + MainMenu.sublayanan + " anda\nBERHASIL");
//                        bundle.putString(ConstantTransaction.BUNDLE_RM, rmResult);
//                        i.putExtras(bundle);
//                        startActivity(i);
//                        finish();
                    } else {
                        System.out.println("asd [MenuKonfirmasiPembelian] not success code");
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    class executeResendOTP extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeResendOTP(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                json.put(ConstantTransaction.MSISDN_NASABAH_INQ, Constant.msisdnNasabah);
                json.put(ConstantTransaction.ACTION, Constant.ACTION_RESEND_TOKEN);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
//                System.out.println("OK OK "+ resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuKonfirmasiPembelian.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
//                    System.out.println("Nilai Result JSONParser rmResult : " +rmResult);
                    Toast.makeText(activity, rmResult, Toast.LENGTH_LONG).show();
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
