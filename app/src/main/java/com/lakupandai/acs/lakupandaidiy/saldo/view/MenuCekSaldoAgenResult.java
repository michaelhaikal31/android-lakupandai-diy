package com.lakupandai.acs.lakupandaidiy.saldo.view;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Erdy on 12/18/2018.
 */
public class MenuCekSaldoAgenResult extends HomeActivity {

    private TextView txtNameCekSaldoAgenResult, txtUsernameCekSaldoAgenResult, txtNoRekCekSaldoAgenResult, txtSaldoCekSaldoAgenResult;
    private Activity activity;
    String rmResult, titleMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_cek_saldo_agen_result);
        initUI();
    }

    private void initUI() {
        activity = this;
        txtNameCekSaldoAgenResult = findViewById(R.id.txtNameCekSaldoAgenResult);
        txtNoRekCekSaldoAgenResult=findViewById(R.id.txtNoRekCekSaldoAgenResult);
        txtUsernameCekSaldoAgenResult=findViewById(R.id.txtUsernameCekSaldoAgenResult);
        txtSaldoCekSaldoAgenResult=findViewById(R.id.txtSaldoCekSaldoAgenResult);

        Bundle bundle = getIntent().getExtras();
        rmResult = bundle.getString(ConstantTransaction.BUNDLE_RM);
        titleMenu = bundle.getString(ConstantTransaction.BUNDLE_TITLE);
        setResponseValue(rmResult);

    }

    private void setResponseValue(String rmResult) {
        try {
            JSONObject json = new JSONObject(rmResult);
            String rm = json.getString("RM");
            System.out.println("Value RM " + rm);
            JSONObject jsonRM = new JSONObject(rm);
            JSONArray arrayData = jsonRM.getJSONArray("data");
            System.out.println("arrayData " + arrayData);

            JSONArray jsonArray = new JSONArray(arrayData.toString());
            HashMap<String, String> list = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = jsonObject1.optString("header");
                String value2 = jsonObject1.optString("value");
                System.out.println("Value 1 " + value1);
//            System.out.println("Value 2 " + value2);
                list.put(value1, value2);
            }


            System.out.println(list.get("Layanan"));
            System.out.println(list.get("Tanggal"));
            System.out.println(list.get("Msisdn"));
            System.out.println(list.get("Balance"));

            txtSaldoCekSaldoAgenResult.setText(list.get("Balance"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
