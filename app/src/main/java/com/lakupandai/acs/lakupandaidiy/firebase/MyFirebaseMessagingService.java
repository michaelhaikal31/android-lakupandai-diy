package com.lakupandai.acs.lakupandaidiy.firebase;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.login.view.MenuLogin;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.util.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Erdy on 12/06/2018.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    String title, message, image;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @SuppressLint("WrongThread")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        Log.d(TAG, "From: " + remoteMessage.getFrom());
        JSONObject object = new JSONObject();
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            String js = remoteMessage.getData().toString();

            Map<String, String> params = remoteMessage.getData();
            object = new JSONObject(params);
            Log.e("JSON_OBJECT", object.toString());
//            new generatePictureStyleNotification(getApplicationContext(), "Testing", object.toString(), "https://orig00.deviantart.net/d2ca/f/2018/161/1/0/devil_may_cry_5_by_nerojinkazama-dce14c3.jpg").execute();
//            sendNotification(object);
            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }
        try {
            title =  object.getString("title");
            message = object.getString("message");
            image = object.getString("image");
            JSONArray jsonArray = new JSONArray();
            final JSONObject notifikasi = new JSONObject();
            notifikasi.put("image",image);
            notifikasi.put("agent",object.getString("agent"));
            notifikasi.put("title",title);
            notifikasi.put("message",message);
            notifikasi.put("rrn",object.getString("rrn"));
            notifikasi.put("crtime",object.getString("crtime"));
            if(DeviceSession.getJsonNotif(getApplicationContext()).isEmpty()){
                jsonArray.put(notifikasi);
                DeviceSession.setJsonNotif(getApplicationContext(),jsonArray.toString());
                DeviceSession.setNewNotif(getApplicationContext(),true);
                System.out.println("asd new notif "+jsonArray);
            }else{
                jsonArray = new JSONArray(DeviceSession.getJsonNotif(getApplicationContext()));
                System.out.println("asd json array tersimpan "+jsonArray.toString());
                jsonArray.put(notifikasi);
                DeviceSession.setJsonNotif(getApplicationContext(),jsonArray.toString());
                DeviceSession.setNewNotif(getApplicationContext(),true);
            }
        } catch (JSONException e) {
            image = "";
            System.out.println("asd JSONexception[MyFirebaseMessagingService] "+e.getMessage());
//            e.printStackTrace();
        }catch (Exception e){
            System.out.println("asd exception[MyFirebaseMessagingService] "+e.getMessage());
        }


        new GeneratePictureStyleNotification(getApplicationContext(), title, message, image).execute();

        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            Intent intentA = null;
//
//            if(status==2){
//                intentA = new Intent(this, this.getClass()); ///harus ke Notifikasi Activity
//            }else if(status==1){
//                intentA = new Intent(this, this.getClass());
//            }else{
//                intentA = new Intent(this, MenuLogin.class);
//            }
//
//            PendingIntent pi = PendingIntent.getActivity(this, 0, intentA, PendingIntent.FLAG_UPDATE_CURRENT);
//
//            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//            NotificationCompat.Builder notificationBuilder =
//                    new NotificationCompat.Builder(this, remoteMessage.getNotification().getBody())
//                            .setSmallIcon(R.drawable.logodjulie)
//                            .setContentTitle("Pesan Baru")
//                            .setContentText("Ketuk untuk melihat Pesan.")
//                            .setAutoCancel(true)
//                            .setSound(defaultSoundUri);
//            if(status==1){
//
//            }else{
//                notificationBuilder.setContentIntent(pi)
//                        .addAction(R.drawable.logodjulie, "View", pi);
//            }
//
//            NotificationManager notificationManager =
//                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//            // Since android Oreo notification channel is needed.
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                NotificationChannel channel = new NotificationChannel("Pesan Baru",
//                        "Channel human readable title",
//                        NotificationManager.IMPORTANCE_DEFAULT);
//                notificationManager.createNotificationChannel(channel);
//            }
//
//            notificationManager.notify(0, notificationBuilder.build());
//        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        // [START dispatch_job]
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(MyJobService.class)
//                .setTag("my-job-tag")
//                .build();
//        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     */
//    private void sendNotification(JSONObject messageBody) {
//        Intent intent = new Intent(this, MyFirebaseMessagingService.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
////                PendingIntent.FLAG_ONE_SHOT);
//
////        JSONObject jsonBody = new JSONObject(messageBody);
//        String channelId = "Pesan Baru";
//        String text = null;
//        String title = null;
//        String ClassName = this.getClass().getSimpleName();
//        try {
//            title = messageBody.getString("title");
//            text = messageBody.getString("text");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        Intent intentA = null;
//        System.out.println("THIS : "+ClassName+" "+this.getApplicationContext());
//
//        if(status==2){
//            intentA = new Intent(this, this.getClass()); ///harus ke Notifikasi Activity
//        }else if(status==1){
//            intentA = new Intent(this, this.getClass());
//        }else{
//            intentA = new Intent(this, MenuLogin.class);
//        }
//
//        PendingIntent pi = PendingIntent.getActivity(this, 0, intentA, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
////        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MenuLogin.class), 0);
////        Uri defaultSoundUri=Uri.parse("android.resource://"+getPackageName()+"/raw/yogya");
//        NotificationCompat.Builder notificationBuilder =
//                new NotificationCompat.Builder(this, channelId)
//                        .setSmallIcon(R.drawable.logodjulie)
//                        .setContentTitle("Pesan Baru")
//                        .setContentText("Ketuk untuk melihat Pesan.")
//                        .setAutoCancel(true)
//                        .setSound(defaultSoundUri);
//        if(status==1){
//
//        }else{
//            notificationBuilder.setContentIntent(pi)
//                    .addAction(R.drawable.logodjulie, "View", pi);
//        }
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        // Since android Oreo notification channel is needed.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel(channelId,
//                    "Channel human readable title",
//                    NotificationManager.IMPORTANCE_DEFAULT);
//            notificationManager.createNotificationChannel(channel);
//        }
//
//        notificationManager.notify(0, notificationBuilder.build());
//    }

    public class GeneratePictureStyleNotification extends AsyncTask<String, Void, Bitmap> {

        private Context mContext;
        private String title, message, imageUrl;

        public GeneratePictureStyleNotification(Context context, String title, String message, String imageUrl) {
            super();
            this.mContext = context;
            this.title = title;
            this.message = message;
            this.imageUrl = imageUrl;
        }

        @Override
        protected Bitmap doInBackground(String... params) {

            //INI UNTUK MENAMPILKAN GAMBAR BESAR DI NOTIFIKASI
//
//            InputStream in;
//            try {
//                URL url = new URL(this.imageUrl);
//                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//                connection.setDoInput(true);
//                connection.connect();
//                in = connection.getInputStream();
//                Bitmap myBitmap = BitmapFactory.decodeStream(in);
//                return myBitmap;
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            return null;
        }


        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            Intent intent = new Intent(mContext, MyFirebaseMessagingService.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            Intent intent = new Intent(mContext, MyOpenableActivity.class);
//            intent.putExtra("key", "value");
//            PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 100, intent, PendingIntent.FLAG_ONE_SHOT);
            String channelId = "Pesan Baru";
            Intent intentA = null;

            if(Constant.status==2){
                intentA = new Intent(mContext, this.getClass()); ///harus ke Notifikasi Activity
            }else if(Constant.status==1){
                intentA = new Intent(mContext, this.getClass());
            }else{
                intentA = new Intent(mContext, MenuLogin.class);
            }

            PendingIntent pi = PendingIntent.getActivity(mContext, 0, intentA, PendingIntent.FLAG_UPDATE_CURRENT);
            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

//            NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
            Notification.Builder notif = new Notification.Builder(mContext)
//                    .setContentIntent(pendingIntent)
                    .setContentIntent(pi)
                    //.addAction(R.drawable.logodiy, "View", pi)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.ic_logo_bpd_diy)
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.logodiy))
                    //.setLargeIcon(result)
                    //.setStyle(new Notification.BigPictureStyle().bigPicture(result))
                    .setSound(defaultSoundUri);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
            {
                notif.setSmallIcon(R.drawable.ic_logo_bpd_diy);
                notif.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_logo_bpd_diy));
            }
//            if(status==1){
//
//            }else{
//                notif.setContentIntent(pi)
//                        .addAction(R.drawable.logodjulie, "View", pi);
//            }

            NotificationManager notificationManager =
                    (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

            // Since android Oreo notification channel is needed.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
            {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(true);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                assert notificationManager != null;
                notif.setChannelId(NOTIFICATION_CHANNEL_ID);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            notificationManager.notify(0, notif.build());
        }
    }
}
