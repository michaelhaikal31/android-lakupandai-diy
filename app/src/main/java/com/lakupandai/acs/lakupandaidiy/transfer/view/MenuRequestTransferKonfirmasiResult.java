package com.lakupandai.acs.lakupandaidiy.transfer.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.saldo.view.MenuCekSaldo;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Erdy on 12/21/2018.
 */
public class MenuRequestTransferKonfirmasiResult extends HomeActivity {
    Activity activity;
    Button llHomeMenuCekStatusTerakhirResult;
    TextView txtValueLayanan, txtValueNama, txtValueNoHP, txtValueNamaKe, txtValueNoRek, txtValueRekTujuan, txtValueJumlah, txtValueNoRef;
    String rmResult, titleMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_request_transfer_konfirmasi_result);
        initUI();
    }

    private void initUI() {
        activity = this;

        txtValueNama = findViewById(R.id.txtValueNama);
        txtValueLayanan = findViewById(R.id.txtValueLayanan);
        txtValueNoHP = findViewById(R.id.txtValueNoHP);
        txtValueNamaKe = findViewById(R.id.txtValueNamaKe);
        txtValueNoRek = findViewById(R.id.txtValueNoRek);
        txtValueRekTujuan = findViewById(R.id.txtValueRekTujuan);
        txtValueJumlah = findViewById(R.id.txtValueJumlah);
        llHomeMenuCekStatusTerakhirResult = findViewById(R.id.llHomeMenuCekStatusTerakhirResult);
        llHomeMenuCekStatusTerakhirResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();
        rmResult = bundle.getString(ConstantTransaction.BUNDLE_RM);
        titleMenu = bundle.getString(ConstantTransaction.BUNDLE_TITLE);

        setResponseValue(rmResult);
    }

    private void setResponseValue(String rmResult) {
        try {
            JSONObject jsonRM = new JSONObject(rmResult);
            JSONArray arrayData = jsonRM.getJSONArray("data");
            JSONArray jsonArray = new JSONArray(arrayData.toString());
            HashMap<String, String> list = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = jsonObject1.optString("header");
                String value2 = jsonObject1.optString("value");
                list.put(value1, value2);
            }


            String layanan = list.get("Layanan");
            String destinationAccount = list.get("No Rek. Penerima");
            String destinationName = list.get("Nama Penerima");
            String amount = list.get("Jumlah");
            String fromName = list.get("Nama Pengirim");
            String fromMsisdn = list.get("No HP Pengirim");
            String status = list.get("Keterangan");



            txtValueNama.setText(": " + fromName);
            txtValueNama.setTypeface(null, Typeface.BOLD);
            txtValueLayanan.setText(": " + layanan);
            txtValueLayanan.setTypeface(null, Typeface.BOLD);
            txtValueNoHP.setText(": " + fromMsisdn);
            txtValueNoHP.setTypeface(null, Typeface.BOLD);
            txtValueNamaKe.setText(": " + destinationName);
            txtValueNamaKe.setTypeface(null, Typeface.BOLD);
            txtValueNoRek.setText(": " + destinationAccount);
            txtValueNoRek.setTypeface(null, Typeface.BOLD);
            txtValueRekTujuan.setText(": " + destinationName);
            txtValueRekTujuan.setTypeface(null, Typeface.BOLD);
            txtValueJumlah.setText(": " + amount);
            txtValueJumlah.setTypeface(null, Typeface.BOLD);

        } catch (Exception e) {
            Function.showAlert(activity, e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
