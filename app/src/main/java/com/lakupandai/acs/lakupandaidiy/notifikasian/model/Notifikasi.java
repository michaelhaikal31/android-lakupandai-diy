package com.lakupandai.acs.lakupandaidiy.notifikasian.model;

/**
 * Created by Erdy on 12/05/2018.
 */
public class Notifikasi {
   private String title;
   private String tanggal;
   private String totalAmount;
   private String resi;
   private boolean alreadyRead;




    public Notifikasi(String title, String tanggal, String totalAmount, String resi, boolean alreadyRead) {
        this.title = title;
        this.tanggal = tanggal;
        this.totalAmount = totalAmount;
        this.resi = resi;
        this.alreadyRead = alreadyRead;
    }

    public Notifikasi() {
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public String getTitle() {
        return title;
    }

    public String getTanggal() {
        return tanggal;
    }



    public String getResi() {
        return resi;
    }

    public boolean isAlreadyRead() {
        return alreadyRead;
    }

    public void setAlreadyRead(boolean alreadyRead) {
        this.alreadyRead = alreadyRead;
    }


}
