package com.lakupandai.acs.lakupandaidiy.aktivasi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.sms.SmsReceiver;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONObject;

/**
 * Created by Erdy on 12/03/2018.
 */
public class MenuAktivasiRekBaruResponse extends HomeActivity implements SmsReceiver.SmsListener {
    private Activity activity;
    private TextView txtNoKTPMenuAktivasiRekBaruResponse, txtNamaMenuAktivasiRekBaruResponse, txtTglLahirMenuAktivasiRekBaruResponse, txtNoHPMenuAktivasiRekBaruResponse,txtResendotp_menu_aktivasi_rekbaru_response;
    private EditText etPinAgenMenuAktivasiRekBaruRespon, etTokenAgenMenuAktivasiRekBaruRespon;
    private Button btnKirimMenuAktivasiRekBaruResponse;
    private ImageView llHomeMenuAktivasiRekBaruResponse;
    public String noKTP, nama, nomorHP, pinAgen, tglLahir, noHPAgen, token, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();
    SharedPreferences shared;
    private TextView txtProgressBar;
    private TextView txtCaption1;
    private TextView txtCaption2;

    private ProgressBar progressBar;
    private CountDownTimer countDownTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_aktivasi_rek_baru_response);
        initUI();
        startCountdown();

    }
    private void startCountdown() {
        txtCaption1.setVisibility(View.VISIBLE);
        txtProgressBar.setVisibility(View.VISIBLE);
        txtCaption2.setTextColor(Color.parseColor("#808080"));
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int second = (int) ((millisUntilFinished / 1000));
                //txtProgressBar.setText(second);
                txtProgressBar.setText(String.valueOf(second));

            }

            @Override
            public void onFinish() {
//                tvResendOTP.setVisibility(View.VISIBLE);
//                tvResendOTP.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        new MenuKonfirmasiPembelianPulsa.executeResendOTP(activity).execute();
//                    }
//                });
                txtCaption1.setVisibility(View.GONE);
                txtProgressBar.setVisibility(View.INVISIBLE);
                txtProgressBar.setText("");
                txtCaption2.setText("Kirim ulang OTP");
                txtCaption2.setTextColor(getResources().getColor(R.color.colorPrimary));

                txtCaption2.setTypeface(null, Typeface.BOLD);
                txtCaption2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new MenuAktivasiRekBaruResponse.executeResendOTP().execute();
                        System.out.println("asd [menukonfirPembayaran  ressend otp clicked | data param body ] "+ DeviceSession.getTempRequestBody(MenuAktivasiRekBaruResponse.this));
                        startCountdown();
                    }
                });
            }
        };

        countDownTimer.start();

    }
    class executeResendOTP extends AsyncTask<String, Void, String> {
        Context context;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... result) {
            return jsonParser.HttpRequestPost(ConstantTransaction.URL, DeviceSession.getTempRequestBody(MenuAktivasiRekBaruResponse.this), ConstantTransaction.TimeOutConnection, MenuAktivasiRekBaruResponse.this);

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
//                    System.out.println("Nilai Result JSONParser rmResult : " +rmResult);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Function.showAlert(MenuAktivasiRekBaruResponse.this,"OTP berhasil dikirim, silahkan masukan OTP");
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void smsReceived(String msgFrom, String msgBody) {
//        System.out.println("From: " + msgFrom);
//        System.out.println("Body: " + msgBody);

//        handler.removeCallbacks(runnable);
//        spinnerDialog.dismissAllowingStateLoss();
//        String hasilRCSms = msgBody.substring(4, 6);
//        String hasilRMSms = msgBody.substring(7, msgBody.length());
//
//
//        if (hasilRCSms.equals(Constant.SUCCESS_CODE)) {
//            Intent i = new Intent(mContext, MenuHasilResponse.class);
//            Bundle bundle = new Bundle();
//            bundle.putString(Constant.BUNDLE_TITLE, Constant.BUNDLE_TITLE_REK_BARU);
//            bundle.putString(Constant.BUNDLE_RC, hasilRCSms);
//            bundle.putString(Constant.BUNDLE_RM, hasilRMSms);
//            i.putExtras(bundle);
//            startActivity(i);
//            finish();
//        } else {
//            Function.showAlert(mContext, hasilRMSms);
//        }
    }

    private void initUI() {
        activity = this;
        txtResendotp_menu_aktivasi_rekbaru_response = (TextView) findViewById(R.id.txtResendotp_menu_aktivasi_rekbaru_response);
        txtNoKTPMenuAktivasiRekBaruResponse = (TextView) findViewById(R.id.txtNoKTPMenuAktivasiRekBaruResponse);
        txtNamaMenuAktivasiRekBaruResponse = (TextView) findViewById(R.id.txtNamaMenuAktivasiRekBaruResponse);
        txtNoHPMenuAktivasiRekBaruResponse = (TextView) findViewById(R.id.txtNoHPMenuAktivasiRekBaruResponse);
        txtTglLahirMenuAktivasiRekBaruResponse = (TextView) findViewById(R.id.txtTglLahirMenuAktivasiRekBaruResponse);
        etPinAgenMenuAktivasiRekBaruRespon = (EditText) findViewById(R.id.etPinAgenMenuAktivasiRekBaruRespon);
        etTokenAgenMenuAktivasiRekBaruRespon = (EditText) findViewById(R.id.etTokenAgenMenuAktivasiRekBaruRespon);

        txtProgressBar = findViewById(R.id.txtProgressBar);
        txtCaption1 = findViewById(R.id.textView16);
        txtCaption2 = findViewById(R.id.textView17);

        Bundle bundle = getIntent().getExtras();
        noKTP = bundle.getString(ConstantTransaction.BUNDLE_NOKTP);
        nama = bundle.getString(ConstantTransaction.BUNDLE_NAMA);
        nomorHP = bundle.getString(ConstantTransaction.BUNDLE_NOHP);
        tglLahir = bundle.getString(ConstantTransaction.BUNDLE_TGL);
//        System.out.println(pinAgen);
        txtNoKTPMenuAktivasiRekBaruResponse.setText(": " + noKTP);
        txtNamaMenuAktivasiRekBaruResponse.setText(": " + nama);
        txtNoHPMenuAktivasiRekBaruResponse.setText(": " + nomorHP);
        txtTglLahirMenuAktivasiRekBaruResponse.setText(": " + tglLahir);

        btnKirimMenuAktivasiRekBaruResponse = (Button) findViewById(R.id.btnKirimMenuAktivasiRekBaruResponse);
        btnKirimMenuAktivasiRekBaruResponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinAgen = etPinAgenMenuAktivasiRekBaruRespon.getText().toString();
                if (pinAgen.length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else {
                    new executeAktivasiRekBaru(activity,etTokenAgenMenuAktivasiRekBaruRespon.getText().toString()).execute();
                }
            }
        });

        llHomeMenuAktivasiRekBaruResponse = (ImageView) findViewById(R.id.llHomeMenuAktivasiRekBaruResponse);
        llHomeMenuAktivasiRekBaruResponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    class executeAktivasiRekBaru extends AsyncTask<String, JSONObject, String> {
        Context context;
        private String tokens;
        executeAktivasiRekBaru(Context mContext,String tokens) {
            this.context = mContext;
            this.tokens = tokens;

        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                token = tokens;
                json.put(ConstantTransaction.MSISDN_AGEN_REK_BARU, noHPAgen);
                json.put(ConstantTransaction.TOKEN_REK_BARU, token);
                json.put(ConstantTransaction.PIN_REK_BARU, pinAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_REK_BARU);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Json Send: " + json.toString());
                System.out.println("Json encry "+resultEnc);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuAktivasiRekBaruResponse.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Function.showListenerOkDialogMessage(MenuAktivasiRekBaruResponse.this, getLayoutInflater(), rmResult, new Function.InputSenderDialogListener() {
                            @Override
                            public void onOK(String value) {
                                Intent intent = new Intent(MenuAktivasiRekBaruResponse.this, MainMenu.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void onCancel(String value) {

                            }
                        });
                    } else {
                        Function.showListenerOkDialogMessage(MenuAktivasiRekBaruResponse.this, getLayoutInflater(), rmResult, new Function.InputSenderDialogListener() {
                            @Override
                            public void onOK(String value) {
                                Intent intent = new Intent(MenuAktivasiRekBaruResponse.this, MainMenu.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void onCancel(String value) {

                            }
                        });
                    }
                } catch (Exception e) {
                    Function.showListenerOkDialogMessage(MenuAktivasiRekBaruResponse.this, getLayoutInflater(), e.getMessage()   , new Function.InputSenderDialogListener() {
                        @Override
                        public void onOK(String value) {
                            Intent intent = new Intent(MenuAktivasiRekBaruResponse.this, MainMenu.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onCancel(String value) {

                        }
                    });
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
