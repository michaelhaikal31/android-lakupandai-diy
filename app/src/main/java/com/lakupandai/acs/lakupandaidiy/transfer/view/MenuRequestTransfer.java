package com.lakupandai.acs.lakupandaidiy.transfer.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.NumberTextWatcherForThousand;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONObject;

/**
 * Created by Erdy on 12/07/2018.
 */
public class MenuRequestTransfer extends HomeActivity {
    private Activity activity;
    private ImageView llHomeMenuRequestTransfer;
    private EditText etNomorHPMenuRequestTransfer, etNoRekMenuRequestTransfer, etNominalMenuRequestTransfer;
    private Button btnKirimMenuRequestTransfer;
    private String current;
    private String noHPAgen, noHpNasabah, nominal, noRek, token, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_request_transfer);
        initUI();
    }

    private void initUI() {
        activity = this;

        llHomeMenuRequestTransfer = (ImageView) findViewById(R.id.llHomeMenuRequestTransfer);
        llHomeMenuRequestTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etNominalMenuRequestTransfer = (EditText) findViewById(R.id.etNominalMenuRequestTransfer);
        etNominalMenuRequestTransfer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (!s.toString().equals(current)) {
                        etNominalMenuRequestTransfer.removeTextChangedListener(this);
                        String cleanString = s.toString().replaceAll("[.]", "");
                        String formatted = null;
                        if (!cleanString.isEmpty()) {
                            double parsed = Double.parseDouble(cleanString);

                            formatted = NumberTextWatcherForThousand.formatAmount(parsed);
                        }
                        current = formatted;
                        etNominalMenuRequestTransfer.setText(formatted);
                        if (!cleanString.isEmpty()) {
                            etNominalMenuRequestTransfer.setSelection(formatted.length());
                        }
                        etNominalMenuRequestTransfer.addTextChangedListener(this);
                    }
                } catch (Exception e) {
                    Toast.makeText(activity, ConstantError.ALERT_NOMINAL_INPUT_FALSE, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etNomorHPMenuRequestTransfer = (EditText) findViewById(R.id.etNomorHPMenuRequestTransfer);
        etNoRekMenuRequestTransfer = (EditText) findViewById(R.id.etNoRekMenuRequestTransfer);


        btnKirimMenuRequestTransfer = (Button) findViewById(R.id.btnKirimMenuRequestTransfer);
        btnKirimMenuRequestTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etNominalMenuRequestTransfer.getText().toString().isEmpty() || etNomorHPMenuRequestTransfer.getText().toString().isEmpty() || etNoRekMenuRequestTransfer.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etNomorHPMenuRequestTransfer.getText().toString().length() < 10) {
                    Function.showAlert(activity, ConstantError.ALERT_NUMBER_LESS_THAN_10);
                } else if (etNoRekMenuRequestTransfer.getText().toString().length() < 10) {
                    Function.showAlert(activity, ConstantError.ALERT_NO_REK_LESS_THAN_10);
                } else if (Integer.parseInt(Function.removePoint(etNominalMenuRequestTransfer.getText().toString())) < 1) {
                    Function.showAlert(activity, ConstantError.ALERT_NOMINAL_LESS_THAN_1);
                } else {
                    nominal = Function.removePoint(etNominalMenuRequestTransfer.getText().toString());
                    noHpNasabah = etNomorHPMenuRequestTransfer.getText().toString();
                    noRek = etNoRekMenuRequestTransfer.getText().toString();
                    new executeRequestTransfer(activity).execute();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    class executeRequestTransfer extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeRequestTransfer(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN_REQUEST_TRANSFER, noHPAgen);
                json.put(ConstantTransaction.MSISDN_NASABAH_REQUEST_TRANSFER, noHpNasabah);
                json.put(ConstantTransaction.NOMINAL_REQUEST_TRANSFER, nominal);
                json.put(ConstantTransaction.NO_REK_REQUEST_TRANSFER, noRek);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_REQUEST_TRANSFER);
                Log.v(ConstantTransaction.LOG_REQUEST_TRANSFER, ConstantTransaction.MESSAGE_LOG_REQUEST_TRANSFER);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
//                System.out.println(resultDec);
                System.out.println("Response Send " + resultEnc);
                System.out.println("Response Send Decrypt " + resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuRequestTransfer.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt [MenuRequestTransfer] " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    token = jsonObject.getString(ConstantTransaction.TOKEN_REQUEST_TRANSFER);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Intent i = new Intent(activity, MenuRequestTransferKonfirmasi.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.BUNDLE_TOKEN, token);
                        bundle.putString(ConstantTransaction.BUNDLE_RC, rcResult);
                        bundle.putString(ConstantTransaction.BUNDLE_RM, rmResult);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
