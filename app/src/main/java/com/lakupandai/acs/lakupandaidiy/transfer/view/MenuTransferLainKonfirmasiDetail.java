package com.lakupandai.acs.lakupandaidiy.transfer.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponse;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponseNew;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.sms.SmsReceiver;
import com.lakupandai.acs.lakupandaidiy.tariktunai.view.MenuKonfirmasiTarikTunaiDetail;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.NotificationHelper;
import com.lakupandai.acs.lakupandaidiy.util.SpinnerDialog;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Erdy on 12/07/2018.
 */
public class MenuTransferLainKonfirmasiDetail extends HomeActivity implements SmsReceiver.SmsListener{
    private Activity activity;
    private TextView txtIsiMenuRequestTransferKonfirmasi;
    private EditText etPinAgenMenuTransferKonfirmasiDetail;
    private Button btnKirimMenuTransferKonfirmasiDetail;
    private ImageView llHomeMenuTransferKonfirmasiDetail, llMenuTransferKonfirmasiDetailPopup, llBackMenuTransferKonfirmasiDetail;
    private String pinAgen, rmBundle, tokenBundle, noHPAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();
    TextView txtValueLayanan, txtValueNama, txtValueNoHP, txtValueNamaKe, txtValueNoHpKe, txtValueJumlah;

    private Runnable runnable;
    private Handler handler;
    SpinnerDialog spinnerDialog;
    SmsReceiver receiver;
    String statusSMSShared;
    SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_transfer_lain_konfirmasi_detail);
        initUI();
    }


    @Override
    protected void onStop() {
        super.onStop();
        // unregisterReceiver(receiver);
    }


    @Override
    public void smsReceived(String msgFrom, String msgBody) {
//        System.out.println("From: " + msgFrom);
//        System.out.println("Body: " + msgBody);

//        handler.removeCallbacks(runnable);
//        spinnerDialog.dismissAllowingStateLoss();
//        String hasilRCSms = msgBody.substring(4, 6);
//        String hasilRMSms = msgBody.substring(7, msgBody.length());
//
//        if (hasilRCSms.equals(Constant.SUCCESS_CODE)) {
//            Intent i = new Intent(mContext, MenuHasilResponse.class);
//            Bundle bundle = new Bundle();
//            bundle.putString(Constant.BUNDLE_TITLE, Constant.BUNDLE_TITLE_TRANSFER);
//            bundle.putString(Constant.BUNDLE_RC, hasilRCSms);
//            bundle.putString(Constant.BUNDLE_RM, hasilRMSms);
//            i.putExtras(bundle);
//            startActivity(i);
//            finish();
//        } else {
//            Function.showAlert(mContext, hasilRMSms);
//        }
    }

    private void initUI() {
        activity = this;

//        final IntentFilter filter = new IntentFilter();
//        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
//        filter.setPriority(1000);
//        receiver = new SmsReceiver(this);
//        registerReceiver(receiver, filter);

        txtValueNama = findViewById(R.id.txtValueNama);
        txtValueLayanan = findViewById(R.id.txtValueLayanan);
        txtValueNoHP = findViewById(R.id.txtValueNoHP);
        txtValueNamaKe = findViewById(R.id.txtValueNamaKe);
        txtValueNoHpKe = findViewById(R.id.txtValueNoHpKe);
        txtValueJumlah = findViewById(R.id.txtValueJumlah);

        Bundle bundle = getIntent().getExtras();
        rmBundle = bundle.getString(ConstantTransaction.BUNDLE_RM);
        tokenBundle = bundle.getString(ConstantTransaction.BUNDLE_TOKEN);
        setResponseValue(rmBundle);

        llHomeMenuTransferKonfirmasiDetail = (ImageView) findViewById(R.id.llHomeMenuTransferKonfirmasiDetail);
        llHomeMenuTransferKonfirmasiDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etPinAgenMenuTransferKonfirmasiDetail = (EditText) findViewById(R.id.etPinAgenMenuTransferKonfirmasiDetail);
//        txtIsiMenuRequestTransferKonfirmasi = (TextView) findViewById(R.id.txtIsiMenuRequestTransferKonfirmasi);
//        txtIsiMenuRequestTransferKonfirmasi.setText(rmBundle);


        btnKirimMenuTransferKonfirmasiDetail = (Button) findViewById(R.id.btnKirimMenuTransferKonfirmasiDetail);
        btnKirimMenuTransferKonfirmasiDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etPinAgenMenuTransferKonfirmasiDetail.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etPinAgenMenuTransferKonfirmasiDetail.getText().toString().length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else {
                    pinAgen = etPinAgenMenuTransferKonfirmasiDetail.getText().toString();
                    new executeTransferKonfirmasiDetail(activity).execute();
                }
            }
        });
    }

    private void setResponseValue(String rmBundle) {

        try {
            JSONObject jsonRM = new JSONObject(rmBundle);
            JSONArray arrayData = jsonRM.getJSONArray("data");
            JSONArray jsonArray = new JSONArray(arrayData.toString());
            HashMap<String, String> list = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = jsonObject1.optString("header");
                String value2 = jsonObject1.optString("value");
                list.put(value1, value2);
            }
            String layanan = list.get("Layanan");
            String destinationMsisdn = list.get("No Rek. Penerima");
            String destinationName = list.get("Nama Penerima");
            String amount = list.get("Jumlah");
            String fromName = list.get("Nama Pengirim");
            String fromMsisdn = list.get("No HP Pengirim");
            String status = list.get("Status");
            //String ref = list.get("Ref");
            txtValueNama.setText(": " + fromName);
            txtValueNama.setTypeface(null, Typeface.BOLD);
            txtValueLayanan.setText(": " + layanan);
            txtValueLayanan.setTypeface(null, Typeface.BOLD);
            txtValueNoHP.setText(": " + fromMsisdn);
            txtValueNoHP.setTypeface(null, Typeface.BOLD);
            txtValueNamaKe.setText(": " + destinationName);
            txtValueNamaKe.setTypeface(null, Typeface.BOLD);
            txtValueNoHpKe.setText(": " + destinationMsisdn);
            txtValueNoHpKe.setTypeface(null, Typeface.BOLD);
            txtValueJumlah.setText(": "+amount);
            txtValueJumlah.setTypeface(null, Typeface.BOLD);
//            txtValueNoRef.setText(": " + ref);
//            txtValueNoRef.setTypeface(null, Typeface.BOLD);

        } catch (Exception e) {
            Function.showAlert(activity, e.getMessage()+"o_0");
        }
    }


    class executeTransferKonfirmasiDetail extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeTransferKonfirmasiDetail(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN_TRANSFER_KONFIRMASI_DETAIL, noHPAgen);
                json.put(ConstantTransaction.TOKEN_TRANSFER_KONFIRMASI_DETAIL, tokenBundle);
                json.put(ConstantTransaction.PIN_TRANSFER_KONFIRMASI_DETAIL, pinAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_TRANSFER_KONFIRMASI_DETAIL_LAKUPANDAI);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuTransferLainKonfirmasiDetail.this);
                System.out.println("Response Send "+resultEnc);
                System.out.println("Response Send Decrypt "+resultDec);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
//                shared = getSharedPreferences(Constant.MyPref, MODE_PRIVATE);
//                statusSMSShared = (shared.getString(Constant.SETTING_SMS, ""));
//                if (statusSMSShared.equals("0")) {
//                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
//                } else if (statusSMSShared.equals("1")) {
//                    spinnerDialog = SpinnerDialog.createInstance(Constant.LOADING_SMS, Constant.PESAN_SMS);
//                    spinnerDialog.show(getFragmentManager(), Constant.FRAGMENT_SMS);
//                    handler = new Handler();
//                    runnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            if (spinnerDialog.isVisible()) {
//                                spinnerDialog.dismissAllowingStateLoss();
//                                Toast.makeText(getApplicationContext(), Constant.FAIL_RECEIVE_SMS, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    };
//                    handler.postDelayed(runnable, Constant.TIMEOUT_SMS);
//                    SendSms.splitSMS(resultEnc, mContext);
//                }
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive "+result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt [MenuTransferLainKonfirmasiDetail] "+resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        DeviceSession.setNewNotif(MenuTransferLainKonfirmasiDetail.this, true);
                        NotificationHelper.CreateNotification(MenuTransferLainKonfirmasiDetail.this, "Transfer antar nasabah berhasil","Tekan untuk melihat transaksi di kotak masuk");

                        JSONObject jsonDATA = new JSONObject(rmResult);
                        String data = jsonDATA.getString(ConstantTransaction.DATA);
                        Constant.dataResult = new JSONArray(data);
                        Intent i = new Intent(activity, MenuHasilResponseNew.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.BUNDLE_TITLE, ConstantTransaction.BUNDLE_TITLE_TRANSFER);
                        bundle.putString(ConstantTransaction.BUNDLE_RC, rcResult);
                        bundle.putString(ConstantTransaction.BUNDLE_RM, rmResult);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
