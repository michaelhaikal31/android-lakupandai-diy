package com.lakupandai.acs.lakupandaidiy.saldo.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponseNew;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponsePrintShare;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponsePulsa;
import com.lakupandai.acs.lakupandaidiy.sms.SmsReceiver;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Erdy on 12/04/2018.
 */
public class MenuCekStatusTerakhir extends HomeActivity implements SmsReceiver.SmsListener {

    private static Activity activity;
    private EditText etNomorHPMenuCekStatusTerakhir, etPinAgenMenuCekStatusTerakhir;
    private Button btnKirimMenuCekStatusTerakhir;
    private ImageView llHomeMenuCekStatusTerakhir;
    private String noHPAgen, noHpNasabah, pin, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_cek_status_terakhir);
        initUI();
    }

    private void initUI() {
        activity = this;
        etNomorHPMenuCekStatusTerakhir = (EditText) findViewById(R.id.etNomorHPMenuCekStatusTerakhir);
        etPinAgenMenuCekStatusTerakhir = (EditText) findViewById(R.id.etPinAgenMenuCekStatusTerakhir);

        llHomeMenuCekStatusTerakhir = (ImageView) findViewById(R.id.llHomeMenuCekStatusTerakhir);
        llHomeMenuCekStatusTerakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnKirimMenuCekStatusTerakhir = (Button) findViewById(R.id.btnKirimMenuCekStatusTerakhir);
        btnKirimMenuCekStatusTerakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etNomorHPMenuCekStatusTerakhir.getText().toString().isEmpty() || etPinAgenMenuCekStatusTerakhir.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etNomorHPMenuCekStatusTerakhir.getText().toString().length() < 10) {
                    Function.showAlert(activity, ConstantError.ALERT_NUMBER_LESS_THAN_10);
                } else if (etPinAgenMenuCekStatusTerakhir.getText().toString().length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else {
                    noHpNasabah = etNomorHPMenuCekStatusTerakhir.getText().toString();
                    pin = etPinAgenMenuCekStatusTerakhir.getText().toString();
                    new executeCekStatusTerakhir(activity).execute();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregisterReceiver(receiver);
    }

    @Override
    public void smsReceived(String msgFrom, String msgBody) {
        System.out.println("From: " + msgFrom);
        System.out.println("Body: " + msgBody);
    }


    class executeCekStatusTerakhir extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeCekStatusTerakhir(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_NASABAH_CEK_STATUS_TERAKHIR, noHpNasabah);
                json.put(ConstantTransaction.MSISDN_AGEN_CEK_STATUS_TERAKHIR, noHPAgen);
                json.put(ConstantTransaction.PIN_CEK_STATUS_TERAKHIR, pin);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_CEK_STATUS_TERAKHIR);

                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Response Send " + resultEnc);
                System.out.println("Response Send Decrypt " + resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuCekStatusTerakhir.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
//                t=new tes(mContext);
//                t.show();
//                SendSms.splitSMS(resultEnc,mContext);
//                 // Function.alertSms(mContext,resultEnc);
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
//                t.dismiss();

//                shared = getSharedPreferences(Constant.MyPref, MODE_PRIVATE);
//                statusSMSShared = (shared.getString(Constant.SETTING_SMS, ""));
//                if (statusSMSShared.equals("0")) {
//                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
//                } else if (statusSMSShared.equals("1")) {
//                    spinnerDialog = SpinnerDialog.createInstance(Constant.LOADING_SMS, Constant.PESAN_SMS);
//                    spinnerDialog.show(getFragmentManager(), Constant.FRAGMENT_SMS);
//                    handler = new Handler();
//                    runnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            if (spinnerDialog.isVisible()) {
//                                spinnerDialog.dismissAllowingStateLoss();
//                                Toast.makeText(getApplicationContext(), Constant.FAIL_RECEIVE_SMS, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    };
//                    handler.postDelayed(runnable, Constant.TIMEOUT_SMS);
//                    SendSms.splitSMS(resultEnc, mContext);
//                }

            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Intent i = new Intent(activity, MenuHasilResponsePulsa.class);
                        Bundle bundle = new Bundle();
                        JSONObject jsonDATA = new JSONObject(rmResult);
                        String data = jsonDATA.getString(ConstantTransaction.DATA);
                        Constant.dataResult = new JSONArray(data);
                        Constant.layanan_title="Status Terakhir";
                        i.putExtra("notif",false);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
