package com.lakupandai.acs.lakupandaidiy.pembelian.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.adapter.ResponAdapter;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponsePulsa;
import com.lakupandai.acs.lakupandaidiy.pembayaran.view.MenuKonfirmasiPembayaran;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.setortunai.model.Respon;
import com.lakupandai.acs.lakupandaidiy.tariktunai.view.MenuKonfirmasiTarikTunai;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.NotificationHelper;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

public class MenuKonfirmasiPembelianPulsa extends HomeActivity {

    private Activity activity;
    private Button btnKirimPembelian;
    private ListView listRespon;
    private EditText etPin, etOTPPembelian;
    private ImageView llHomeMenuKonfirmasiPembelian;
    private String noHPAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    private TableRow tblRowMenuKonfirmasiTarikTunai1, trResendOTP;
    private TextView tvMetode, tvResendOTP;
    private String otp;
    ProgressDialog progressDialog;
    private TextView txtProgressBar;
    private TextView txtCaption1;
    private TextView txtCaption2;
    private ProgressBar progressBar;
    private CountDownTimer countDownTimer;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    ArrayList<Respon> myListItems = new ArrayList<>();
    ResponAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_konfirmasi_pembelian);
        initUI();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregisterReceiver(receiver);
    }

    private void initUI() {
        activity = this;
        etPin = (EditText) findViewById(R.id.etPin);
        etOTPPembelian = (EditText) findViewById(R.id.etOTPPembelian);
        listRespon = (ListView) findViewById(R.id.listRespon);

//        tvResendOTP = findViewById(R.id.tvResendOTP);
//        tvResendOTP.setVisibility(View.INVISIBLE);
//        tvResendOTP.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                new MenuKonfirmasiPembelianPulsa.executeResendOTP(activity).execute();
//            }
//        });
        txtProgressBar = findViewById(R.id.txtProgressBar);
        txtCaption1 = findViewById(R.id.textView16);
        txtCaption2 = findViewById(R.id.textView17);

        JSONObject jsonC;
        myListItems = new ArrayList<>();
        System.out.println("asd [arrayKonfirmasipembelianPulsa] "+Constant.dataResult.toString());
        for (int a = 0; a < Constant.dataResult.length(); a++) {
            try {
                jsonC = Constant.dataResult.getJSONObject(a);
                String convertValue;
                if (jsonC.getString("header").equals("Desc")) {
                    if (jsonC.getString("value").contains(",")) {
                        String value = jsonC.getString("value");
//                        System.out.println("Value 1 "+value);
//                        String[] value_split = value.split(Pattern.quote(","));
//                        convertValue = value_split[1].substring(1);
//                        System.out.println("Value 2 "+convertValue);
                        convertValue = value;
                    } else {
                        String value = jsonC.getString("value");
                        convertValue = value;
                    }

                    //Respon respon = new Respon(jsonC.getString("header"),convertValue);

                    //myListItems.add(new Respon(jsonC.getString("header"), convertValue.replace("]","")));

                } else if (jsonC.getString("header").equals("otp")) {
                    otp = jsonC.getString("value");
                } else {
                    myListItems.add(new Respon(jsonC.getString("header"), jsonC.getString("value")));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter = new ResponAdapter(getApplicationContext(), myListItems);
        listRespon.setAdapter(adapter);

        tvMetode = findViewById(R.id.tvMetode);
        tvMetode.setText("Metode Pembelian\n" + Constant.metode);
//        tblRowMenuKonfirmasiTarikTunai1 = findViewById(R.id.tblRowMenuKonfirmasiTarikTunai1);
//        trResendOTP = findViewById(R.id.trResendOTP);
        if (Constant.metode.equals("Debit")) {
            etOTPPembelian.setVisibility(View.VISIBLE);
            txtCaption2.setVisibility(View.VISIBLE);
            txtCaption1.setVisibility(View.VISIBLE);
            txtProgressBar.setVisibility(View.VISIBLE);
            startCountdown();
        } else {
            etOTPPembelian.setVisibility(View.GONE);
            txtCaption2.setVisibility(View.GONE);
            txtCaption1.setVisibility(View.GONE);
            txtProgressBar.setVisibility(View.GONE);
        }

        btnKirimPembelian = (Button) findViewById(R.id.btnKirimPembelian);
        btnKirimPembelian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etPin.getText().toString().length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else if (etOTPPembelian.getText().toString().length() < 6 && Constant.metode.equals("debit")) {
                    Function.showAlert(activity, ConstantError.ALERT_TOKEN_LESS_THAN_6);
                } else {
                    new MenuKonfirmasiPembelianPulsa.executeKonfirmasiPembelian(activity, etOTPPembelian.getText().toString(), etPin.getText().toString()).execute();
                }
            }
        });

        llHomeMenuKonfirmasiPembelian = (ImageView) findViewById(R.id.llHomeMenuKonfirmasiPembelian);
        llHomeMenuKonfirmasiPembelian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }
    private void startCountdown() {
        txtCaption1.setVisibility(View.VISIBLE);
        txtProgressBar.setVisibility(View.VISIBLE);
        txtCaption2.setTextColor(Color.parseColor("#808080"));
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int second = (int) ((millisUntilFinished / 1000));
                //txtProgressBar.setText(second);
                txtProgressBar.setText(String.valueOf(second));
            }

            @Override
            public void onFinish() {
                txtCaption1.setVisibility(View.GONE);
                txtProgressBar.setVisibility(View.INVISIBLE);
                txtProgressBar.setText("");
                txtCaption2.setText("Kirim ulang OTP");
                txtCaption2.setTextColor(getResources().getColor(R.color.colorPrimary));

                txtCaption2.setTypeface(null, Typeface.BOLD);
                txtCaption2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new executeResendOTP().execute();
                        System.out.println("asd [menukonfirPembayaran  ressend otp clicked | data param body ] "+ DeviceSession.getTempRequestBody(MenuKonfirmasiPembelianPulsa.this));
                        startCountdown();
                    }
                });

            }
        };

        countDownTimer.start();

    }

    class executeKonfirmasiPembelian extends AsyncTask<String, JSONObject, String> {
        Context context;
        String otp;
        String pin;


        private executeKonfirmasiPembelian(Context mContext, String otp, String pin) {
            this.context = mContext;
            this.otp = otp;
            this.pin = pin;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                if (Constant.metode.equals("Debit")) {
                    json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                    json.put(ConstantTransaction.PIN_PEMBAYARAN, pin);
                    json.put(ConstantTransaction.OTP_PEMBAYARAN, otp);
                    json.put(ConstantTransaction.KODE_PRODUK, com.lakupandai.acs.lakupandaidiy.pembelian.view.MenuPembelian.productCode);
                    json.put(ConstantTransaction.TIPE, (Constant.metode).toLowerCase());
                    json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_KONFIRMASI_PEMBELIAN);
                } else {
                    json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                    json.put(ConstantTransaction.PIN_PEMBAYARAN, pin);
                    json.put(ConstantTransaction.KODE_PRODUK, MenuPembelian.productCode);
                    json.put(ConstantTransaction.OTP_PEMBAYARAN, MenuPembelian.otp);
                    json.put(ConstantTransaction.TIPE, (Constant.metode).toLowerCase());
                    json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_KONFIRMASI_PEMBELIAN);

                }
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Response Send " + resultEnc);
                System.out.println("Response Send Decrypt " + resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuKonfirmasiPembelianPulsa.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            System.out.println("asd response [pembelian] " + tripleDES.decrypt(result));

            try {
                resultfromJsonDecrypt = tripleDES.decrypt(result);
                jsonObject = new JSONObject(resultfromJsonDecrypt);
                rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);

                if (rcResult.contains(ConstantTransaction.CONNECTION_LOST)) {
                    Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
                } else if (rcResult.contains(ConstantTransaction.CONNECTION_ERROR)) {
                    Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
                } else {
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        JSONArray notifikasi=null;
                        if (Constant.metode.equals("Debit")) {
                            final JSONArray array = Constant.dataResult;
                            array.remove(array.length() - 1);
                            final Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
                            array.put(new JSONObject().put("header", "Waktu").put("value", Function.getCurrentDateTime()));
                            array.put(new JSONObject().put("header", "Status").put("value", rmResult));
                        } else {
                            JSONObject jsonDATA = new JSONObject(rmResult);
                            final String jsonData = jsonDATA.getString(ConstantTransaction.DATA);
                            notifikasi = new JSONArray(jsonData);
                            Constant.dataResult = notifikasi;
                            JSONArray data = new JSONArray();
                            String resi="";
                            String nominal="";
                            String layanan="";
                            String tanggal="";
                            final int size = notifikasi.length();
                            for(int i=0;i<size;i++){
                                if(notifikasi.getJSONObject(i).getString("header").contains("Resi")){
                                    resi = notifikasi.getJSONObject(i).getString("value");
                                }
                                if(notifikasi.getJSONObject(i).getString("header").contains("Jumlah Bayar")){
                                    nominal = notifikasi.getJSONObject(i).getString("value");
                                }
                                if(notifikasi.getJSONObject(i).getString("header").contains("Layanan")){
                                    layanan = notifikasi.getJSONObject(i).getString("value");
                                }
                                if(notifikasi.getJSONObject(i).getString("header").contains("Waktu")){
                                    tanggal = notifikasi.getJSONObject(i).getString("value");
                                }

                            }
                            if(DeviceSession.getJsonNotif(MenuKonfirmasiPembelianPulsa.this).isEmpty()){
                                data.put(new JSONObject()
                                        .put("notifikasi",notifikasi)
                                        .put("date",tanggal)
                                        .put("nominal",nominal)
                                        .put("layanan",layanan)
                                        .put("isRead",false)
                                        .put("resi",resi));
                                DeviceSession.setJsonNotif(MenuKonfirmasiPembelianPulsa.this,data.toString());
                            }else{
                                System.out.println("asd notif else");
                                data = new JSONArray(DeviceSession.getJsonNotif(MenuKonfirmasiPembelianPulsa.this));
                                data.put(new JSONObject()
                                        .put("notifikasi",notifikasi)
                                        .put("date",tanggal)
                                        .put("nominal",nominal)
                                        .put("layanan",layanan)
                                        .put("isRead",false)
                                        .put("resi",resi));
                                DeviceSession.setJsonNotif(MenuKonfirmasiPembelianPulsa.this,data.toString());
                            }
                            DeviceSession.setNewNotif(MenuKonfirmasiPembelianPulsa.this,true);
                            NotificationHelper.CreateNotification(MenuKonfirmasiPembelianPulsa.this,"Pembelian "+layanan,"Tekan untuk melihat transaksi di kotak masuk");

                        }


                        Intent i = new Intent(activity, MenuHasilResponsePulsa.class);
                        Bundle bundle = new Bundle();
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    } else {
                        String temp = null;

                        for (int a = 0; a < Constant.dataResult.length(); a++) {
                            if (Constant.dataResult.getJSONObject(a).getString("header").equalsIgnoreCase("Status")) {
                                temp = Constant.dataResult.getJSONObject(a).getString("value");
                            } else {
                                temp = rmResult;
                            }
                        }
                        Function.showAlert(activity, temp);
                    }
                }
            }catch (Exception e){
                Function.showAlert(activity,e.getMessage());
            }
        }

    }

    class executeResendOTP extends AsyncTask<String, Void, String> {
        Context context;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... result) {
//            JSONObject json = new JSONObject();
//            try {
////                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
////                json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
////                json.put(ConstantTransaction.MSISDN_NASABAH_INQ, Constant.msisdnNasabah);
////                json.put(ConstantTransaction.ACTION, Constant.ACTION_RESEND_TOKEN);
////                tripleDES = new TripleDES();
////                resultEnc = tripleDES.encrypt(json.toString());
////                resultDec = tripleDES.decrypt(resultEnc);
////                System.out.println("OK OK "+ resultDec);
//                return jsonParser.HttpRequestPost(ConstantTransaction.URL, DeviceSession.getTempRequestBody(getBaseContext()), ConstantTransaction.TimeOutConnection, MenuKonfirmasiPembelianPulsa.this);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return resultfromJson;
            return jsonParser.HttpRequestPost(ConstantTransaction.URL, DeviceSession.getTempRequestBody(MenuKonfirmasiPembelianPulsa.this), ConstantTransaction.TimeOutConnection, MenuKonfirmasiPembelianPulsa.this);

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
//                    System.out.println("Nilai Result JSONParser rmResult : " +rmResult);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                         Function.showAlert(MenuKonfirmasiPembelianPulsa.this,"OTP berhasil dikirim, silahkan masukan OTP");
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}




