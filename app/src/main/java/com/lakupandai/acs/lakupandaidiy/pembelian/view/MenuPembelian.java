package com.lakupandai.acs.lakupandaidiy.pembelian.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableRow;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction.ACTION_PEMBELIAN;

/**
 * Created by Erdy on 12/06/2018.
 */
public class MenuPembelian extends HomeActivity {
    public static String noHPAgen, productCode, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    private ImageView llHomeMenuPembelian;
    private Button btnKirimMenuPembelian;
    private Spinner spLayanan, spSubLayanan, spNominal, spMetodePembayaran;
    private Activity activity;
    private TableRow tblRowMenuAktivasi6;
    private EditText etRekeningPonsel, etNomorHPPembelian;

    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    public int posLayanan;

    public static String metode, sublayanan, otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_pembelian);
        Constant.layanan_title = "Pembelian";
        initUI();
    }

    private void initUI() {
        activity = this;
        spLayanan = (Spinner) findViewById(R.id.spLayanan);
        spSubLayanan = (Spinner) findViewById(R.id.spSubLayanan);
        spNominal = (Spinner) findViewById(R.id.spNominal);
        spMetodePembayaran = (Spinner) findViewById(R.id.spMetodePembayaran);
        tblRowMenuAktivasi6 = (TableRow) findViewById(R.id.tblRowMenuAktivasi6);
        etRekeningPonsel = findViewById(R.id.etRekeningPonsel);
        etNomorHPPembelian = findViewById(R.id.etNomorHPPembelian);

        final String[] arrayA = new String[MainMenu.layanan.size()];
        for (int i = 0; MainMenu.layanan.size() > i; i++) {
            arrayA[i] = MainMenu.layanan.get(i);
        }

        final ArrayAdapter<String> adapterA = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line, arrayA);
        spLayanan.setAdapter(adapterA);
        spLayanan.setClipToPadding(true);

        spLayanan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
                                       long arg3) {
                // TODO Auto-generated method stub
                try {
                    JSONArray ArrayB = new JSONArray(MainMenu.sublayanan.get(pos));

                    String[] arrayDenom = new String[ArrayB.length()];
                    String[] arrayDenoma = new String[ArrayB.length()];
                    for (int i = 0; ArrayB.length() > i; i++) {
                        JSONObject jsonC = new JSONObject(ArrayB.getString(i));
                        arrayDenom[i] = jsonC.getString("productName");
                        arrayDenoma[i] = jsonC.getString("denom");
                    }

                    ArrayAdapter<String> adapterdenom = new ArrayAdapter<String>(activity,
                            android.R.layout.simple_dropdown_item_1line, arrayDenom);
                    spSubLayanan.setAdapter(adapterdenom);
                    spSubLayanan.setClipToPadding(true);
                    posLayanan = pos;

                    spSubLayanan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
                                                   long arg3) {
                            // TODO Auto-generated method stub
                            try {
                                JSONArray ArrayB = new JSONArray(MainMenu.sublayanan.get(posLayanan));
                                JSONObject jsonC = new JSONObject(ArrayB.getString(pos));
                                String Denom = jsonC.getString("denom");
                                String[] listDenom = Denom.split(",");

                                final int size = listDenom.length;
                                String temp="";
                                for (int i = size; i >= 0; i--) {
                                    for (int j = 0; j < i - 1; j++) {
                                        if (Integer.valueOf(listDenom[j]) > Integer.valueOf(listDenom[j + 1])) {
                                            temp = listDenom[j + 1];
                                            listDenom[j + 1] = listDenom[j];
                                            listDenom[j] = temp;
                                        }
                                    }

                                }

                                ArrayAdapter<String> adapterdenoma = new ArrayAdapter<String>(activity,
                                        android.R.layout.simple_dropdown_item_1line, listDenom);
                                spNominal.setAdapter(adapterdenoma);
                                spNominal.setClipToPadding(true);

                                productCode = jsonC.getString("productCode");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        String[] arrayB = new String[]{"Debit", "Tunai"};
        ArrayAdapter<String> adapterB = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line, arrayB);
        spMetodePembayaran.setAdapter(adapterB);
        spMetodePembayaran.setClipToPadding(true);

        spMetodePembayaran.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
                                       long arg3) {
                // TODO Auto-generated method stub
                if (spMetodePembayaran.getSelectedItem().toString().equals("Debit")) {
                    tblRowMenuAktivasi6.setVisibility(View.VISIBLE);
                } else {
                    tblRowMenuAktivasi6.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        llHomeMenuPembelian = (ImageView) findViewById(R.id.llHomeMenuPembelian);
        llHomeMenuPembelian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//        ivNotifikasi = (ImageView) findViewById(R.id.ivNotifikasi);
//        ivNotifikasi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(mContext, "Notifikasi", Toast.LENGTH_SHORT).show();
//            }
//        });

        btnKirimMenuPembelian = (Button) findViewById(R.id.btnKirimMenuPembelian);
        btnKirimMenuPembelian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spMetodePembayaran.getSelectedItem().toString().equals("Debit") && etRekeningPonsel.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etNomorHPPembelian.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else {
                    new executePembelian(activity).execute();
                }
            }
        });

    }

    class executePembelian extends AsyncTask<String, JSONObject, String[]> {
        Context context;

        private executePembelian(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @SuppressLint("WrongThread")
        @Override
        protected String[] doInBackground(String... params) {
            JSONObject json = new JSONObject();
            final String[] temp = new String[2];
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                if (spMetodePembayaran.getSelectedItem().toString().equals("Debit")) {
                    json.put(ConstantTransaction.KODE_PRODUK, productCode);
                    json.put(ConstantTransaction.ID_PELANGGAN, etNomorHPPembelian.getText());
                    json.put(ConstantTransaction.MSISDN_NASABAH, etRekeningPonsel.getText());
                    json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                    json.put(ConstantTransaction.DENOM, spNominal.getSelectedItem().toString());
                    json.put(ConstantTransaction.ACTION, ACTION_PEMBELIAN);

                } else {
                    json.put(ConstantTransaction.KODE_PRODUK, productCode);
                    json.put(ConstantTransaction.ID_PELANGGAN, etNomorHPPembelian.getText());
                    json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                    json.put(ConstantTransaction.DENOM, spNominal.getSelectedItem().toString());
                    json.put(ConstantTransaction.ACTION, ACTION_PEMBELIAN);
                }


                //Log.v(Constant.LOG_LOGIN, Constant.MESSAGE_LOG_LOGIN);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Response Send "+resultEnc);
                System.out.println("Response Send Decrypt "+resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuPembelian.this);
                System.out.println("Result json: "+resultfromJson);
                resultfromJsonDecrypt = tripleDES.decrypt(resultfromJson);
                System.out.println("RESPONSE DATA: " + resultfromJson);
                System.out.println("RESPONSE DATA: " + resultfromJsonDecrypt);

                temp[0] = resultfromJson;
                temp[1] = resultEnc;
                return temp;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return temp;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;

            if (result[0].contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result[0].contains(ConstantTransaction.SERVER_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.SERVER_ERROR);
            } else if (result[0].contains(ConstantTransaction.CONNECTION_ERROR)) {
                //SendSms.splitSMS(resultEnc, mContext);
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result[0]);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    //lastLogin = jsonObject.getString(Constant.LAST_LOGIN);
                    System.out.println("Nilai Result JSONParser resultfromJsonDecrypt : " + resultfromJsonDecrypt);
                    if (jsonObject.getString("RC").equalsIgnoreCase("00")) {
                        JSONObject jsonDATA = new JSONObject(rmResult);
                        String data = jsonDATA.getString(ConstantTransaction.DATA);
                        Constant.dataResult = new JSONArray(data);
                        System.out.println("asd data "+Constant.dataResult);
                        Constant.metode = spMetodePembayaran.getSelectedItem().toString();
                        sublayanan = spSubLayanan.getSelectedItem().toString();
                        if (spMetodePembayaran.getSelectedItem().toString().equals("Debit")) {
                            Intent i = new Intent(MenuPembelian.this, MenuKonfirmasiPembelianPulsa.class);
                            startActivity(i);
                            //finish();
                            Constant.msisdnNasabah = etRekeningPonsel.getText().toString();
                            Constant.metode = "Debit";
                            DeviceSession.setTempRequestBody(getBaseContext(),result[1]);
                        }else{
                            Intent i = new Intent(MenuPembelian.this, MenuKonfirmasiPembelianPulsa.class);
                            startActivity(i);
                           // finish();
                            Constant.metode = "Tunai";
                            otp = jsonObject.getString(ConstantTransaction.TOKEN_REK_BARU);
                        }

                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                    Function.showAlert(mContext, strResponse);
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
