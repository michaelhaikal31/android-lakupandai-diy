package com.lakupandai.acs.lakupandaidiy.saldo.model;

/**
 * Created by Erdy on 12/04/2018.
 */
public class Mutasi {
    public String judul;
    public String date;
    public String action;
    public String saldo;
    public int id;
    public String resi;

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public Mutasi(String judul, String date, String action, String saldo, int id, String resi) {
        this.judul = judul;
        this.date = date;
        this.action = action;
        this.saldo = saldo;
        this.id = id;
        this.resi = resi;
    }

    public String getResi() {
        return resi;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Mutasi{" +
                "judul='" + judul + '\'' +
                ", date='" + date + '\'' +
                ", action='" + action + '\'' +
                ", saldo='" + saldo + '\'' +
                '}';
    }
}
