package com.lakupandai.acs.lakupandaidiy.util;

import android.app.Activity;
import android.support.v4.view.ViewPager;

import java.util.TimerTask;

public class SliderTimer extends TimerTask {
    private ViewPager viewPager;
    private int size;
    private Activity activity;

    public SliderTimer(ViewPager viewPager, int size, Activity activity) {
        this.viewPager = viewPager;
        this.size = size;
        this.activity = activity;
    }

    @Override
    public void run() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (viewPager.getCurrentItem() < size - 1) {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                } else {
                    viewPager.setCurrentItem(0, true);
                }
            }
        });
    }
}
