package com.lakupandai.acs.lakupandaidiy.pembayaran.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableRow;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.lakupandai.acs.lakupandaidiy.util.ConstantError.ALERT_LAYANAN;

/**
 * Created by Erdy on 12/06/2018.
 */
public class MenuPembayaran extends HomeActivity {
    public String noHPAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rcResult, rmResult;
    public static String productCode;
    private ImageView llHomeMenuPembayaran;
    private Button btnKirimMenuPembayaran;
    private Spinner spLayanan, spSubLayanan, spMetodePembayaran;
    private Activity activity;
    private TableRow tblRowMenuAktivasi6;
    private EditText etIdPelanggan, etRekeningPonsel,etTahunTagihan;
    ProgressDialog progressDialog;
    private Spinner spPeriode;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();
    String[] arrayDenom;
    private String[] arrayPeriode = new String[]{"1","2","3","4","5","6","7","8","9","10","11","12"};
    private TableRow tableRowPeriode;
    public int posLayanan;
    private TextInputLayout ilTagihan;

    public static String metode, sublayanan, otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_pembayaran);
        Constant.layanan_title = "Pembayaran";
        initUI();
    }

    private void initUI() {
        activity = this;
        spLayanan = (Spinner) findViewById(R.id.spLayanan);
        spSubLayanan = (Spinner) findViewById(R.id.spSubLayanan);
        spMetodePembayaran = (Spinner) findViewById(R.id.spMetodePembayaran);
        tblRowMenuAktivasi6 = (TableRow) findViewById(R.id.tblRowMenuAktivasi6);
        etIdPelanggan = findViewById(R.id.etIdPelanggan);
        etRekeningPonsel = findViewById(R.id.etRekeningPonsel);
        etTahunTagihan = findViewById(R.id.etThnTagihan);
        spPeriode = findViewById(R.id.spPeriode);
        tableRowPeriode = findViewById(R.id.tblRowMenuAktivasi4);
        ilTagihan = findViewById(R.id.ilTahunTagihan);
//        JSONArray ArrayA = null;
//        try {
//            ArrayA = new JSONArray(MenuMain.sublayanan.get(0));
//            JSONObject jsonC = new JSONObject(ArrayA.getString(0));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


        String[] arrayA = new String[MainMenu.layanan.size()];
        for (int i = 0; MainMenu.layanan.size() > i; i++) {
                arrayA[i] = MainMenu.layanan.get(i);
            }
//
//        }

//        String[] arrayA = new String[4];
//        for (int i = 0; 4 > i; i++) {
//            if(MainMenu.layanan.get(i).equals("PDAM") || MainMenu.layanan.get(i).equalsIgnoreCase("PBB")||MainMenu.layanan.get(i).equalsIgnoreCase("PAJAK DAERAH")||MainMenu.layanan.get(i).equalsIgnoreCase("RETRIBUSI")){
//                arrayA[i] = MainMenu.layanan.get(i);
//            }
//
//        }


        System.out.println("asd lewat 0");
        ArrayAdapter<String> adapterA = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line, arrayA);
        spLayanan.setAdapter(adapterA);
        spLayanan.setClipToPadding(true);

        ArrayAdapter<String> adapterPeriode = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line, arrayPeriode);
        spPeriode.setAdapter(adapterPeriode);
        spPeriode.setClipToPadding(true);


//        spLayanan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//
//            @Override
//            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
//                                       long arg3) {
//                // TODO Auto-generated method stub
//                try {
//                    JSONArray ArrayB = new JSONArray(MainMenu.sublayanan.get(pos));
//
//                    arrayDenom = new String[ArrayB.length()];
//                    for (int i = 0; ArrayB.length() > i; i++) {
//                        JSONObject jsonC = new JSONObject(ArrayB.getString(i));
//                        arrayDenom[i] = jsonC.getString("productName");
//                    }
//
//                    ArrayAdapter<String> adapterdenom = new ArrayAdapter<String>(activity,
//                            android.R.layout.simple_dropdown_item_1line, arrayDenom);
//                    spSubLayanan.setAdapter(adapterdenom);
//                    spSubLayanan.setClipToPadding(true);
//
//                    posLayanan = pos;
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> arg0) {
//                // TODO Auto-generated method stub
//
//            }
//        });
//
        System.out.println("asd lewat 1");
        spLayanan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
                                       long arg3) {
                // TODO Auto-generated method stub
                try {
                    JSONArray ArrayB = new JSONArray(MainMenu.sublayanan.get(pos));
                    if(MainMenu.sublayanan.get(pos).contains("PBB")){
                        ilTagihan.setVisibility(View.VISIBLE);
                    }else{
                        ilTagihan.setVisibility(View.GONE);
                    }
                    arrayDenom = new String[ArrayB.length()];
                    for (int i = 0; ArrayB.length() > i; i++) {
                        JSONObject jsonC = new JSONObject(ArrayB.getString(i));
                        arrayDenom[i] = jsonC.getString("productName");
                    }

                    ArrayAdapter<String> adapterdenom = new ArrayAdapter<String>(activity,
                            android.R.layout.simple_dropdown_item_1line, arrayDenom);
                    spSubLayanan.setAdapter(adapterdenom);
                    spSubLayanan.setClipToPadding(true);


                    posLayanan = pos;

                    spSubLayanan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
                                                   long arg3) {
                            // TODO Auto-generated method stub
                            try {
                                JSONArray ArrayB = new JSONArray(MainMenu.sublayanan.get(posLayanan));
                                JSONObject jsonC = new JSONObject(ArrayB.getString(pos));
                                productCode = jsonC.getString("productCode");
                                if(arrayDenom[pos].equals("BPJS Kesehatan")){
                                    tableRowPeriode.setVisibility(View.VISIBLE);
                                }else{
                                    tableRowPeriode.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                System.out.println("asd json exception [menupembayaran] "+e.getMessage());
                            }catch (Exception e){
                                System.out.println("asd exception[MenuPembayaran] "+e.getMessage());
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });


        String[] arrayB = new String[]{"Debit", "Tunai"};
        ArrayAdapter<String> adapterB = new ArrayAdapter<String>(activity,
                android.R.layout.simple_dropdown_item_1line, arrayB);
        spMetodePembayaran.setAdapter(adapterB);
        spMetodePembayaran.setClipToPadding(true);
        System.out.println("asd lewat 2");
        spMetodePembayaran.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
                                       long arg3) {
                // TODO Auto-generated method stub
                if (spMetodePembayaran.getSelectedItem().toString().equals("Debit")) {
                    tblRowMenuAktivasi6.setVisibility(View.VISIBLE);
                } else {
                    tblRowMenuAktivasi6.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        System.out.println("asd lewat 3");
        llHomeMenuPembayaran = (ImageView) findViewById(R.id.llHomeMenuPembayaran);
        System.out.println("asd lewat 4");
        llHomeMenuPembayaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//        ivNotifikasi = (ImageView) findViewById(R.id.ivNotifikasi);
//        ivNotifikasi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(mContext, "Notifikasi", Toast.LENGTH_SHORT).show();
//            }
//        });

        btnKirimMenuPembayaran =  findViewById(R.id.btnKirimMenuPembayaran);
        System.out.println("asd lewat 5");
        btnKirimMenuPembayaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("asd lewat 6");
                if (spLayanan.getSelectedItem().toString().equals("Pilih Layanan")) {
                    Function.showAlert(activity, ALERT_LAYANAN);
                } else if (spMetodePembayaran.getSelectedItem().toString().equals("Debit") && etRekeningPonsel.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etIdPelanggan.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else {
                    new executePembayaran(activity).execute();
                }
            }
        });

    }

    class executePembayaran extends AsyncTask<String, JSONObject, String[]> {
        Context context;

        private executePembayaran(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @SuppressLint("WrongThread")
        @Override
        protected String[] doInBackground(String... params) {
            JSONObject json = new JSONObject();
            final String[] temp = new String[2];
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.KODE_PRODUK, productCode);
                if(arrayDenom[spSubLayanan.getSelectedItemPosition()].equals("BPJS Kesehatan")){
                    System.out.println("asd 1");
                    json.put(ConstantTransaction.ID_PELANGGAN, etIdPelanggan.getText().toString() +String.format("%02d", Integer.valueOf(arrayPeriode[spPeriode.getSelectedItemPosition()])));
                }else if(spLayanan.getSelectedItem().toString().equals("PBB")){
                    System.out.println("asd 2");
                    json.put(ConstantTransaction.ID_PELANGGAN, etIdPelanggan.getText().toString() + etTahunTagihan.getText().toString());
                }else{
                    System.out.println("asd 3");
                    json.put(ConstantTransaction.ID_PELANGGAN, etIdPelanggan.getText().toString());
                }
                if (spMetodePembayaran.getSelectedItem().toString().equals("Debit")) {
                    json.put(ConstantTransaction.MSISDN_NASABAH, etRekeningPonsel.getText());
                    json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                    json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_PEMBAYARAN);
                } else {
                    json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                    json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_PEMBAYARAN);
                }
                //Log.v(Constant.LOG_LOGIN, Constant.MESSAGE_LOG_LOGIN);
                tripleDES = new TripleDES();

                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);

                System.out.println("Response Send [MenuPembayaran] " + resultEnc);
                System.out.println("Response Send Decrypt [MenuPembayaran] " + resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuPembayaran.this);
//                System.out.println("Result json: "+resultfromJson);
                resultfromJsonDecrypt = tripleDES.decrypt(resultfromJson);
//                System.out.println("RESPONSE DATA: " + resultfromJson);
//                System.out.println("RESPONSE DATA: " + resultfromJsonDecrypt);
                temp[0] = resultfromJson;
                temp[1] = resultEnc;

                return temp;
            } catch (Exception e) {
                System.out.println("asd doInbackground[MenuPembayaran] "+e.getMessage());
            }
            return temp;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;

            if (result[0].contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result[0].contains(ConstantTransaction.SERVER_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.SERVER_ERROR);
            } else if (result[0].contains(ConstantTransaction.CONNECTION_ERROR)) {
                //SendSms.splitSMS(resultEnc, mContext);
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result[0]);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    //lastLogin = jsonObject.getString(Constant.LAST_LOGIN);
                    System.out.println("Nilai Result JSONParser resultfromJsonDecrypt : " + resultfromJsonDecrypt);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        System.out.println("asd nih a "+rcResult.toString());
//                        JSONObject jsonDATA = new JSONObject(rmResult);
//                        String data = jsonDATA.getString(ConstantTransaction.DATA);
//                        Constant.dataResult = new JSONArray(data);
                        Constant.metode = spMetodePembayaran.getSelectedItem().toString();
                        sublayanan = spSubLayanan.getSelectedItem().toString();
                        if (spMetodePembayaran.getSelectedItem().toString().equals("Debit")) {
                            System.out.println("asd masuk debit");
                            Intent i = new Intent(MenuPembayaran.this, MenuKonfirmasiPembayaran.class);
                            Constant.dataResult = new JSONObject(rmResult).getJSONArray("data");
                            System.out.println("asd data result "+Constant.dataResult.toString());
                            startActivity(i);
                            Constant.msisdnNasabah = etRekeningPonsel.getText().toString();
                            Constant.metode = "Debit";
                            DeviceSession.setTempRequestBody(MenuPembayaran.this, result[1]);
                        } else {
                            System.out.println("asd masuk cash");
                            Constant.dataResult = new JSONObject(rmResult).getJSONArray("data");
                            Intent i = new Intent(MenuPembayaran.this, MenuKonfirmasiPembayaran.class);
                            startActivity(i);
                            Constant.metode = "Tunai";
                            otp = jsonObject.getString(ConstantTransaction.TOKEN_REK_BARU);
                        }

                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                    Function.showAlert(mContext, strResponse);
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    //    private String getKodeProvider() {
//        switch (spSubLayanan.getSelectedItem().toString()) {
//
//            case "Telkom":
//                return "017001";
//            case "Telkomsel (Halo)":
//                return "010001";
//            case "XL Postpaid":
//                return "013001";
//
//            default:
//                return "";
//        }
//    }
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
