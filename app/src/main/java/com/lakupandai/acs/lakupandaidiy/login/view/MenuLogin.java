package com.lakupandai.acs.lakupandaidiy.login.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.ImageCache;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class MenuLogin extends Activity {
    private EditText etNoHpAgen, etPinAgen;
    private Button btnLoginMenuLogin;
    private CheckBox chkLoginMenuLogin;
    private TextView txtVersionMenuLogin;
    Activity mContext;
    private ProgressDialog progressDialog;
    private String lastLogin, simSerialNumber, pin, imei, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult, valuePin, valueMsisdnAgen;
    public static String msisdnAgen;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();
    SharedPreferences shared;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.menu_login);
        DeviceSession.setTempRequestUrl(this,"");
        DeviceSession.setTempRequestBody(this,"");
        changeStatusBarColor("#ffffff00");
        checkAndRequestPermissions();
        initUI();
        checkDevice();
        getVersion();
        FirebaseMessaging.getInstance().subscribeToTopic("agent");

    }

    private void initUI() {
        mContext = this;
        Constant.status = 1;
        System.out.println("Token : "+FirebaseInstanceId.getInstance().getToken());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        //getWindow().setBackgroundDrawableResource(R.drawable.bglogin);

        shared = getSharedPreferences(Constant.MY_PREF, MODE_PRIVATE);
        etNoHpAgen = (EditText) findViewById(R.id.etNoHpAgenMenuLogin);
        etPinAgen = (EditText) findViewById(R.id.etPinAgenMenuLogin);

        //INI UNTUK MENAMBAHKAN TOMBOL VIEW PASSWORD JIKA MENGGUNAKAN VIEW menu_login.xml
        etPinAgen.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (etPinAgen.getRight() - etPinAgen.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        if ((etPinAgen.getTransformationMethod().toString().substring(0, 48)).equals("android.text.method.PasswordTransformationMethod")) {
                            etPinAgen.setTransformationMethod(new HideReturnsTransformationMethod());
                        } else {
                            etPinAgen.setTransformationMethod(new PasswordTransformationMethod());
                        }
                        etPinAgen.setSelection(etPinAgen.getText().length());
                        return true;
                    }
                }
                return false;
            }
        });

        System.out.println("FCM TOKEN " + FirebaseInstanceId.getInstance().getToken());
        if (shared.contains(Constant.My_PREF_MSISDN_AGEN_LOGIN)) {
            valueMsisdnAgen = (shared.getString(Constant.My_PREF_MSISDN_AGEN_LOGIN, ""));
            valuePin = (shared.getString(Constant.MY_PREF_PIN, ""));

//            System.out.println("Nilai Username:  "+valueMsisdnAgen);
//            System.out.println("Nilai Pin: "+valuePin);
            etNoHpAgen.setText(Function.decryptSharedPref(valueMsisdnAgen));
            etPinAgen.setText(Function.decryptSharedPref(valuePin));
        }

        chkLoginMenuLogin = (CheckBox) findViewById(R.id.chkLoginMenuLogin);
        chkLoginMenuLogin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked == false) {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Constant.MY_PREF, MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.clear();
                    editor.commit();
                    editor.putString(Constant.My_PREF_MSISDN_AGEN, Function.encryptSharedPref(msisdnAgen));
                    editor.commit();
                }
            }
        });


        btnLoginMenuLogin = findViewById(R.id.btnLoginMenuLogin);
        btnLoginMenuLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseMessaging.getInstance().subscribeToTopic("agent");
                msisdnAgen = etNoHpAgen.getText().toString();
                pin = etPinAgen.getText().toString();
                if (etNoHpAgen.getText().toString().isEmpty() || etPinAgen.getText().toString().isEmpty()) {
                    Function.showAlert(mContext, ConstantError.ALERT_FIELD_EMPTY);
                } else if (msisdnAgen.length() < 1) {
                    Function.showAlert(mContext, ConstantError.ALERT_USERNAME_EMPTY);
                } else if (pin.length() < 6) {
                    Function.showAlert(mContext, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else {
                    if (chkLoginMenuLogin.isChecked()) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constant.MY_PREF, MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        if (!pref.contains(Constant.SETTING_SMS)) {
                            editor.putString(Constant.SETTING_SMS, "1");
                        }
                        editor.putString(Constant.MY_PREF_PIN, Function.encryptSharedPref(pin));
                        editor.putString(Constant.My_PREF_MSISDN_AGEN, Function.encryptSharedPref(msisdnAgen));
                        editor.putString(Constant.My_PREF_MSISDN_AGEN_LOGIN, Function.encryptSharedPref(msisdnAgen));
                        editor.commit();

                        new executeLogin(mContext).execute();
                    } else if (!chkLoginMenuLogin.isChecked()) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences(Constant.MY_PREF, MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.clear();
                        editor.commit();
                        if (!pref.contains(Constant.SETTING_SMS)) {
                            editor.putString(Constant.SETTING_SMS, "1");
                        }
                        editor.putString(Constant.My_PREF_MSISDN_AGEN, Function.encryptSharedPref(msisdnAgen));
                        editor.commit();
                        new executeLogin(mContext).execute();
                    }

                }
            }
        });
    }

    private void getVersion() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        txtVersionMenuLogin = (TextView) findViewById(R.id.txtVersionMenuLogin);
        txtVersionMenuLogin.setText("Version " + packageInfo.versionName);
    }

    private void changeStatusBarColor(String color) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }


    @Override
    public void onBackPressed() {
        Constant.status = 99;
        finish();
    }


    class executeLogin extends AsyncTask<String, JSONObject, String> {

        Context context;

        private executeLogin(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(mContext);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @SuppressLint("MissingPermission")
        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                simSerialNumber = telemamanger.getSimSerialNumber();
                imei = telemamanger.getDeviceId();
                json.put(ConstantTransaction.SIM_SERIAL_LOGIN, simSerialNumber);
                json.put(ConstantTransaction.PIN_LOGIN, pin);
                json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, msisdnAgen);
                json.put(ConstantTransaction.IMEI_LOGIN, imei);
                json.put(ConstantTransaction.TOKEN, FirebaseInstanceId.getInstance().getToken());
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_LOGIN);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                System.out.println("json: request login " + json);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL,resultEnc, ConstantTransaction.TimeOutConnection, MenuLogin.this);
                resultfromJsonDecrypt = tripleDES.decrypt(resultfromJson);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonObject;
            System.out.println("asd response [login] "+tripleDES.decrypt(result));

            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                progressDialog.dismiss();
                Function.showAlert(mContext, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.SERVER_ERROR)) {
                progressDialog.dismiss();
                Function.showAlert(mContext, ConstantTransaction.SERVER_ERROR);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                //SendSms.splitSMS(resultEnc, mContext);
                progressDialog.dismiss();
                Function.showAlert(mContext, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    //System.out.println("Nilai Result JSONParser Decrypt: " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);

                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    //lastLogin = jsonObject.getString(Constant.LAST_LOGIN);
                    if (!jsonObject.has(ConstantTransaction.LAST_LOGIN)){
                        lastLogin = "";
                    } else {
                        lastLogin = jsonObject.getString(ConstantTransaction.LAST_LOGIN);
                    }
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        JSONArray jsonArray = new JSONObject(rmResult).getJSONArray("data");
                        JSONObject object;
                        for (int i = 0; i < jsonArray.length(); i++) {
                             object = jsonArray.getJSONObject(i);
                            if (object.getString("header").equals("Agent Id")) {
                                Constant.idAgen = object.getString("value");
                            }
                            if (object.getString("header").equals("Agent Name")) {
                                Constant.usernameAgen = object.getString("value");
                            }
                        }
                       new executeInit(mContext).execute();

                    } else if (rcResult.contains(ConstantTransaction.RESET_CODE)) {
//                        Toast.makeText(getApplicationContext(), rmResult, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(mContext, MenuResetPassword.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.LAST_LOGIN, lastLogin);
                        // System.out.println("Hasil last login: " + lastLogin);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();

                    } else {
                        progressDialog.dismiss();
                        Function.showAlert(mContext, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    progressDialog.dismiss();
                    Function.showAlert(mContext, e.getMessage());
                }
            }
        }
    }

    class executeInit extends AsyncTask<String, JSONObject, String> {

        Context context;

        private executeInit(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @SuppressLint("MissingPermission")
        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, msisdnAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_INIT);
                json.put("info","getImage");
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                System.out.println("json: " + json.toString());
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL,resultEnc, ConstantTransaction.TimeOutConnection, MenuLogin.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(mContext, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.SERVER_ERROR)) {
                Function.showAlert(mContext, ConstantTransaction.SERVER_ERROR);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                //SendSms.splitSMS(resultEnc, mContext);
                Function.showAlert(mContext, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("asd response init "+resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        generateSlideShow(rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(mContext, e.getMessage());
                }
            }
        }
    }

    private void generateSlideShow(String rmResult) {
        try{
            final JSONObject jsonObject = new JSONObject(rmResult);
            final int length = jsonObject.getJSONObject("data").getJSONArray("image_slider").length();
            DeviceSession.setJumlahSlideImage(this,length);
            final ImageCache imageCache = ImageCache.getInstance();
            imageCache.initializeCache();
            final JSONArray arrayImage = jsonObject.getJSONObject("data").getJSONArray("image_slider");
            for(int i=0;i<length;i++){
                imageCache.addImageToWarehouse("image_slider"+i,getBitmap(arrayImage.getJSONObject(i).getString("base64")));
            }

        }catch (Exception e){
            System.out.println("asd exception generateSlideShow[login] "+e.getMessage());
        }finally {
            final Intent i = new Intent(mContext, MainMenu.class);
            final Bundle bundle = new Bundle();
            bundle.putString(ConstantTransaction.LAST_LOGIN, lastLogin);
            i.putExtras(bundle);
            progressDialog.dismiss();
            startActivity(i);
            finish();
        }
    }

    private boolean checkAndRequestPermissions() {
        int MY_PERMISSIONS_REQUEST_CAMERA = 1;

        int P1 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        int P2 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_NETWORK_STATE);

        int P3 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET);

        int P4 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_STATE);

        int P5 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.BLUETOOTH);

        int P6 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.BLUETOOTH_ADMIN);

        int P7 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_SMS);

        int P8 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);

        int P9 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (P1 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (P2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_NETWORK_STATE);
        }
        if (P3 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.INTERNET);
        }
        if (P4 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (P5 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.BLUETOOTH);
        }
        if (P6 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.BLUETOOTH_ADMIN);
        }
        if (P7 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (P8 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (P9 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECEIVE_SMS);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_REQUEST_CAMERA);
            return false;
        }
        return true;
    }

    private static boolean isDeviceRooted() {
        //return checkRootMethod1() || checkRootMethod2() || checkRootMethod3();
        return false;
    }

    private static boolean checkRootMethod1() {
        String buildTags = android.os.Build.TAGS;
        return buildTags != null && buildTags.contains("test-keys");
    }

    private static boolean checkRootMethod2() {
        String[] paths = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }

    private static boolean checkRootMethod3() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su"});
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            if (in.readLine() != null) return true;
            return false;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }

    private void checkDevice() {
        if (isDeviceRooted() == true) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(ConstantTransaction.MESSAGE);
            builder.setMessage(ConstantTransaction.MESSAGE_ROOT);
            builder.setPositiveButton(ConstantTransaction.MESSAGE_OK, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    System.exit(0);
                }
            });
            AlertDialog alertLogin = builder.create();
            alertLogin.show();
        }
    }

    private Bitmap getBitmap(String image) {
        byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    private String base64="{\n" +
            "\"data\":{\n" +
            "\"image_slider\":[\n" +
            "{\"base64\":\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUQExIVFRUVEg8VFxcVFRAVFRUSFRUWFhUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGxAQGzYlHyY4NTcvKy0rLzIvLS0tLS0tLSstLS0vLTUtLy0tLi0tLS0tKy0tLTAtLSstLS0tLy0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAAAQIDBQYHBAj/xABEEAABAgIGBgQJCwQDAQAAAAABAAIDEQQSITFBUQUGImFxgQcTUrEWMpGToaLB0eEjM0JTVGNyc5Ky8BQVYvEXQ4I0/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAMEAQIGBQf/xAAwEQEAAgECAwYEBQUAAAAAAAAAAQIDBBETMVEFEhUhQaFhcZGxFCIyUuEjQmKBwf/aAAwDAQACEQMRAD8A7ihCg8yuQSJQkzNSQIFBKhEMrr8k4VtuKCSAU1XFMrce9BMlChCM7ce5WIECglRi5qMMzNt+SCwIBTUYl00EiUgq2OmbfIrUCmmovFirY6dh5b0FoKJppEIGkCqQ+dmGeavQIlNBVFfCdk7/AGILgUEoATQCFXWww7lYEAhCECJQ0IJTQVPBFo5hN0USstJuTiPlxwCrEMt2r8/ggshslab0ntxF+IzU2unaEnvkgj1wlP0YzRDZib+5Q6s+NjkrWPnagjEZiL+9AjCU/RvUnvAEyqerJ2sckE2NJ2jyGSlEZO0XohvmpOdK0oINi2W2EXpNbWtN2A9qgWF21dl8VbDiTsuIQERk+OCTImBsIUyVSW1rbhggkBWtN2G9TeyajDfgb1YgrY+VhvzzUfG/D3pOFfh3qUN0tk8t6CbmgiSg10jI8irVS7asF2J9yBk1rBdifYFOqJSwUGGrsnkVagqBq2G7A5bihzpmQ5nJDzPZHM5JN2bMM/egta2QkkBJSSmgaEIQCqe6r7lYSk1uKCMJuN5KsVRFW0XYjJSdEAE/JvQQibJmMcE4QntG09ycNmJv7knNlaOYQWqmKJbQ8mamYglNRYye0eQyQKEJ7R8mSuVb2fSF+IzTEQSmgjGbLaFhHpUYe0ZnDBNra1puwHvUojMRf3oLFXFZjcRimyJMTuleoAVrTdgM0EWGsbcMM16FXEhztFhCcOJO+wi9ARGT3SxVTXVrD/tS8b8Pepvhz3SuQTAUXsmow34G9JxrWC7E+wIIB5OzPnmr2iVii6GJSSY/6Jv70E3NnYVRXPiz5/zFTe6eyOZyUurEpIGxsrAmRNVtdKw8im99tUX9yCNaWzP4K1oSawASQLEEkIQgRKaFW51XgglEfJUhhbtS+Cshs+kb+5WIECk98hNVu2bcDh7k4bZ7R5DJBWGHxpcl6GunaE15qVFbCa6K5wawCbiTIADFB6HuAEzctD1i6Q6PBcWwh1zwbgZQwf8AJ+PJahrvr1EpTjBgksgAyssdE3uyG7yrS0G2aQ6RKfENkVsIZQ2ges6bvSsYdbKd9rjfrcsML5Ym4YngF7GaJpBtFHjHhCin2IPYdaKb9qjfrcn4V077XG845eT+zUn7NH8zF9yP7NSfs0fzMX3IPX4V077XG845I6003Glxv1uWIcJGRsIskcCmxhcQACSbgASTwAQZfwrp32uN5xyPCunfa43nHLH/ANujfUxfNxPcgaPjfUxPNv8Aco+LT90fVnuz0e86003Glxv1uUoettObdS4vN5I8hWO/t0b6mL5uJ7lGJQ4jRN0KI0ZljwPKQsxkpPKY+p3Z6Nx0R0nUuGQIwZHbjMBj5bnNs8oK6ToHWaBTm/JOk4WuY6Qe3ljxC+flfQqW+E9sWG4se0zDheD7ty3YfSkIy2T/ALVq1fUrWdtPg7UmxmSrtHoe3cVsHWHxcc0E4pnsj/SUPZsOOOasYySbmzsKBpTVYcRs+RWNCBoQhAiVENxKkU0FPi/h7lY94AmiI4AWqhrZSJFncgsYye0eQySIq2i7EZK6a8NO0xR4PzseGzc57QfJOaD2F4lOdi4x0ja3mkxDR4TpQGG0j/seMT/iMPKs3r1rtCMEwaLFrOiEhxbWkxmMicTdZvXLUAtm1Q1Oi00159XAaZOiEXkXtYMTvuCwmiWwTFb/AFDnNhTm6oJuIH0RlPNdUo/SDo6G1sOG2I2G0ANYIcgAN00Gy6B1ao1GHyMINzeQHRH73PNstwsWYewi0cxmtO/5PoH3vm/ij/k+gZxvN/FBubHgiarnW/D3rSonSVQSbOt3/J3+lXDpPoH3vm/ig49pX5+N+dG/eV6tV/8A64P5g7ivFToofFiPFzokRw4OcSO9W6HpLYceHEdOq10zK0ykVDqazbDesc5ifs2pO1ol1wmdguxKbmZWSWvN11ov3n6Pin4bUX7z9HxXz3wzWemKfo9nj4v3Q2Fj58UnGdg5rW4mudGN3Wfo+KkzXSigf9n6Pinher58KfocfH+6C1h1UhxWl8JoZFAnZY1+4jA71zuIwtJaQQQSCDeCLwV0bw2ov3n6PitT1qpsCNEEWDWBIk8FspkXO4+5dL2Lk1tJ4OopPd9JmOXw+Sjqq4p/NSY3eXV3S76LSGUhh8UycO0w+M0/y8BfQlCiMiwmxGGbXtDgeInNfNK2eha6R4VC/omGRrOlEntNhOvY3IznbgF0ak65SdbKHCcYcWkww5thtJ5GQsKpfrxQMKUzyP8AcuBTQg743XPR8v8A6mHk/wBy92i9Y6LHdUhUhj3ZAkE8Ab186qyBFc1we1xa5pBDgZEEXEFB9OIXGv8Ak+k5N8iEHZVWXVb7lj9YdPQaHCMWKdzWjxnuyA9uC4vrPrnSKWS0uqQsIbSQCP8AI/S7kHT9Ma9UOATOJ1rx9CFJwB3u8UHmtM0r0p0h8xBhMhjN226XcFz9CDKU/WKlxrIlIiEdkOLW/pbILFqcKE51jWl3AEr3QtCxje0N/ER3CZWs2iOcpceHJk/RWZ/0xyFnoWrZ+lEHIe0q4auM7bvVWk5qdVuvZepn+33hraFsh1cZ23eqjwcZ23eqscejPhWp6e8NbQtkGrjO271UHVxnbd6qceh4VqenvDW0LZBq4ztu9VIaus7bvVTj0PCtT094a4hbIdXGdt3qoGrjO271U49DwrU9PeGtoWx+DrO271Uzq4ztu9VOPQ8K1PT3hraFsg1dZ23eqkdXWdt3qpx6HhWp6e8NcQtk8HGdt3qpDV1nbd6qceh4VqenvDXELYzq6ztu9VS8HGdt3qpx6HhWp6e8MDQ6K6K6o0cTgBmVlvBt31g/Sfes1QaIyGKrRLPM8SvQSob57b/lepp+ycVaf1fOfnLS9I0EwnhhdPZBnKV5I9i8pKy2s3zw/Lb+5yxJCs0mZrEy8LVY6481q15RJIQhbq7P66acdS6VEfPYa5zIYwDGmQPOU+awCJqbHkEOFhBnwKEfFkKFoSI/adsN3+MeA96zNG0NCZaW1t7vctf/ALpG+sPoWxaF0h1zTWG00gHIzuKq5eJEb7+Xwe72f+DtbuRX83+XqyDGAWAADdYmUpyRfwVV7/whHuViFG7gsnJJV9ylfwUlg5gIUbkTmhuidym1MBRIxQ22SUHbk5zuTAQ5hqaiRiitkhvsTtybEwEiMVk29UlB3pTrJtCwcyZ6VJJwSreVDkH+lDfSvJpOl9VDL7ySAOJWsv0tGJn1h5SClpim8bwoartDHp7d23nPwerWf54flt/c5YkKyPGc81nOJMpTOX8KrJV2le7WIczqcsZctrx6kmkhbIAFIDACZ3JL3aB+fZ/7/a5YtO0TKTFTv5K06zEfV5BAf2HeQrZtAUEw2lzrC6VmQF099pWVKgN6p3zTaNnS6Xs2mnyd/fefRK/ggWKqmUpsNtZxkPSTkAtZpum4j7G7DcheeJ9y0pjtfkn1Wtxaf9U7z0htbngXkDmFEPBuI8oWiucTeSeJJSa4i4kcFN+G+LzZ7b8/0eXz/hvt3BSWoUPTMRlhNduTr+RWx0OmNiNm02Yg3jcVFfFavN6Ol1+LP5V8p6S9RM0XcEwmol3YKJOCidymEN9ylK5MFNQO5DkZOCKuSbU0NtyBScVTSqQ2GKzjIfz0rW6ZpuI6xmw31jxOHJSUxzbkqarXY9PG1ufSG0OIGIB3lNkUG4jyhaG95N5J4klDTK6xTfhvi8zxvafKnl8/4b8SlV8q1Ch6XiwzfWGTrfIbwtm0fT2RWzbeLwbwor4rUehpdfi1E7R5T0n/AIq0xRDFh1R4wIcN5GHpWqOozwZFjgeBW8vQ1Zx5ZpGyPV9nU1F+9vtLQ3NIsII4ghIhZbWf54flt/c5YkK5S3ejdzeoxcLJam++xIQmtkJArIaD+fZ/7/a5Y9enR9IEOI15BIFacr7QR7VraN6ym09orlraeUTH3brelFcACTcASeAWGGsbOw71fevLpPTYiQyxrXCZEyZXYixUow235Onv2lp60mYtvPRj9I0wxX1jcLGjILyoW+9FurbI73UqM2syG4NY0+K6KACSRiGzHM7leiNo2hyl72vabW5ywGidTqbSGh8OAQ03OeQwHhO1T0rqVTYDa74JLReYZD5cQLV13WXXCjUIhkQlzyJiGwAuDcCcAPcqNXtdKPTXdUwuY+RNV4ALgOyRYVlq4Qr6FS3QnB7eYwIxC3/pT1WbClTYTaoc6UUCwVjc+WBNx8q5ysTG/lLatprMWrzhvcCKHNDha0gEKZOAWsaL0wIbKjmk2mUpXHC1e1usTOw71VSthtv5Q6nF2lgtSJtbafVmwErlhvCRnYf6vvSOsbOw71Vrwr9Es9oab9/3ZonJMBYNusTOw71VLwkZ2Her71nhX6MR2hpvW/3ZkjFJz/5ksN4Rs7DvVXmp2nA9jmNa4FwlMyuxSMNt+TW/aWnrWZrbd4NKU4xXk/REw0bszvK8aFufRtq0ykxXRora0OERJhufENoDs2i+WKvRERG0OVyZLZLTa3OWG0RqrS6SA6FBNU3OcQ1p4TvXq0jqLToLS50CsBaerIeRyFq6xrFrLR6CG9Y4l7hZDYASW8LmhU6u6+UWlxBBbWhxDOq2IAK0rZNIMidyy0cJV1FpDobg9t49IxBXTOljVhgZ/XQmhrg5oigXOaTIRJZgyB3HcuWpMbs1tNZ3jm3miRg9oeMR5NyuIWraI0sITSxwJE5iUrM717/CRnYd6vvVG2G0T5Q6rD2lgtjib22n1+bw6zfPD8tv7nLEkr26WpoivDgCBVAtlgSfavEQreONqxEuc1d63zWtWd4mSQhC3V11Ko7ob3Q3CTmOc1wyLTIqlfQGndTqJSndZFhkPlIuYapdK6tgViB0ZUC49b5z4IOLIXZ29GVBJs62X5l/oWH1z6PoECivj0frK8MtcQ51YGHOTrJYTnyQcwXbeioNOjmAfWR634q5PcWriS3To31obRXugRnVYMVwNbCHElKZyBAAPAIMTr2x40hSK8yesmJ4sIFSW6XcvPqkx5ptGEOdbroZs7IM38qs12/TerVFprWmKwOkNiIwydVNsg4XtxyVOgNWKLQiXQ2ScQflHms6riJnxRwQVdJDmjR0et2WgfirCS4Kt/6T9bWUgiiQHVobHTe8eK94uDTiBnmtAQCF0bUjUaBSKMI9IEQV3OqVXVRUFkzZiZrYXdGdBnIdbP8AMu9CDjCF2v8A4woP33nPgq39GdBBt62X5l3oQcYQuzv6NKDcOtJP3nwUx0YUH73znwQcVQu0ROjKgi35aX5nwWL1n6PKNDo0SLR+sMRrawBfWBaLXWSymg5Wuy9D9X+idLxuviVvIJehcaW2dHmtIocYtiE9TFkHHsOFz5ZYFBHpOY8aRil85FsIsn2KoFnOssBohrzHgiHOuY0KrK+tWBEl3nTeh6LTobTEaIglOG9jtqR7LxgqdXtT6LRHdZDh/KSIrvNZzQbw04ckBroW/wBvpTnfURRwMpNHlkuAro/SjrVDij+jgODm1gYzmmbSWmbWA4yNp4Bc4QCF0Lo+1IhUuA6PSK8i+rDquqzDRtOuttMv/JWzv6MKFKzrfOfBBxZNuV67O3ozoBH/AHec+CyOhtRaHR3iK2G5zgZtMR1aqcCBdNBy3wDpv1RTXeEIBVPbW4KwhNBXDf8ARNhUosMOBa4TBBBBxBsKURk/eqg8u2fKc+CDhOumrbqHHIAJgvJMN2EsWE5ha6vpXSOjYUeGYMVgcw4HA4EHA71yrWLoxjwyX0U9czsEhsRu62x3oO5BqGi9PUmjirBjxIbeyDNvJrpgKektYqVSBVjUiI9vZmGtPFrQAeaopWiY8MyfBiNIwcxw9iVG0XHiGTIMRx/xY8+xB41m9U9XYlNjCG0EMBBiPwazd/kcAtg1e6M6TFIdSPkGZTDohG4CxvPyLqGidGw6LDFHgtDWjHEnNxxKD1wYLWMbBhgBrWhoGDWgSCmzZsNxx96tYySZE7EDVcV+AtJVZeW7N+W7irYbJcc0FbW1OBxyV6CF53Oq2XjDd8EFsR8rLycFV1dW0iYN9n8sVkNmN5KsQcJ191XdRIxewEwIhJY4XNJtMM8MN3BaqvpLSNDhvY6HEaHw3iRafYuYaw9GMVs4lEIiNv6txAeNwcbHc5c0Gl6M01SKP8xGfD3NOz+kzHoV+kdZqZHaWRaTEc03tmGtIyIaBPmqKXoWkQjKJAit4sd3ytVUDR0Z5k2FEcdzHn2IPKAsrq3oOJS4zYMMGV73YMZiSe7MrYdX+jelRiHRh1EPGtIxCNzBdzlwXWNBaEg0SH1UFsheSbXOObjigv0XRGQoTIMMSaxoaBwz3r1OdK1VxRLaHPekzatNwuHvQFUnaVrSmlJA0IQgRCTXKSqeJ3eVAONawXYn2BSdCEpXSuShOwuIwViCuG/A396T3T2RzOSUXaMhhjknBMtm49+9BLqxKSTH21Tfgc1YqYpnsi/uQSe/6Iv7kCEJSUYVmyb881cgqa6RqnkU4j8BelGP0byfRvUYeyZHHFBNsISttnekDVsN2B9hVqriuwvJwQOI+XE3JMh52k3qDBVNuOOW5XoKfF/D3Kb3y9m9ERwA9maqa2raf9ILGMxN/cokVbRdiMlcovcALUA54Amosabz5MlUGkbRFmWS9AKCtzZWjmFPrBKeCbnStK89Q+NKyc5e1BY1s7TdgPaU3strC/EZqbXTtCCUCa8ETQDNVynaBZ3q0FA0IQgRTASKGuQRiMnaLCq+sJ2RYcfgpPdPZHM5IMLKwj+WoJsbKwJRGT4ohvnZilEf9EX9yCHWHxcc/arWMkodSJb896cN+Bv70EnsnYqusI2ccCpxH4C9IQRK2/PeglDZLjiVJzZiRUGP+ib8DmnEfLjggr6wt2Tbl8VZDZK03qIhZ2k/yxDHSNU8igsInYqS+rYbRh7lbEfJQbDxdaT6EDhsxN/crCFUDVsN2B9hU3vkJoKyanDuTY2e0eQyQ1k7Xchkl4v4e5BcqXbNuGWXBWlwAmq2trWm7Ae0oEwVto3YD2q5UkVbRdiMlbWEp4IK3irtC7EJN2rcO9AFa3DvTc2Vo5j3ILUpIa6dqQM0EkIQgFW8ZKxCCEOUlNIjFBQVxRM2X5pwRhjipgIIQNVxROzHuU0AIK4Qlff3q1IhE0EIwwxwShiRtvzVgCCEDUIspWqQQBigqhtkbeSuSIQECfKVqqY2RE+W5WyxUiEAk7egBKSClrcTdhuXoQogSQMqirj9GdyuImpIEE1ECSCEEKuVytCEIBCEIBCEIBCEIBCEIBCEIBCEIBCEIBCEIBCEIBCEIBCEIBCEIBCEIBCEIBCEIBCEIP/Z\"}\n" +
            "]\n" +
            "}\n" +
            "}";



}
