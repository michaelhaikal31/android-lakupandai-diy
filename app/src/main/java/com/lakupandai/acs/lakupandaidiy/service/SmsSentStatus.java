package com.lakupandai.acs.lakupandaidiy.service;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Erdy on 12/06/2018.
 */
public class SmsSentStatus extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            System.out.println("Masuk SMS Sent Status");
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //Function.showAlert(context, "SMS Terkirim");
                    Toast.makeText(context, "SMS Terkirim",
                            Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    //Function.showAlert(context, "SMS Tidak Terkirim");
                    Toast.makeText(context, "SMS Tidak Terkirim",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
            context.unregisterReceiver(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
