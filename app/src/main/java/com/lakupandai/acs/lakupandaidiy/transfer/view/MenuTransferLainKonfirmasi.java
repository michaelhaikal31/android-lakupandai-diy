package com.lakupandai.acs.lakupandaidiy.transfer.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.pembelian.view.MenuKonfirmasiPembelianPulsa;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.sms.SmsReceiver;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.SpinnerDialog;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONObject;

/**
 * Created by Erdy on 12/07/2018.
 */
public class MenuTransferLainKonfirmasi extends HomeActivity implements SmsReceiver.SmsListener{
    private Activity activity;
    private ImageView  llHomeMenuTransferKonfirmasi;
    private EditText etKodeKonfirmasiMenuTransferKonfirmasi;
    private Button btnKirimMenuTransferKonfirmasi;
    private String noHPAgen,token, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    private ProgressDialog progressDialog;
    private TripleDES tripleDES;
    private JSONParser jsonParser = new JSONParser();
    private TextView txtProgressBar;
    private TextView txtCaption1;
    private TextView txtCaption2;
    private CountDownTimer countDownTimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_transfer_lain_konfirmasi);
        initUI();
        startCountdown();
    }
    private void startCountdown() {
        txtCaption1.setVisibility(View.VISIBLE);
        txtProgressBar.setVisibility(View.VISIBLE);
        txtCaption2.setTextColor(Color.parseColor("#808080"));
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int second = (int) ((millisUntilFinished / 1000));
                //txtProgressBar.setText(second);
                txtProgressBar.setText(String.valueOf(second));

            }

            @Override
            public void onFinish() {
//                tvResendOTP.setVisibility(View.VISIBLE);
//                tvResendOTP.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        new MenuKonfirmasiPembelianPulsa.executeResendOTP(activity).execute();
//                    }
//                });
                txtCaption1.setVisibility(View.GONE);
                txtProgressBar.setVisibility(View.INVISIBLE);
                txtProgressBar.setText("");
                txtCaption2.setText("Kirim ulang OTP");
                txtCaption2.setTextColor(getResources().getColor(R.color.colorPrimary));

                txtCaption2.setTypeface(null, Typeface.BOLD);
                txtCaption2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!DeviceSession.getTempRequestBody(activity).isEmpty()) {
                            new MenuTransferLainKonfirmasi.executeResendOTP().execute();
                            System.out.println("asd [menukonfirPembayaran  ressend otp clicked | data param body ] "+ DeviceSession.getTempRequestBody(MenuTransferLainKonfirmasi.this));
                            startCountdown();
                        } else {
                            Function.showAlert(activity, Constant.BELUM_TRANSAKSI);
                        }
                    }
                });
            }
        };

        countDownTimer.start();

    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregisterReceiver(receiver);
    }


    @Override
    public void smsReceived(String msgFrom, String msgBody) {
    }


    private void initUI() {
        activity = this;
        txtProgressBar = findViewById(R.id.txtProgressBar);
        txtCaption1 = findViewById(R.id.textView16);
        txtCaption2 = findViewById(R.id.textView17);
//        final IntentFilter filter = new IntentFilter();
//        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
//        filter.setPriority(1000);
//        receiver = new SmsReceiver(this);
//        registerReceiver(receiver, filter);

        llHomeMenuTransferKonfirmasi = (ImageView) findViewById(R.id.llHomeMenuTransferKonfirmasi);
        llHomeMenuTransferKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etKodeKonfirmasiMenuTransferKonfirmasi = (EditText) findViewById(R.id.etKodeKonfirmasiMenuTransferKonfirmasi);

        btnKirimMenuTransferKonfirmasi = (Button) findViewById(R.id.btnKirimMenuTransferKonfirmasi);
        btnKirimMenuTransferKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etKodeKonfirmasiMenuTransferKonfirmasi.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_CONF_CODE_EMPTY);
                }
                else if(etKodeKonfirmasiMenuTransferKonfirmasi.getText().toString().length()<6){
                    Function.showAlert(activity,ConstantError.ALERT_CONF_CODE_LESS_THAN_6);
                }
                else{
                    token=etKodeKonfirmasiMenuTransferKonfirmasi.getText().toString();
                    new executeRequestTransfer(activity).execute();
                }
            }
        });
    }

    class executeRequestTransfer extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeRequestTransfer(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen=Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN,activity);
                json.put(ConstantTransaction.MSISDN_AGEN_TRANSFER_KONFIRMASI, noHPAgen);
                json.put(ConstantTransaction.TOKEN_TRANSFER_KONFIRMASI, token);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_TRANSFER_KONFIRMASI_LAKUPANDAI);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuTransferLainKonfirmasi.this);
                System.out.println("Response Send "+resultEnc);
                System.out.println("Response Send Decrypt "+resultDec);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive "+result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt "+resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
//                    token = jsonObject.getString(Constant.TOKEN_TRANSFER_KONFIRMASI);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    System.out.println(jsonObject);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        DeviceSession.setTempRequestBody(activity,"");
                        DeviceSession.setTempRequestUrl(activity,"");
                        Intent i = new Intent(activity, MenuTransferLainKonfirmasiDetail.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.BUNDLE_TOKEN, token);
                        bundle.putString(ConstantTransaction.BUNDLE_RC, rcResult);
                        bundle.putString(ConstantTransaction.BUNDLE_RM, rmResult);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    class executeResendOTP extends AsyncTask<String, Void, String> {
        Context context;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... result) {
            return jsonParser.HttpRequestPost(ConstantTransaction.URL, DeviceSession.getTempRequestBody(MenuTransferLainKonfirmasi.this), ConstantTransaction.TimeOutConnection, MenuTransferLainKonfirmasi.this);

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
//                    System.out.println("Nilai Result JSONParser rmResult : " +rmResult);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Function.showAlert(MenuTransferLainKonfirmasi.this,"OTP berhasil dikirim, silahkan masukan OTP");
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
