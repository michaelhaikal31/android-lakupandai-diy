package com.lakupandai.acs.lakupandaidiy.tariktunai.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Erdy on 12/20/2018.
 */
public class MenuTarikTunaiResult extends Activity {

    TextView txtTextJumlah, txtValueJumlah, txtValueNoHP, txtTextNoHP, txtValueNama, txtTextNama, txtDescMenuTarikTunaiResult, txtJudulMenuTarikTunaiResult;
    Button llHomeMenuTarikTunaiResult;
    String rmBundle, titleBundle;
    Activity activity;
    Button btnShare;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_tarik_tunai_result);
        initUI();
    }

    private void initUI() {
        activity = this;
        txtTextJumlah = findViewById(R.id.txtTextJumlah);
        txtValueJumlah = findViewById(R.id.txtValueJumlah);
        txtValueNoHP = findViewById(R.id.txtValueNoHP);
        txtTextNoHP = findViewById(R.id.txtTextNoHP);
        txtValueNama = findViewById(R.id.txtValueNama);
        txtTextNama = findViewById(R.id.txtTextNama);
        txtDescMenuTarikTunaiResult = findViewById(R.id.txtDescMenuTarikTunaiResult);
        txtJudulMenuTarikTunaiResult = findViewById(R.id.txtJudulMenuTarikTunaiResult);
        llHomeMenuTarikTunaiResult = findViewById(R.id.llHomeMenuTarikTunaiResult);
        btnShare = findViewById(R.id.btnShareTarikTunai);
        llHomeMenuTarikTunaiResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        Bundle bundle = getIntent().getExtras();
        rmBundle = bundle.getString(ConstantTransaction.BUNDLE_RM);
        titleBundle = bundle.getString(ConstantTransaction.BUNDLE_TITLE);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateResponse(rmBundle);
            }
        });
        setResponseValue(rmBundle);
    }

    private void generateResponse(String rmResult){
        String result="";
        try{
            JSONArray jsonArray;
            jsonArray = new JSONObject(rmResult).getJSONArray("data");
            System.out.println("asd jsonArray "+jsonArray);
            final int size = jsonArray.length();
            JSONObject jsonObject;
            for(int i=0;i<size;i++){
                jsonObject =jsonArray.getJSONObject(i);
                result = result + jsonObject.getString("header")+" : "+jsonObject.getString("value")+"\n";
            }
        }catch (Exception e){
            System.out.println("asd exception generateResponse [Menuhasilresponse] " +e.getMessage());
        }
        final Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Share Transaksi");
        sharingIntent.putExtra(Intent.EXTRA_TEXT,result);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    private void setResponseValue(String rmBundle) {
        try {
            System.out.println("Response rmResult "+rmBundle);
            JSONObject jsonRM = new JSONObject(rmBundle);
            JSONArray arrayData = jsonRM.getJSONArray("data");
            System.out.println("arrayData " + arrayData);

            JSONArray jsonArray = new JSONArray(arrayData.toString());
            HashMap<String, String> list = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = jsonObject1.optString("header");
                String value2 = jsonObject1.optString("value");
                list.put(value1, value2);
            }

            String namaNasabah = list.get("Nama Nasabah");
            String layanan = list.get("Layanan");
            String jumlah = list.get("Jumlah");
            String msisdn = list.get("No HP");
            String description = list.get("Keterangan");


            txtValueNama.setText(namaNasabah);
            txtValueJumlah.setText( jumlah);
            txtValueNoHP.setText(msisdn);
            txtDescMenuTarikTunaiResult.setText(description);

        } catch (Exception e) {
            Function.showAlert(activity, e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(activity, MainMenu.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
