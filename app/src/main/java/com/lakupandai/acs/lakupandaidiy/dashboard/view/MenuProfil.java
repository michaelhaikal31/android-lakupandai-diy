package com.lakupandai.acs.lakupandaidiy.dashboard.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Erdy on 12/20/2018.
 */
public class MenuProfil extends HomeActivity {
    Activity activity;
    TextView txtNamaMenuProfil, txtUserNameMenuProfil, txtValueNoRek, txtSaldoMenuProfil;
    Button btnSelesaiMenuProfil;
    private String rmResult, titleMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_profil);
        initUI();
    }

    private void initUI() {
        activity = this;
        txtNamaMenuProfil = findViewById(R.id.txtNamaMenuProfil);
        txtUserNameMenuProfil = findViewById(R.id.txtUserNameMenuProfil);
        txtValueNoRek = findViewById(R.id.txtValueNoRek);
        txtSaldoMenuProfil = findViewById(R.id.txtSaldoMenuProfil);
        btnSelesaiMenuProfil = findViewById(R.id.btnSelesaiMenuProfil);

        Bundle bundle = getIntent().getExtras();
        rmResult = bundle.getString(ConstantTransaction.BUNDLE_RM);
        titleMenu = bundle.getString(ConstantTransaction.BUNDLE_TITLE);

        System.out.println("Response RM " + rmResult);
        System.out.println("Response title " + titleMenu);


        setResponseValue(rmResult);

        btnSelesaiMenuProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setResponseValue(String rmResult) {
        try {
            JSONObject jsonRM = new JSONObject(rmResult);

            JSONArray arrayData = jsonRM.getJSONArray("data");
            System.out.println("arrayData " + arrayData);

            JSONArray jsonArray = new JSONArray(arrayData.toString());
            HashMap<String, String> list = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = jsonObject1.optString("header");
                String value2 = jsonObject1.optString("value");
                list.put(value1, value2);
            }

            String userId = list.get("User Id");
            String userName = list.get("User Name");
            String userAccount = list.get("User Account");
            String balance = list.get("Balance");

            Constant.usernameAgen = userName;
            Constant.idAgen = userId;

            txtNamaMenuProfil.setText(userName);
            txtNamaMenuProfil.setTypeface(null, Typeface.BOLD);
            txtUserNameMenuProfil.setText(userId);
            txtUserNameMenuProfil.setTypeface(null, Typeface.BOLD);
            txtValueNoRek.setText(userAccount);
            txtValueNoRek.setTypeface(null, Typeface.BOLD);
            txtSaldoMenuProfil.setText("Rp. " + balance);
            txtSaldoMenuProfil.setTypeface(null, Typeface.BOLD);

        } catch (Exception e) {
            Function.showAlert(activity, e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(activity, MainMenu.class);
        startActivity(i);
        finish();
    }
}
