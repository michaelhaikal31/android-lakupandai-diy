package com.lakupandai.acs.lakupandaidiy.saldo.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Erdy on 12/20/2018.
 */
public class MenuCekStatusTerakhirResultTransfer extends HomeActivity {

    Activity activity;
    Button llHomeMenuCekStatusTerakhirResult;
    TextView txtValueLayanan, txtValueNama, txtValueNoHP, txtValueNamaKe, txtValueNoRek, txtValueRekTujuan, txtValueJumlah, txtValueNoRef,txtValueDesc;
    String rmResult, titleMenu;
    LinearLayout rowRekTujuan;
    int menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_cek_status_terakhir_result_transfer);
        initUI();
    }

    private void initUI() {
        activity = this;

        txtValueNama = findViewById(R.id.txtValueNama);
        txtValueLayanan = findViewById(R.id.txtValueLayanan);
        txtValueNoHP = findViewById(R.id.txtValueNoHP);
        txtValueNamaKe = findViewById(R.id.txtValueNamaKe);
        txtValueNoRek = findViewById(R.id.txtValueNoRek);
        txtValueRekTujuan = findViewById(R.id.txtValueRekTujuan);
        txtValueJumlah = findViewById(R.id.txtValueJumlah);
        rowRekTujuan = findViewById(R.id.rowRekeningTujuan);
        txtValueDesc = findViewById(R.id.txtValueDesc);
//        txtValueNoRef = findViewById(R.id.txtValueNoRef);
        llHomeMenuCekStatusTerakhirResult = findViewById(R.id.llHomeMenuCekStatusTerakhirResult);
        llHomeMenuCekStatusTerakhirResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();
        rmResult = bundle.getString(ConstantTransaction.BUNDLE_RM);
        titleMenu = bundle.getString(ConstantTransaction.BUNDLE_TITLE);
        menu = bundle.getInt("menu");
        //jika menu=1 maka tampilkan view transfer ke redk bpd diy, jika menu=2 maka tampilkan transfer ke rek laku pandai

        setResponseValue(rmResult);
    }

    private void setResponseValue(String rmResult) {
        try {
            final JSONObject jsonRM = new JSONObject(rmResult);
            final JSONArray arrayData = jsonRM.getJSONArray("data");
            final JSONArray jsonArray = new JSONArray(arrayData.toString());
            final HashMap<String, String> list = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = jsonObject1.optString("header");
                String value2 = jsonObject1.optString("value");
                list.put(value1, value2);
            }

            //String ref = list.get("Ref");
            String destinationAccount="not set";
            if(menu==2){
                 destinationAccount = list.get("No Tujuan");
                 rowRekTujuan.setVisibility(View.GONE);
                 txtValueRekTujuan.setText("No Hp Tujuan");
            }else{
                 destinationAccount = list.get("No Rek. Penerima");
            }
            String layanan = list.get("Layanan");
            String destinationName = list.get("Nama Penerima");
            String amount = list.get("Jumlah");
            String fromName = list.get("Nama Pengirim");
            String fromMsisdn = list.get("No HP Pengirim");
            String status = list.get("Keterangan");


            txtValueNama.setText(fromName);
            txtValueLayanan.setText(layanan);
            txtValueNoHP.setText(fromMsisdn);
            txtValueNamaKe.setText(destinationName);
            txtValueNoRek.setText(destinationAccount);
            txtValueRekTujuan.setText(destinationName);
            txtValueJumlah.setText("Rp. " + amount);
            txtValueDesc.setText(status);
//            txtValueNoRef.setText(": " + ref);
//            txtValueNoRef.setTypeface(null, Typeface.BOLD);

        } catch (Exception e) {
            Function.showAlert(activity, e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(activity, MainMenu.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }
}
