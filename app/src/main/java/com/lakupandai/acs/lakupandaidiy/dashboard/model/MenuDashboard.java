package com.lakupandai.acs.lakupandaidiy.dashboard.model;

public class MenuDashboard {
    private Integer icon;
    private String title;

    public MenuDashboard(Integer icon, String title) {
        this.icon = icon;
        this.title = title;
    }

    public MenuDashboard() {
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
