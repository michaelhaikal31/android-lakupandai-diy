package com.lakupandai.acs.lakupandaidiy.dashboard.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lakupandai.acs.lakupandaidiy.R;

import java.util.List;

public class SlidingImageAdapter extends PagerAdapter {


    private List<Bitmap> promos;
    private LayoutInflater inflater;
    private Context context;

    public SlidingImageAdapter(Context context, List<Bitmap> promos) {
        this.context = context;
        this.promos=promos;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return promos.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages_layout, view, false);
        final ImageView imageView =  imageLayout.findViewById(R.id.imgLogin);
        imageView.setImageBitmap(promos.get(position));
        view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }



}