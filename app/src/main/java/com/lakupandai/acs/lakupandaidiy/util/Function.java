package com.lakupandai.acs.lakupandaidiy.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.alert.DialogAlert;
import com.lakupandai.acs.lakupandaidiy.alert.DialogAlertHome;
import com.lakupandai.acs.lakupandaidiy.alert.DialogAlertInquiry;

import org.apache.commons.codec.binary.Base64;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.zip.Inflater;

import static android.content.Context.MODE_PRIVATE;

public class Function {
    public static Context mContext;
    public static TripleDES tripleDES;

    public static void showProgressDialogLoading(Context mContext, ProgressDialog progressDialog) {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage(Constant.LOADING);
        progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }





    public static void showAlertHome(Activity mContext, String result) {
        Constant.ALERT_RM = result;
        final DialogAlertHome callCenterDialog = new DialogAlertHome();
        callCenterDialog.show(mContext.getFragmentManager(), "Alert");
    }

    public static void showAlertInquiry(Activity mContext, String result) {
        Constant.ALERT_RM = result;
        final DialogAlertInquiry callCenterDialog = new DialogAlertInquiry();
        callCenterDialog.show(mContext.getFragmentManager(), "Alert Inquiry");
    }

    public static String removePoint(String in) {
        String hasil = in.replace(".", "");
        return hasil;
    }

    public static String getDateNow() {
        DateFormat dateFormat = new SimpleDateFormat("yyyymmddhism");
        Date date = new Date();

        return dateFormat.format(date);
    }

    public static String getdatetime() {
        Date datenow = new Date();
        //System.out.println("Waktu sekarang: " + datenow.getTime());
        return String.valueOf(datenow.getTime());
    }

    public static String encryptSharedPref(String data) {
        String result = "";
        try {
            tripleDES = new TripleDES();
            result = tripleDES.encrypt(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String decryptSharedPref(String data) {
        String result = "";
        try {
            tripleDES = new TripleDES();
            result = tripleDES.decrypt(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getValueSharedPref(String idPref, Context c) {
        String result = "";
        try {
            SharedPreferences shared = c.getSharedPreferences(Constant.MY_PREF, MODE_PRIVATE);
            result = (shared.getString(idPref, ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Function.decryptSharedPref(result);
    }

    public static String returnFormattedDate(String tgl) {
        String oldDateString = tgl;
        String newDateString = "";
        try {
            final String OLD_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
            final String NEW_FORMAT = "dd-MM-yyyy HH:mm:ss.SSS";

            // August 12, 2010
            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
            Date dd = sdf.parse(oldDateString);
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(dd);
            //System.out.println("INIH: " + newDateString);
            return newDateString;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newDateString;
    }

    public static String encodeBase64(String str) {
        byte[] bytesEncoded = Base64.encodeBase64(str.getBytes());
//        System.out.println("encoded value is " + new String(bytesEncoded));
        return new String(bytesEncoded);
    }

    public static String decodeBase64(String bytesEncoded) {
        //Decode data on other side, by processing encoded data
        byte[] valueDecoded = Base64.decodeBase64(bytesEncoded.getBytes());
        return new String(valueDecoded);
    }

    public static String getCurrentDateTime(){
        String monthName="--";
        final Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

        switch (calendar.get(Calendar.MONTH)){
            case 0:
                monthName="Januari";
                break;
            case 1:
                monthName="Februari";
                break;
            case 2:
                monthName="Maret";
                break;
            case 3:
                monthName="April";
                break;
            case 4:
                monthName="Mei";
                break;
            case 5:
                monthName="Juni";
                break;
            case 6:
                monthName="Juli";
                break;
            case 7:
                monthName="Agustus";
                break;
            case 8:
                monthName="September";
                break;
            case 9:
                monthName="Oktober";
                break;
            case 10:
                monthName="November";
                break;
            case 11:
                monthName="Desember";
                break;
        }

        final DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        final Date date = new Date();
        return calendar.get(Calendar.DAY_OF_MONTH)+" "+monthName+" "+calendar.get(Calendar.YEAR)+" "+dateFormat.format(date);
    }

    public static String getRupiah(String param){
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String jenis="";
        if(param.contains("D") || param.contains("K") || param.contains("N")){
            jenis = param.substring(0,1);
            param = param.replace(jenis+" ","");
            jenis = "("+jenis+")";
        }
        return jenis+" "+formatRupiah.format((double) Integer.valueOf(param));
    }

    public static void showListenerOkDialogMessage(Context context,LayoutInflater inflater,String message,final InputSenderDialogListener listener){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View dialogView = inflater.inflate(R.layout.dialog_alert, null);
        builder.setView(dialogView);
        dialogView.findViewById(R.id.btOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onOK("");
            }
        });
        final TextView tvRM = dialogView.findViewById(R.id.tvRM);
        tvRM.setText(message);
        builder.create().show();
    }

    public static void showListenerFinishDialogMessage(final Context context, LayoutInflater inflater, String message, final InputSenderDialogListener listener){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View dialogView = inflater.inflate(R.layout.dialog_alert, null);
        builder.setView(dialogView);
        final TextView tvRM = dialogView.findViewById(R.id.tvRM);
        tvRM.setText(message);
        final Dialog dialog = builder.create();

        dialogView.findViewById(R.id.btOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                listener.onOK("");
            }
        });
        dialog.show();
    }



    public static void showAlert(Activity mContext, String result) {
        Constant.ALERT_RM = result;
        final DialogAlert callCenterDialog = new DialogAlert();
        callCenterDialog.show(mContext.getFragmentManager(), "Alert");
    }

    public interface InputSenderDialogListener{
        public abstract void onOK(String value);
        public abstract void onCancel(String value);
    }
}
