package com.lakupandai.acs.lakupandaidiy.saldo.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.ImageView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Erdy on 12/04/2018.
 */
public class MenuCekSaldo extends HomeActivity {

    private String noHPAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    private Activity activity;
    private ImageView llCekSaldoNasabahMenuCekSaldo, llCekSaldoAgenMenuCekSaldo, llCekStatusTerakhir, llHomeMenuCekSaldo, llMutasiRekening;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_cek_saldo);
        initUI();
    }

    private void initUI() {
        activity = this;
        llCekSaldoNasabahMenuCekSaldo = (ImageView) findViewById(R.id.llCekSaldoNasabahMenuCekSaldo);
        llCekSaldoNasabahMenuCekSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, MenuCekSaldoNasabah.class);
                startActivity(i);

            }
        });
        llCekSaldoAgenMenuCekSaldo = (ImageView) findViewById(R.id.llCekSaldoAgenMenuCekSaldo);
        llCekSaldoAgenMenuCekSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, MenuCekSaldoAgen.class);
                startActivity(i);

            }
        });

        llMutasiRekening = (ImageView) findViewById(R.id.llMutasiRekening);
        llMutasiRekening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(mContext, MenuMutasiRekeningDetail.class);
//                startActivity(i);
//                finish();
                new executeMutasi(activity).execute();
            }
        });

        llCekStatusTerakhir = (ImageView) findViewById(R.id.llCekStatusTerakhir);
        llCekStatusTerakhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, MenuCekStatusTerakhir.class);
                startActivity(i);

            }
        });

        llHomeMenuCekSaldo = (ImageView) findViewById(R.id.llHomeMenuCekSaldo);
        llHomeMenuCekSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    class executeMutasi extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeMutasi(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                String dateDefault = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.START_DATE, dateDefault);
                json.put(ConstantTransaction.END_DATE, dateDefault);
                json.put(ConstantTransaction.PAGE_JSON, "0");
                json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_MUTASI);
                //Log.v(Constant.LOG_LOGIN, Constant.MESSAGE_LOG_LOGIN);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);

                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuCekSaldo.this);
//                System.out.println("Result json: "+resultfromJson);
                resultfromJsonDecrypt = tripleDES.decrypt(resultfromJson);
//                System.out.println("RESPONSE DATA: " + resultfromJson);
//                System.out.println("RESPONSE DATA: " + resultfromJsonDecrypt);

                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;

            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.SERVER_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.SERVER_ERROR);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Nilai Result JSONParser Decrypt : " + resultfromJsonDecrypt.replaceAll("\\\\", ""));
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);

                    //lastLogin = jsonObject.getString(Constant.LAST_LOGIN);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        JSONObject jsonA = new JSONObject(rmResult);
                        try {
                            Constant.jsonB = jsonA.getJSONArray("mutasi");
                            Constant.sizePerPage = jsonA.getInt("sizePerPage");
                            Constant.totalPages = jsonA.getInt("totalPages");
                            Constant.totalMutasi = jsonA.getInt("totalMutasi");
                            Constant.page = jsonA.getInt("page");
                            Constant.rc = jsonA.getInt("rc");
                        } catch (Exception e) {
                            Constant.jsonB = new JSONArray();
                            Constant.sizePerPage = 10;
                            Constant.totalPages = 999999;
                            Constant.totalMutasi = 999999;
                            Constant.page = 0;
                            Constant.rc = 0;
                        }

//                        System.out.println("JSONArray : "+ jsonB.toString());
                        Intent i = new Intent(activity, MenuMutasiRekeningDetail.class);
                        startActivity(i);
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                    Function.showAlert(mContext, strResponse);
                    Function.showAlert(activity, e.toString());
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
