package com.lakupandai.acs.lakupandaidiy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.notifikasian.model.Notifikasi;
import com.lakupandai.acs.lakupandaidiy.util.Function;

import java.util.ArrayList;

/**
 * Created by Erdy on 12/05/2018.
 */
public class NotifikasiAdapter extends ArrayAdapter<Notifikasi> {

    private static class ViewHolder {
        TextView txtTitle,txtTanggal,txtNominal,txtResi;
        View imageView;
    }

    public NotifikasiAdapter(Context context, ArrayList<Notifikasi> users) {
        super(context, R.layout.adapter_notifikasi, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Notifikasi user = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.adapter_notifikasi, parent, false);
            viewHolder.txtTitle =  convertView.findViewById(R.id.txtTitle);
            viewHolder.txtTanggal =  convertView.findViewById(R.id.txtTanggal);
            viewHolder.txtNominal = convertView.findViewById(R.id.txtNominal);
            viewHolder.txtResi =  convertView.findViewById(R.id.txtResi);
            viewHolder.imageView = convertView.findViewById(R.id.newNotif);
//            viewHolder.tvAgent = (TextView) convertView.findViewById(R.id.tvAgent);
            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.txtTitle.setText(user.getTitle());
        viewHolder.txtTanggal.setText(user.getTanggal());
        viewHolder.txtNominal.setText(Function.getRupiah(user.getTotalAmount()));
        viewHolder.txtResi.setText("Resi."+user.getResi());
        if(user.isAlreadyRead()){
            viewHolder.imageView.setVisibility(View.INVISIBLE);
        }else{
            viewHolder.imageView.setVisibility(View.VISIBLE);
        }
        // Return the completed view to render on screen
        return convertView;
    }

    private void removeBlueDot(int pos){

    }
}
