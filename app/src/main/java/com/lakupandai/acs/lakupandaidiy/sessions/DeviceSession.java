package com.lakupandai.acs.lakupandaidiy.sessions;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;

public class DeviceSession {

    public static void setTempRequestUrl(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("deviceSession", Context.MODE_PRIVATE).edit();
        editor.putString("temp_url", value);
        editor.commit();

    }

    public static String getTempRequestUrl(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("deviceSession",	Context.MODE_PRIVATE);
        String nma = prefs.getString("temp_url", "");
        return nma;
    }

    public static void setTempRequestBody(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("deviceSession", Context.MODE_PRIVATE).edit();
        editor.putString("temp_body", value);
        editor.commit();

    }

    public static String getTempRequestBody(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("deviceSession",	Context.MODE_PRIVATE);
        String nma = prefs.getString("temp_body", "");
        return nma;
    }

    public static void setNewNotif(Context context, Boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("deviceSession", Context.MODE_PRIVATE).edit();
        editor.putBoolean("new_notif", value);
        editor.commit();
    }

    public static Boolean getNewNotif(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("deviceSession",	Context.MODE_PRIVATE);
        Boolean nma = prefs.getBoolean("new_notif", false);
        return nma;
    }

    public static void setJsonNotif(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("deviceSession", Context.MODE_PRIVATE).edit();
        editor.putString("json_notif", value);
        editor.commit();
    }

    public static String getJsonNotif(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("deviceSession",	Context.MODE_PRIVATE);
        return prefs.getString("json_notif", "");
    }

    public static void setJumlahSlideImage(Context context, int value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("deviceSession", Context.MODE_PRIVATE).edit();
        editor.putInt("jumlah", value);
        editor.commit();
    }

    public static Integer getJumlahSlideImage(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("deviceSession",	Context.MODE_PRIVATE);
        return prefs.getInt("jumlah", 0);
    }



}
