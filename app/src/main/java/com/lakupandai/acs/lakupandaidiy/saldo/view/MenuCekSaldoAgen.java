package com.lakupandai.acs.lakupandaidiy.saldo.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponse;
import com.lakupandai.acs.lakupandaidiy.sms.SmsReceiver;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONObject;

/**
 * Created by Erdy on 12/04/2018.
 */
public class MenuCekSaldoAgen extends HomeActivity implements SmsReceiver.SmsListener {

    private Activity activity;
    private ImageView llHomeMenuCekSaldoAgen;
    private Button btnKirimMenuCekSaldoAgen;
    private EditText etPinAgenMenuCekSaldoAgen;
    private String noHPAgen, pin, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    String statusSMSShared;
    SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_cek_saldo_agen);
        initUI();
    }

    private void initUI() {
        activity = this;
        etPinAgenMenuCekSaldoAgen = (EditText) findViewById(R.id.etPinAgenMenuCekSaldoAgen);

        llHomeMenuCekSaldoAgen = (ImageView) findViewById(R.id.llHomeMenuCekSaldoAgen);
        llHomeMenuCekSaldoAgen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnKirimMenuCekSaldoAgen = (Button) findViewById(R.id.btnKirimMenuCekSaldoAgen);
        btnKirimMenuCekSaldoAgen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etPinAgenMenuCekSaldoAgen.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etPinAgenMenuCekSaldoAgen.getText().toString().length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else {
                    pin = etPinAgenMenuCekSaldoAgen.getText().toString();
                    new executeCekSaldoAgen(activity).execute();
                }
            }
        });

    }

    @Override
    public void smsReceived(String msgFrom, String msgBody) {
//        System.out.println("From: " + msgFrom);
//        System.out.println("Body: " + msgBody);

//        handler.removeCallbacks(runnable);
//        spinnerDialog.dismissAllowingStateLoss();
//        String hasilRCSms = msgBody.substring(4, 6);
//        String hasilRMSms = msgBody.substring(7, msgBody.length());
//
//        if (hasilRCSms.equals(Constant.SUCCESS_CODE)) {
//            Intent i = new Intent(mContext, MenuHasilResponse.class);
//            Bundle bundle = new Bundle();
//            bundle.putString(Constant.BUNDLE_TITLE, Constant.BUNDLE_TITLE_CEK_STATUS_TERAKHIR);
//            bundle.putString(Constant.BUNDLE_RC, hasilRCSms);
//            bundle.putString(Constant.BUNDLE_RM, hasilRMSms);
//            i.putExtras(bundle);
//            startActivity(i);
//            finish();
//        } else {
//            Function.showAlert(mContext, hasilRMSms);
//        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        //  unregisterReceiver(receiver);
    }

    class executeCekSaldoAgen extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeCekSaldoAgen(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN_CEK_SALDO_AGEN, noHPAgen);
                json.put(ConstantTransaction.PIN_CEK_SALDO_AGEN, pin);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_CEK_SALDO_AGEN);
                Log.v(ConstantTransaction.LOG_CEK_SALDO_AGEN, ConstantTransaction.MESSAGE_LOG_CEK_SALDO_AGEN);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Response Send "+resultEnc);
                System.out.println("Response Send Decrypt "+resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuCekSaldoAgen.this);
                System.out.println("Response : "+resultfromJson);
                System.out.println("Response decrypt: " + tripleDES.decrypt(resultfromJson));
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive "+result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt "+resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Intent i = new Intent(activity, MenuResponseSaldo.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.BUNDLE_TITLE, ConstantTransaction.BUNDLE_TITLE_CEK_SALDO_AGEN);
                        bundle.putString(ConstantTransaction.BUNDLE_RC, rcResult);
                        bundle.putString(ConstantTransaction.BUNDLE_RM, rmResult);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
