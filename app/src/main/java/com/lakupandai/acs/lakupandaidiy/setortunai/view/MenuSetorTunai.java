package com.lakupandai.acs.lakupandaidiy.setortunai.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.NumberTextWatcherForThousand;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Erdy on 12/04/2018.
 */
public class MenuSetorTunai extends HomeActivity {
    private Activity activity;
    private ImageView llHomeMenuSetorTunai;
    private EditText etNomorHPMenuSetorTunai, etNominalMenuSetorTunai;
    private Button btnKirimMenuSetorTunai;
    private String current;
    private String noHPAgen, noHpNasabah, nominal, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    public static String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_setor_tunai);
        initUI();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // unregisterReceiver(receiver);
    }

    private void initUI() {
        activity = this;

        etNomorHPMenuSetorTunai = (EditText) findViewById(R.id.etNomorHPMenuSetorTunai);

        llHomeMenuSetorTunai = (ImageView) findViewById(R.id.llHomeMenuSetorTunai);
        llHomeMenuSetorTunai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etNominalMenuSetorTunai = (EditText) findViewById(R.id.etNominalMenuSetorTunai);
        etNominalMenuSetorTunai.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (!s.toString().equals(current)) {
                        etNominalMenuSetorTunai.removeTextChangedListener(this);
                        String cleanString = s.toString().replaceAll("[.]", "");
                        String formatted = null;
                        if (!cleanString.isEmpty()) {
                            double parsed = Double.parseDouble(cleanString);

                            formatted = NumberTextWatcherForThousand.formatAmount(parsed);
                        }
                        current = formatted;
                        etNominalMenuSetorTunai.setText(formatted);
                        if (!cleanString.isEmpty()) {
                            etNominalMenuSetorTunai.setSelection(formatted.length());
                        }
                        etNominalMenuSetorTunai.addTextChangedListener(this);
                    }
                } catch (Exception e) {
                    Toast.makeText(activity, ConstantError.ALERT_NOMINAL_INPUT_FALSE, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        btnKirimMenuSetorTunai = (Button) findViewById(R.id.btnKirimMenuSetorTunai);
        btnKirimMenuSetorTunai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etNomorHPMenuSetorTunai.getText().toString().isEmpty() || etNominalMenuSetorTunai.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etNomorHPMenuSetorTunai.getText().toString().length() < 10) {
                    Function.showAlert(activity, ConstantError.ALERT_NUMBER_LESS_THAN_10);
                } else if (Long.parseLong(Function.removePoint(etNominalMenuSetorTunai.getText().toString()))<1){
                    Function.showAlert(activity, ConstantError.ALERT_NOMINAL_LESS_THAN_1);
                }else {
                    noHpNasabah = etNomorHPMenuSetorTunai.getText().toString();
                    nominal = Function.removePoint(etNominalMenuSetorTunai.getText().toString());
                    new executeCekStatusTerakhir(activity).execute();
                }
            }
        });
    }


    class executeCekStatusTerakhir extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeCekStatusTerakhir(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN_SETOR_TUNAI, noHPAgen);
                json.put(ConstantTransaction.MSISDN_NASABAH_SETOR_TUNAI, noHpNasabah);
                json.put(ConstantTransaction.NOMINAL_SETOR_TUNAI, nominal);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_SETOR_TUNAI);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("asd param [menu setor tunai] "+json.toString());
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuSetorTunai.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    token = jsonObject.getString(ConstantTransaction.TOKEN_REK_BARU);
                    //lastLogin = jsonObject.getString(Constant.LAST_LOGIN);
                    System.out.println("Nilai Result JSONParser resultfromJsonDecrypt : " +resultfromJsonDecrypt);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        JSONObject jsonDATA = new JSONObject(rmResult);
                        String data = jsonDATA.getString(ConstantTransaction.DATA);
                        Constant.dataResult = new JSONArray(data);
                        Intent i = new Intent(MenuSetorTunai.this, MenuKonfirmasiSetorTunai.class);
                        startActivity(i);
                        finish();
                    }else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                    Function.showAlert(mContext, strResponse);
                    Function.showAlert(activity, e.toString());
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }


}
