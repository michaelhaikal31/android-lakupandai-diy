package com.lakupandai.acs.lakupandaidiy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.model.MenuDashboard;

import java.util.ArrayList;

/**
 * Created by Erdy on 12/02/2018.
 */
public class GridAdapter extends BaseAdapter {
    Context context;
    ArrayList<MenuDashboard> menus = new ArrayList<>();
    private static LayoutInflater inflater = null;

    public GridAdapter(Context mContext, ArrayList<MenuDashboard> listMenu) {
        context = mContext;
        menus = listMenu;

    }

    @Override
    public int getCount() {
        return menus.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class Holder{
        ImageView imgGambar;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final MenuDashboard menu = menus.get(i);
        if (view == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(context);
            view = layoutInflater.inflate(R.layout.menu_dashboard, null);
        }
        final ImageView imageView = view.findViewById(R.id.iconMenuDashboard);
        final TextView textView = view.findViewById(R.id.txtMenuDashboard);
        imageView.setImageResource(menu.getIcon());
        textView.setText(menu.getTitle());
        return view;
    }
}
