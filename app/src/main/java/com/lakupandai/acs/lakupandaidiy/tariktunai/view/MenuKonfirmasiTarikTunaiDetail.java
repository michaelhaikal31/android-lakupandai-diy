package com.lakupandai.acs.lakupandaidiy.tariktunai.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponse;
import com.lakupandai.acs.lakupandaidiy.pembayaran.view.MenuKonfirmasiPembayaran;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.sms.SmsReceiver;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.NotificationHelper;
import com.lakupandai.acs.lakupandaidiy.util.SpinnerDialog;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Erdy on 12/05/2018.
 */
public class MenuKonfirmasiTarikTunaiDetail extends HomeActivity implements SmsReceiver.SmsListener {

    private Activity activity;
    private ImageView llHomeMenuKonfirmasiTarikTunaiDetail;
    private TextView txtIsiMenuKonfirmasiTarikTunaiDetail;
    private EditText etPinAgenMenuKonfirmasiTarikTunaiDetail;
    private Button btnKirimMenuKonfirmasiTarikTunaiDetail;
    private String rmBundle, tokenBundle, noHPAgen, pinAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();
    private TextView txtTextNoHP, txtValueNoHP, txtTextNama, txtValueNama, txtTextJumlah, txtValueJumlah, txtTextWaktu, txtValueWaktu;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_konfirmasi_tarik_tunai_detail);
        initUI();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        unregisterReceiver(receiver);
    }


    @Override
    public void smsReceived(String msgFrom, String msgBody) {
//        System.out.println("From: " + msgFrom);
//        System.out.println("Body: " + msgBody);
//
//        handler.removeCallbacks(runnable);
//        spinnerDialog.dismissAllowingStateLoss();
//        String hasilRCSms = msgBody.substring(4, 6);
//        String hasilRMSms = msgBody.substring(7, msgBody.length());
//
//
//        if (hasilRCSms.equals(Constant.SUCCESS_CODE)) {
//            Intent i = new Intent(mContext, MenuHasilResponse.class);
//            Bundle bundle = new Bundle();
//            bundle.putString(Constant.BUNDLE_TITLE, Constant.BUNDLE_TITLE_KONF_TARIK_TUNAI);
//            bundle.putString(Constant.BUNDLE_RM, hasilRMSms);
//            i.putExtras(bundle);
//            startActivity(i);
//            finish();
//        } else {
//            Function.showAlert(mContext, hasilRMSms);
//        }
    }


    private void initUI() {
        activity = this;

//        final IntentFilter filter = new IntentFilter();
//        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
//        filter.setPriority(1000);
//        receiver = new SmsReceiver(this);
//        registerReceiver(receiver, filter);

        etPinAgenMenuKonfirmasiTarikTunaiDetail = (EditText) findViewById(R.id.etPinAgenMenuKonfirmasiTarikTunaiDetail);

        txtTextNoHP = findViewById(R.id.txtTextNoHP);
        txtValueNoHP = findViewById(R.id.txtValueNoHP);
        txtTextNama = findViewById(R.id.txtTextNama);
        txtValueNama = findViewById(R.id.txtValueNama);
        txtTextJumlah = findViewById(R.id.txtTextJumlah);
        txtValueJumlah = findViewById(R.id.txtValueJumlah);
        txtTextWaktu = findViewById(R.id.txtTextWaktu);
        txtValueWaktu = findViewById(R.id.txtValueWaktu);

        Bundle bundle = getIntent().getExtras();
        rmBundle = bundle.getString(ConstantTransaction.BUNDLE_RM);
        tokenBundle = bundle.getString(ConstantTransaction.TOKEN);
        System.out.println("MenuKonfirmasiTarikTunaiDetail "+rmBundle);
//        txtIsiMenuKonfirmasiTarikTunaiDetail = (TextView) findViewById(R.id.txtIsiMenuKonfirmasiTarikTunaiDetail);
//        txtIsiMenuKonfirmasiTarikTunaiDetail.setText(rmBundle);
        setResponseValue(rmBundle);

        btnKirimMenuKonfirmasiTarikTunaiDetail = (Button) findViewById(R.id.btnKirimMenuKonfirmasiTarikTunaiDetail);
        btnKirimMenuKonfirmasiTarikTunaiDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etPinAgenMenuKonfirmasiTarikTunaiDetail.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etPinAgenMenuKonfirmasiTarikTunaiDetail.getText().toString().length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else {
                    pinAgen = etPinAgenMenuKonfirmasiTarikTunaiDetail.getText().toString();
                    new executeSendTokenAndPin(activity).execute();
                }
            }
        });

        llHomeMenuKonfirmasiTarikTunaiDetail = (ImageView) findViewById(R.id.llHomeMenuKonfirmasiTarikTunaiDetail);
        llHomeMenuKonfirmasiTarikTunaiDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }


    private void setResponseValue(String rmResult) {
        try {
            System.out.println("Response rmResult "+rmResult);
            JSONObject jsonRM = new JSONObject(rmResult);
            JSONArray arrayData = jsonRM.getJSONArray("data");
            System.out.println("arrayData " + arrayData);

            JSONArray jsonArray = new JSONArray(arrayData.toString());
            HashMap<String, String> list = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = jsonObject1.optString("header");
                String value2 = jsonObject1.optString("value");
                list.put(value1, value2);
            }

//            String waktu = list.get("waktu");
            String msisdn = list.get("No HP");
            String namaNasabah = list.get("Nama Nasabah");
            String jumlah = list.get("Jumlah");

            System.out.println("MenuKonfirmasiTarikTunaiDetail "+msisdn);
            System.out.println("MenuKonfirmasiTarikTunaiDetail "+namaNasabah);
            System.out.println("MenuKonfirmasiTarikTunaiDetail "+jumlah);
            txtTextNoHP.setText("No HP Nasabah ");
            txtValueNoHP.setText(msisdn);
            txtTextNama.setText("Nama ");
            txtValueNama.setText(namaNasabah);
            txtTextJumlah.setText("Jumlah ");
            txtValueJumlah.setText(jumlah);
            txtTextWaktu.setText("Waktu ");
            txtValueWaktu.setText(getDateTime());
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private String getDateTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
        System.out.println( sdf.format(cal.getTime()) );
        return sdf.format(cal.getTime());
    }

    class executeSendTokenAndPin extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeSendTokenAndPin(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN_KONF_TARIK_TUNAI_DETAIL, noHPAgen);
                json.put(ConstantTransaction.TOKEN_KONF_TARIK_TUNAI_DETAIL, tokenBundle);
                json.put(ConstantTransaction.PIN_KONF_TARIK_TUNAI_DETAIL, pinAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_KONFIRMASI_TARIK_TUNAI_DETAIL);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Response Send " + resultEnc);
                System.out.println("Response Send Decrypt " + resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuKonfirmasiTarikTunaiDetail.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        DeviceSession.setNewNotif(MenuKonfirmasiTarikTunaiDetail.this, true);
                        NotificationHelper.CreateNotification(MenuKonfirmasiTarikTunaiDetail.this, "Tarik tunai berhasil","Tekan untuk melihat transaksi di kotak masuk");
                        Intent i = new Intent(activity, MenuTarikTunaiResult.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.BUNDLE_TITLE, ConstantTransaction.BUNDLE_TITLE_KONF_TARIK_TUNAI);
                        bundle.putString(ConstantTransaction.BUNDLE_RM, rmResult);

                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(activity, MenuKonfirmasiTarikTunai.class);
        startActivity(i);
        finish();
    }

}
