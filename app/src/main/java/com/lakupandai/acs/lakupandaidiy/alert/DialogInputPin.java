package com.lakupandai.acs.lakupandaidiy.alert;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponse;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuProfil;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONObject;

/**
 * Created by ACS-Design on 8/16/2017.
 */
public class DialogInputPin extends DialogFragment implements View.OnClickListener {
    EditText etPin;
    private String noHPAgen, pin, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    public static DialogInputPin createInstance() {
        return new DialogInputPin();
    }


    public Activity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dlg = super.onCreateDialog(savedInstanceState);
//        dlg.setTitle("Information");
        dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dlg;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.dialog_input_pin, container);
        view.findViewById(R.id.btKirim).setOnClickListener(this);
        etPin = view.findViewById(R.id.etPin);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btKirim:
                if (etPin.getText().toString().isEmpty()) {
                    Function.showAlert(getActivity(), ConstantError.ALERT_FIELD_EMPTY);
                } else if (etPin.getText().toString().length() < 6) {
                    Function.showAlert(getActivity(), ConstantError.ALERT_PIN_LESS_THAN_6);
                } else {
                    pin = etPin.getText().toString();
                    new executeCekSaldoAgen(getActivity()).execute();
                    dismissAllowingStateLoss();
                }
                break;
            default:
                throw new IllegalArgumentException("Unrecognized id");
        }

    }

    class executeCekSaldoAgen extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeCekSaldoAgen(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, getActivity());
                json.put(ConstantTransaction.MSISDN_AGEN_CEK_SALDO_AGEN, noHPAgen);
                json.put(ConstantTransaction.PIN_CEK_SALDO_AGEN, pin);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_CEK_SALDO_AGEN);
                Log.v(ConstantTransaction.LOG_CEK_SALDO_AGEN, ConstantTransaction.MESSAGE_LOG_CEK_SALDO_AGEN);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, getActivity());
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(getActivity(), ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(getActivity(), ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Intent i = new Intent(activity, MenuProfil.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.BUNDLE_TITLE, ConstantTransaction.BUNDLE_TITLE_CEK_SALDO_AGEN);
                        bundle.putString(ConstantTransaction.BUNDLE_RC, rcResult);
                        bundle.putString(ConstantTransaction.BUNDLE_RM, rmResult);
                        i.putExtras(bundle);
                        activity.startActivity(i);
                        activity.finish();
                    } else {
                        dismissAllowingStateLoss();
//                        System.out.println(rmResult);
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    dismissAllowingStateLoss();
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }
}
