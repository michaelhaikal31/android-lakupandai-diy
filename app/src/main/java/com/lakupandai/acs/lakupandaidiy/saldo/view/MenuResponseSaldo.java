package com.lakupandai.acs.lakupandaidiy.saldo.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Erdy on 12/19/2018.
 */
public class MenuResponseSaldo extends Activity {

    private Button llHomeMenuHasilResponse, btShare;
    private FloatingActionButton fab;
    private Context mContext;
    private TextView txtJudulMenuHasilResponse, txtHasilResponsePesanMenuHasilResponse;
    private String rmResult, titleMenu;
    private TextView txtTextNama, txtValueNama, txtTextUsername, txtValueUsername, txtTextTanggal, txtValueTanggal, txtTextNoRek, txtValueNoRek, txtTextJumlah, txtValueJumlah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_response_saldo);
        initUI();
    }

    private void initUI() {
        mContext = this;
        txtJudulMenuHasilResponse = (TextView) findViewById(R.id.txtJudulMenuHasilResponse);
        llHomeMenuHasilResponse = (Button) findViewById(R.id.llHomeMenuHasilResponse);
        txtTextNama = (TextView) findViewById(R.id.txtTextNama);
        txtValueNama = (TextView) findViewById(R.id.txtValueNama);
        txtTextUsername = (TextView) findViewById(R.id.txtTextUsername);
        txtValueUsername = (TextView) findViewById(R.id.txtValueUsername);
        txtTextTanggal = (TextView) findViewById(R.id.txtTextTanggal);
        txtValueTanggal = (TextView) findViewById(R.id.txtValueTanggal);
        txtTextNoRek = (TextView) findViewById(R.id.txtTextNoRek);
        txtValueNoRek = (TextView) findViewById(R.id.txtValueNoRek);
        txtTextJumlah = (TextView) findViewById(R.id.txtTextJumlah);
        txtValueJumlah = (TextView) findViewById(R.id.txtValueJumlah);


        Bundle bundle = getIntent().getExtras();
        rmResult = bundle.getString(ConstantTransaction.BUNDLE_RM);
        titleMenu = bundle.getString(ConstantTransaction.BUNDLE_TITLE);

        System.out.println("Response RM " + rmResult);
        System.out.println("Response title " + titleMenu);
        txtJudulMenuHasilResponse.setText("INFORMASI SALDO");
        txtJudulMenuHasilResponse.setTypeface(null, Typeface.BOLD);
        // txtHasilResponsePesanMenuHasilResponse.setText(rmResult);

        txtTextNama.setText("Nama");
        txtTextUsername.setText("Username");
        txtTextTanggal.setText("Tanggal");
        txtTextNoRek.setText("No. Rekening");
        txtTextJumlah.setText("Jumlah");

        setResponseValue(rmResult);

        llHomeMenuHasilResponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    public String setDateTime(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm");
        System.out.println( sdf.format(cal.getTime()) );
        return sdf.format(cal.getTime());
    }

    private void setResponseValue(String rmResult) {
        try {
            JSONObject jsonRM = new JSONObject(rmResult);

            JSONArray arrayData = jsonRM.getJSONArray("data");
            System.out.println("arrayData " + arrayData);

            JSONArray jsonArray = new JSONArray(arrayData.toString());
            HashMap<String, String> list = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = jsonObject1.optString("header");
                String value2 = jsonObject1.optString("value");
                list.put(value1, value2);
            }

            String userId = list.get("User Id");
            String userName = list.get("User Name");
            String userAccount = list.get("User Account");
            String layanan = list.get("Layanan");
            String tanggal = list.get("Tanggal");
            String balance = list.get("Balance");

            String msisdn = list.get("Msisdn");

            txtValueNama.setText(userName);
            txtValueUsername.setText(userId);
            txtValueTanggal.setText(setDateTime());
            txtValueNoRek.setText(userAccount);
            txtValueJumlah.setText("Rp. " + balance);

            System.out.println(list.get("User Id"));
            System.out.println(list.get("User Name"));
            System.out.println(list.get("User Account"));
            System.out.println(list.get("Layanan"));
            System.out.println(list.get("Tanggal"));
            System.out.println(list.get("Msisdn"));
            System.out.println(list.get("Balance"));

//            {"RC":"00","RM":"{\"data\":[{\"header\":\"User Id\",\"value\":\"fitri9832\"},{\"header\":\"User Name\",\"value\":\"FITRI NUR HARYATI\"},{\"header\":\"User Account\",\"value\":\"001221033667\"},{\"header\":\"Layanan\",\"value\":\"710000\"},{\"header\":\"Tanggal\",\"value\":\"19-12-2018\"},{\"header\":\"Msisdn\",\"value\":\"08988989832\"},{\"header\":\"Balance\",\"value\":\"40.259.100\"}]}"}

        } catch (Exception e) {
            Function.showAlert(this,"Terjadi kesalahan ");
            System.out.println("asd "+e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
