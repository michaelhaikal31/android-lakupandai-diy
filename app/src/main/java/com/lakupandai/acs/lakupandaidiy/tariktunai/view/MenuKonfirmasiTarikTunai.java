package com.lakupandai.acs.lakupandaidiy.tariktunai.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.pembelian.view.MenuKonfirmasiPembelianPulsa;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.sms.SmsReceiver;
import com.lakupandai.acs.lakupandaidiy.transfer.view.MenuTransferKonfirmasi;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.SpinnerDialog;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Erdy on 12/04/2018.
 */
public class MenuKonfirmasiTarikTunai extends HomeActivity implements SmsReceiver.SmsListener {


    private Activity activity;
    private EditText etKodeKonfirmasiMenuKonfirmasiTarikTunai;
    private Button btnKirimMenuKonfirmasiTarikTunai;
    private ImageView llHomeMenuKonfirmasiTarikTunai;
    private String kodeKonf, noHPAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();
//    public Handler mHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            if (msg.what == Constant.DISPLAY_DATA) {
//                MessageTimeout();
//            }
//        }
//    };

    private Runnable runnable;
    private Handler handler;
    SpinnerDialog spinnerDialog;
    SmsReceiver receiver;
    String statusSMSShared;
    SharedPreferences shared;
    private TextView txtProgressBar;
    private TextView txtCaption1;
    private TextView txtCaption2;
    private ProgressBar progressBar;
    private CountDownTimer countDownTimer;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_konfirmasi_tarik_tunai);
        initUI();


        txtProgressBar = findViewById(R.id.txtProgressBar);
        startCountdown();
    }
    private void startCountdown() {
        txtCaption1.setVisibility(View.VISIBLE);
        txtProgressBar.setVisibility(View.VISIBLE);
        txtCaption2.setTextColor(Color.parseColor("#808080"));
        countDownTimer = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int second = (int) ((millisUntilFinished / 1000));
                //txtProgressBar.setText(second);
                txtProgressBar.setText(String.valueOf(second));


            }

            @Override
            public void onFinish() {
//                tvResendOTP.setVisibility(View.VISIBLE);
//                tvResendOTP.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        new MenuKonfirmasiPembelianPulsa.executeResendOTP(activity).execute();
//                    }
//                });
                txtCaption1.setVisibility(View.GONE);
                txtProgressBar.setVisibility(View.INVISIBLE);
                txtProgressBar.setText("");
                txtCaption2.setText("Kirim ulang OTP");
                txtCaption2.setTextColor(getResources().getColor(R.color.colorPrimary));
                txtCaption2.setTypeface(null, Typeface.BOLD);
                txtCaption2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!DeviceSession.getTempRequestBody(activity).isEmpty()){
                            new executeResendOTP().execute();
                            startCountdown();
                        }else{
                            Function.showAlert(activity,Constant.BELUM_TRANSAKSI);
                        }

                    }
                });

            }
        };

        countDownTimer.start();

    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregisterReceiver(receiver);
    }


    @Override
    public void smsReceived(String msgFrom, String msgBody) {
//        System.out.println("From: " + msgFrom);
//        System.out.println("Body: " + msgBody);
//
//        handler.removeCallbacks(runnable);
//        spinnerDialog.dismissAllowingStateLoss();
//        String hasilRCSms = msgBody.substring(4, 6);
//        String hasilRMSms = msgBody.substring(7, msgBody.length());
//
//
//        if (hasilRCSms.equals(Constant.SUCCESS_CODE)) {
//            Intent i = new Intent(mContext, MenuKonfirmasiTarikTunaiDetail.class);
//            Bundle bundle = new Bundle();
//            bundle.putString(Constant.BUNDLE_TOKEN, kodeKonf);
//            bundle.putString(Constant.BUNDLE_RM, hasilRMSms);
//            i.putExtras(bundle);
//            startActivity(i);
//            finish();
//        } else {
//            Function.showAlert(mContext, hasilRMSms);
//        }
    }



    private void initUI() {
        activity = this;
        txtCaption1 = findViewById(R.id.textView16);
        txtCaption2 = findViewById(R.id.textView17);
        etKodeKonfirmasiMenuKonfirmasiTarikTunai = (EditText) findViewById(R.id.etKodeKonfirmasiMenuKonfirmasiTarikTunai);
        btnKirimMenuKonfirmasiTarikTunai = (Button) findViewById(R.id.btnKirimMenuKonfirmasiTarikTunai);
        btnKirimMenuKonfirmasiTarikTunai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kodeKonf = etKodeKonfirmasiMenuKonfirmasiTarikTunai.getText().toString();
                if (etKodeKonfirmasiMenuKonfirmasiTarikTunai.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_CONF_CODE_EMPTY);
                } else if (etKodeKonfirmasiMenuKonfirmasiTarikTunai.getText().toString().length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_CONF_CODE_LESS_THAN_6);
                } else {
                    new executeKonfirmasiTarikTunai(activity).execute();
                }
            }
        });

        llHomeMenuKonfirmasiTarikTunai = (ImageView) findViewById(R.id.llHomeMenuKonfirmasiTarikTunai);
        llHomeMenuKonfirmasiTarikTunai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    class executeKonfirmasiTarikTunai extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeKonfirmasiTarikTunai(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN_KONF_TARIK_TUNAI, noHPAgen);
                json.put(ConstantTransaction.TOKEN_KONF_TARIK_TUNAI, kodeKonf);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_KONFIRMASI_TARIK_TUNAI);
//                Log.v(Constant.LOG_KONFIRMASI_TARIK_TUNAI, Constant.MESSAGE_LOG_KONFIRMASI_TARIK_TUNAI);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Response Send "+resultEnc);
                System.out.println("Response Send Decrypt "+resultDec);

                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuKonfirmasiTarikTunai.this);
                //  System.out.println("Nilai Result JSONParser: " + resultfromJson);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive "+result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt "+resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        DeviceSession.setTempRequestBody(activity,"");
                        DeviceSession.setTempRequestUrl(activity,"");
                        System.out.println("MenuKonfirmasiTarikTunai "+rmResult);
                        Intent i = new Intent(activity, MenuKonfirmasiTarikTunaiDetail.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.TOKEN, kodeKonf);
                        bundle.putString(ConstantTransaction.BUNDLE_RM, rmResult);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    class executeResendOTP extends AsyncTask<String, Void, String> {
        Context context;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MenuKonfirmasiTarikTunai.this);
            progressDialog.setMessage("mengirim ulang OTP..");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... result) {
            return jsonParser.HttpRequestPost(ConstantTransaction.URL, DeviceSession.getTempRequestBody(MenuKonfirmasiTarikTunai.this), ConstantTransaction.TimeOutConnection, MenuKonfirmasiTarikTunai.this);
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {

                        Function.showAlert(MenuKonfirmasiTarikTunai.this,"OTP berhasil dikirim, silahkan masukan OTP");
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }

    }




}
