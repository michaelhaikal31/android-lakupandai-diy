package com.lakupandai.acs.lakupandaidiy.transfer.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONObject;

/**
 * Created by Erdy on 12/07/2018.
 */
public class MenuTransferAntarBankDetail extends HomeActivity {
    private Activity activity;
    private TextView tvTitle;
    private ImageView llHomeMenuTransferDetail,llRequestTransferMenuTransferDetail,llKonfirmasiTransferDetailMenuTransferDetail;

    private String noHPAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rcResult;
    public static String rmResult;

    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_transfer_antar_bank_detail);
        initUI();
    }

    private void initUI() {
        activity = this;

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText("TRANSFER ANTAR BANK");
        llHomeMenuTransferDetail = (ImageView) findViewById(R.id.llHomeMenuTransferDetail);
        llHomeMenuTransferDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        llRequestTransferMenuTransferDetail = (ImageView) findViewById(R.id.llRequestTransferMenuTransferDetail);
        llRequestTransferMenuTransferDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(mContext, MenuRequestTransfer.class);
//                startActivity(i);
//                finish();
                new executeListBank(activity).execute();
            }
        });

        llKonfirmasiTransferDetailMenuTransferDetail = (ImageView) findViewById(R.id.llKonfirmasiTransferDetailMenuTransferDetail);
        llKonfirmasiTransferDetailMenuTransferDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(mContext, MenuTransferKonfirmasi.class);
//                startActivity(i);
//                finish();
            }
        });


    }

    class executeListBank extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeListBank(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN_REQUEST_TRANSFER, noHPAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_LIST_BANK);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
//                System.out.println(resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuTransferAntarBankDetail.this);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT).replaceAll("\\\\","");
                    System.out.println(rmResult);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Intent i = new Intent(activity, MenuRequestTransferAntarBank.class);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }
    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
