package com.lakupandai.acs.lakupandaidiy.alert;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;
import com.lakupandai.acs.lakupandaidiy.util.Constant;


/**
 * Created by ACS-Design on 8/16/2017.
 */
public class DialogAlertInquiry extends DialogFragment implements View.OnClickListener{
    TextView tvRM;
    Button btOK;

    public static DialogAlertInquiry createInstance(){

        return new DialogAlertInquiry();
    }

    public Activity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dlg = super.onCreateDialog(savedInstanceState);
        dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlg.setCanceledOnTouchOutside(false);
        return dlg;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.dialog_alert_inquiry, container);

        view.findViewById(R.id.btOK).setOnClickListener(this);
        tvRM = view.findViewById(R.id.tvRM);
        tvRM.setText(Constant.ALERT_RM);

        return view;
    }

    @Override
    public void onClick(View view) {
//
        switch (view.getId()){
            case R.id.btOK :
                Intent i = new Intent(activity, MainMenu.class);
                activity.startActivity(i);
                activity.finish();
                break;
            default:
                throw new IllegalArgumentException("Unrecognized id");
        }

    }
}
