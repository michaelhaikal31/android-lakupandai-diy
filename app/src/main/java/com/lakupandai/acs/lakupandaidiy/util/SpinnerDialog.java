package com.lakupandai.acs.lakupandaidiy.util;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;

/**
 * Created by Erdy on 12/05/2018.
 */
public class SpinnerDialog extends DialogFragment implements View.OnClickListener {

    private String title, textContent;
    private TextView contentTv;

    public static SpinnerDialog createInstance(String title, String textContent) {
        SpinnerDialog spinnerDialog = new SpinnerDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("textContent", textContent);
        spinnerDialog.setArguments(bundle);
        return spinnerDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view=inflater.inflate(R.layout.spinner_dialog,container);
        contentTv=(TextView) view.findViewById(R.id.content_tv);
        contentTv.setText(textContent);
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dlg=super.onCreateDialog(savedInstanceState);
        title=getArguments().getString("title");
        dlg.setTitle(title);
        dlg.setCanceledOnTouchOutside(false);

        dlg.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                if(keyCode==KeyEvent.KEYCODE_BACK){

                    return true;
                }
                else{
                    return false;
                }
            }
        });
        textContent=getArguments().getString("textContent");
        return dlg;
    }


    @Override
    public void onClick(View view) {

    }
}
