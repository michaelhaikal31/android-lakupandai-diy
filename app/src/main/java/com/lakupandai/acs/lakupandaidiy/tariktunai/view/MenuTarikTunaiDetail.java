package com.lakupandai.acs.lakupandaidiy.tariktunai.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.sms.SmsReceiver;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.NumberTextWatcherForThousand;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Erdy on 12/04/2018.
 */
public class MenuTarikTunaiDetail extends HomeActivity implements SmsReceiver.SmsListener {
    private Activity activity;
    private ImageView llHomeMenuTarikTunaiDetail;
    private EditText etNomorHPMenuTarikTunaiDetail, etNominalMenuTarikTunaiDetail, etPinAgenMenuTarikTunaiDetail;
    private Button btnKirimMenuTarikTunaiDetail;
    private String current, noHPAgen, noHPNasabah, nominal, pinAgen, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_tarik_tunai_detail);
        initUI();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregisterReceiver(receiver);
    }


    @Override
    public void smsReceived(String msgFrom, String msgBody) {
//        System.out.println("From: " + msgFrom);
//        System.out.println("Body: " + msgBody);
//
//        handler.removeCallbacks(runnable);
//        spinnerDialog.dismissAllowingStateLoss();
//        String hasilRCSms = msgBody.substring(4, 6);
//        String hasilRMSms = msgBody.substring(7, msgBody.length());
//
//
//        if (hasilRCSms.equals(Constant.SUCCESS_CODE)) {
//            Intent i = new Intent(mContext, MenuHasilResponse.class);
//            Bundle bundle = new Bundle();
//            bundle.putString(Constant.BUNDLE_TITLE, Constant.BUNDLE_TITLE_TARIK_TUNAI);
//            bundle.putString(Constant.BUNDLE_RC, hasilRCSms);
//            bundle.putString(Constant.BUNDLE_RM, hasilRMSms);
//            i.putExtras(bundle);
//            startActivity(i);
//            finish();
//        } else {
//            Function.showAlert(mContext, hasilRMSms);
//        }
    }

    private void initUI() {
        activity = this;

//        final IntentFilter filter = new IntentFilter();
//        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
//        filter.setPriority(1000);
//        receiver = new SmsReceiver(this);
//        registerReceiver(receiver, filter);

        etNomorHPMenuTarikTunaiDetail = (EditText) findViewById(R.id.etNomorHPMenuTarikTunaiDetail);
        etPinAgenMenuTarikTunaiDetail = (EditText) findViewById(R.id.etPinAgenMenuTarikTunaiDetail);

        etNominalMenuTarikTunaiDetail = (EditText) findViewById(R.id.etNominalMenuTarikTunaiDetail);
        etNominalMenuTarikTunaiDetail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (!s.toString().equals(current)) {
                        etNominalMenuTarikTunaiDetail.removeTextChangedListener(this);
                        String cleanString = s.toString().replaceAll("[.]", "");
                        String formatted = null;
                        if (!cleanString.isEmpty()) {
                            double parsed = Double.parseDouble(cleanString);

                            formatted = NumberTextWatcherForThousand.formatAmount(parsed);
                        }
                        current = formatted;
                        etNominalMenuTarikTunaiDetail.setText(formatted);
                        if (!cleanString.isEmpty()) {
                            etNominalMenuTarikTunaiDetail.setSelection(formatted.length());
                        }
                        etNominalMenuTarikTunaiDetail.addTextChangedListener(this);
                    }
                } catch (Exception e) {
                    Toast.makeText(activity, ConstantError.ALERT_NOMINAL_INPUT_FALSE, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnKirimMenuTarikTunaiDetail = (Button) findViewById(R.id.btnKirimMenuTarikTunaiDetail);
        btnKirimMenuTarikTunaiDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etNomorHPMenuTarikTunaiDetail.getText().toString().isEmpty() || etNominalMenuTarikTunaiDetail.getText().toString().isEmpty() || etPinAgenMenuTarikTunaiDetail.getText().toString().isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (etNomorHPMenuTarikTunaiDetail.getText().toString().length() < 10) {
                    Function.showAlert(activity, ConstantError.ALERT_NUMBER_LESS_THAN_10);
                } else if (etPinAgenMenuTarikTunaiDetail.getText().toString().length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else if (Integer.parseInt(Function.removePoint(etNominalMenuTarikTunaiDetail.getText().toString())) < 1) {
                    Function.showAlert(activity, ConstantError.ALERT_NOMINAL_LESS_THAN_1);
                } else {
                    noHPNasabah = etNomorHPMenuTarikTunaiDetail.getText().toString();
                    nominal = Function.removePoint(etNominalMenuTarikTunaiDetail.getText().toString());
                    pinAgen = etPinAgenMenuTarikTunaiDetail.getText().toString();
                    new executeTarikTunai(activity).execute();
                }
            }
        });

        llHomeMenuTarikTunaiDetail = (ImageView) findViewById(R.id.llHomeMenuTarikTunaiDetail);
        llHomeMenuTarikTunaiDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    class executeTarikTunai extends AsyncTask<String, JSONObject, String[]> {
        Context context;

        private executeTarikTunai(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String[] doInBackground(String... params) {
            JSONObject json = new JSONObject();
            final String[] temp = new String[2];
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.MSISDN_NASABAH_TARIK_TUNAI, noHPNasabah);
                json.put(ConstantTransaction.NOMINAL_TARIK_TUNAI, nominal);
                json.put(ConstantTransaction.PIN_TARIK_TUNAI, pinAgen);
                json.put(ConstantTransaction.MSISDN_AGEN_TARIK_TUNAI, noHPAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_TARIK_TUNAI);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);

                System.out.println("Response Send " + resultEnc);
                System.out.println("Response Send Decrypt " + resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuTarikTunaiDetail.this);

                temp[0] = resultfromJson;
                temp[1]=resultEnc;

                return temp;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return temp;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            JSONObject jsonObject;
            System.out.println("asd [menu tarik tunai onpost resultDex] "+result[0]);
            System.out.println("asd [menu tarik tunai onpost body param] "+result[1]);
            if (result[0].contains(ConstantTransaction.CONNECTION_LOST)) {
//                shared = getSharedPreferences(Constant.MyPref, MODE_PRIVATE);
//                statusSMSShared = (shared.getString(Constant.SETTING_SMS, ""));
//                if (statusSMSShared.equals("0")) {
//                    Function.showAlert(mContext, Constant.CONNECTION_LOST);
//                } else if (statusSMSShared.equals("1")) {
//                    spinnerDialog = SpinnerDialog.createInstance(Constant.LOADING_SMS, Constant.PESAN_SMS);
//                    spinnerDialog.show(getFragmentManager(), Constant.FRAGMENT_SMS);
//                    handler = new Handler();
//                    runnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            if (spinnerDialog.isVisible()) {
//                                spinnerDialog.dismissAllowingStateLoss();
//                                Toast.makeText(getApplicationContext(), Constant.FAIL_RECEIVE_SMS, Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    };
//                    handler.postDelayed(runnable, Constant.TIMEOUT_SMS);
//                    SendSms.splitSMS(resultEnc, mContext);
//                }
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result[0].contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result[0]);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        DeviceSession.setTempRequestBody(MenuTarikTunaiDetail.this,result[1]);
                        setResponseValue(rmResult);
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    public void setResponseValue(String rmResults) {
        try {
            JSONObject jsonRM = new JSONObject(rmResults);

            JSONArray arrayData = jsonRM.getJSONArray("data");
            System.out.println("arrayData " + arrayData);

            JSONArray jsonArray = new JSONArray(arrayData.toString());
            HashMap<String, String> list = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = jsonObject1.optString("header");
                String value2 = jsonObject1.optString("value");
                list.put(value1, value2);
            }

            String desc = list.get("Keterangan");
            Function.showListenerFinishDialogMessage(activity, getLayoutInflater(), desc, new Function.InputSenderDialogListener() {
                @Override
                public void onOK(String value) {
                    finish();
                }

                @Override
                public void onCancel(String value) {

                }
            });
        } catch (Exception e) {
            Function.showAlertHome(activity, e.getMessage());
        }


    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();

    }

    @Override
    public void onBackPressed() {
        finish();
    }



}
