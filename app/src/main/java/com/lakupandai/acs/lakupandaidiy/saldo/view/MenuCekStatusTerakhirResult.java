package com.lakupandai.acs.lakupandaidiy.saldo.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Erdy on 12/20/2018.
 */
public class MenuCekStatusTerakhirResult extends HomeActivity {
    Activity activity;
    Button llHomeMenuCekStatusTerakhirResult;
    TextView txtValueLayanan, txtValueRef, txtValueNama, txtValueMsisdn, txtValueStatus, txtValueJumlah;
    TextView txtLayanan, txtRef, txtTextNama, txtTextMsisdn, txtTextStatus, txtTextJumlah;
    String rmResult, titleMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_cek_status_terakhir_result);
        initUI();
    }

    private void initUI() {
        activity = this;
        txtValueLayanan = findViewById(R.id.txtValueLayanan);
        txtValueRef = findViewById(R.id.txtValueRef);
        txtValueNama = findViewById(R.id.txtValueNama);
        txtValueMsisdn = findViewById(R.id.txtValueMsisdn);
        txtValueStatus = findViewById(R.id.txtValueStatus);
        txtValueJumlah = findViewById(R.id.txtValueJumlah);

        txtLayanan = findViewById(R.id.txtLayanan);
        txtRef = findViewById(R.id.txtRef);
        txtTextNama = findViewById(R.id.txtTextNama);
        txtTextMsisdn = findViewById(R.id.txtTextMsisdn);
        txtTextStatus = findViewById(R.id.txtTextStatus);
        txtTextJumlah = findViewById(R.id.txtTextJumlah);

        llHomeMenuCekStatusTerakhirResult = findViewById(R.id.llHomeMenuCekStatusTerakhirResult);

        llHomeMenuCekStatusTerakhirResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Bundle bundle = getIntent().getExtras();
        rmResult = bundle.getString(ConstantTransaction.BUNDLE_RM);
        titleMenu = bundle.getString(ConstantTransaction.BUNDLE_TITLE);

        setResponseValue(rmResult);
    }

    private void setResponseValue(String rmResult) {
        try {
            JSONObject jsonRM = new JSONObject(rmResult);
            JSONArray arrayData = jsonRM.getJSONArray("data");
            JSONArray jsonArray = new JSONArray(arrayData.toString());
            HashMap<String, String> list = new HashMap<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String value1 = jsonObject1.optString("header");
                String value2 = jsonObject1.optString("value");
                list.put(value1, value2);
            }

            String layanan = list.get("Layanan");
            String jumlah = list.get("Amount");
            String namaNasabah = list.get("Nama Nasabah");
            String msisdn = list.get("Msisdn");
            String status = list.get("Status");
            String ref = list.get("Ref");

            txtValueLayanan.setText(": " + layanan);
            txtValueRef.setText(": " + ref);
            txtValueNama.setText(": " + namaNasabah);
            txtValueMsisdn.setText(": " + msisdn);
            txtValueStatus.setText(": " + status);
            txtValueJumlah.setText(": Rp. " + jumlah);


        } catch (Exception e) {
            Function.showAlert(activity, e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(activity, MenuCekSaldo.class);
        startActivity(i);
        finish();
    }
}
