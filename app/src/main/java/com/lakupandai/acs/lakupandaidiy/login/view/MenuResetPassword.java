package com.lakupandai.acs.lakupandaidiy.login.view;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONObject;

/**
 * Created by Erdy on 11/27/2018.
 */
public class MenuResetPassword extends Activity {

    private EditText etPasswordBaru, etPasswordBaruUlang;
    private Button btnKirimResetPassword;
    private ImageView llBackMenuAktivasiRekBaru, llMenuAktivasiRekBaruPopup, llHomeResetPassword;
    private Activity activity;
    private static String imei, noHPAgen, passwordLama, passwordBaru, passwordBaruUlang, resultEnc, resultDec, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    private TelephonyManager mTelephonyManager;
    private String deviceId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Constant.status = 99;
        setContentView(R.layout.menu_reset_password);
        initUI();
    }

    private void initUI() {
        activity = this;
        etPasswordBaru = (EditText) findViewById(R.id.etPasswordBaru);
        etPasswordBaruUlang = (EditText) findViewById(R.id.etPasswordBaruUlang);
        llHomeResetPassword = (ImageView) findViewById(R.id.llHomeResetPassword);
        llHomeResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, MenuLogin.class);
                startActivity(i);
                finish();
            }
        });


        btnKirimResetPassword = (Button) findViewById(R.id.btnKirimResetPassword);
        btnKirimResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordBaru = etPasswordBaru.getText().toString();
                passwordBaruUlang = etPasswordBaruUlang.getText().toString();
                if (passwordBaru.length() < 6) {
                    Function.showAlert(activity, ConstantError.ALERT_PIN_LESS_THAN_6);
                } else if (!passwordBaru.equals(passwordBaruUlang)) {
                    Function.showAlert(activity, "PIN yang Anda masukan Salah");
                } else {
                    if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        Function.showAlert(activity, ConstantError.ALERT_REQPERMISSION_READPHONESTATE);
                    } else {
                        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        imei = mngr.getDeviceId();
                        new executeResetPassword(activity).execute();
                    }
                }
            }
        });
    }

    class executeResetPassword extends AsyncTask<String, JSONObject, String> {
        Context context;

        private executeResetPassword(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                passwordLama = Function.getValueSharedPref(Constant.MY_PREF_PIN, activity);
                json.put(ConstantTransaction.MSISDN_AGEN, noHPAgen);
                json.put(ConstantTransaction.PIN_LOGIN, passwordBaru);
                json.put(ConstantTransaction.IMEI_LOGIN, imei);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.RESET_PASS);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Send "+json.toString());
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuResetPassword.this);
                System.out.println("Receive "+resultfromJson);
                resultfromJsonDecrypt=tripleDES.decrypt(resultfromJson);
                System.out.println("Receive decrypt "+resultfromJsonDecrypt);
                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            JSONObject jsonObject;
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
//                }
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    resultfromJsonDecrypt = tripleDES.decrypt(result);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        Intent i = new Intent(activity, MenuLogin.class);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(activity, MenuLogin.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

}
