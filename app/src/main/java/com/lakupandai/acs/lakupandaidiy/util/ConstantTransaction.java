package com.lakupandai.acs.lakupandaidiy.util;

public class ConstantTransaction {
    //ERROR RETURN
    public static String CONNECTION_LOST = "Koneksi Timeout";
    public static String CONNECTION_ERROR = "Koneksi Error";
    public static String SERVER_ERROR = "Server Error";
    public static String SERVER_RETURN_EMPTY_DATA = "Halaman kosong";


    //JSON RESULT
    public static String RC_RESULT = "RC";
    public static String RM_RESULT = "RM";
    public static String DATA = "data";
    public static String SUCCESS_CODE = "00";
    public static String RESET_CODE = "02";
    public static String LAST_LOGIN = "last_login";

    //BUNDLE
    public static String BUNDLE_NOKTP = "NoKTP";
    public static String BUNDLE_NAMA = "Nama";
    public static String BUNDLE_NOHP = "NoHP";
    public static String BUNDLE_PIN = "PIN";
    public static String BUNDLE_TGL = "TGL";

    public static String BUNDLE_RC = "RC";
    public static String BUNDLE_RM = "RM";
    public static String BUNDLE_TOKEN = "token";
    public static String BUNDLE_ARRAY_LIST = "arraylist";
    public static String BUNDLE_TITLE = "TITLE";
    public static String BUNDLE_TITLE_REK_BARU = "Aktivasi Rekening Baru";
    public static String BUNDLE_TITLE_TARIK_TUNAI = "Tarik Tunai";
    public static String BUNDLE_TITLE_KONF_TARIK_TUNAI = "Konfirmasi Tarik Tunai";
    public static String BUNDLE_TITLE_CEK_STATUS_TERAKHIR = "Cek Status Terakhir";
    public static String BUNDLE_TITLE_CEK_SALDO_AGEN = "Cek Saldo Agen";
    public static String BUNDLE_TITLE_CEK_SALDO_NASABAH = "Cek Saldo Nasabah";
    public static String BUNDLE_TITLE_SETOR_TUNAI = "Setor Tunai";
    public static String BUNDLE_TITLE_PEMBELIAN = "Pembelian";
    public static String BUNDLE_TITLE_PEMBAYARAN = "Pembayaran";
    public static String BUNDLE_TITLE_PEMBELIAN_PULSA = "Pembelian Pulsa";
    public static String BUNDLE_TITLE_TRANSFER = "Transfer";

    public static String ACTION = "action";
    public static String TOKEN = "token";
    //AGENT
    //JSON CEK SALDO AGEN
    public static String MSISDN_AGEN_CEK_SALDO_AGEN = "msisdn_agen";
    public static String PIN_CEK_SALDO_AGEN = "pin";
    public static String ACTION_CEK_SALDO_AGEN = "saldo_agen";
    public static String MESSAGE_LOG_CEK_SALDO_AGEN = "Mencoba Cek Saldo Agen";
    public static String LOG_CEK_SALDO_AGEN = "Request Cek Saldo Agen";

    //JSON LOGIN
    public static String SIM_SERIAL_LOGIN = "simserial";
    public static String PIN_LOGIN = "pin";
    public static String MSISDN_AGEN_LOGIN = "msisdn_agen";
    public static String IMEI_LOGIN = "imei";
    public static String ACTION_LOGIN = "login";
    public static String ACTION_INIT = "initialize";



    public static String MESSAGE_LOG_LOGIN = "Mencoba Login";
    public static String LOG_LOGIN = "Request Login";

    public static final int TimeOutConnection = 120000;

    //KONEKSI
    //koneksi http

    // public static final String URL = "aHR0cDovLzEwMy4zLjQ3LjQyOjUxMTUvbGFrdXBhbmRhaS8=";

    //public static final String URL ="aHR0cDovLzE3Mi4xNjguMTAwLjEwOjUxMTUvbGFrdXBhbmRhaS8=";




    //public static final String URL = "http://dev.srv.co.id/lakupandaidiy/";
    //URI ASLI
      //public static final String URL = "http://103.3.47.42:5112/lakupandaidiy/";
      public static final String URL = "https://bb.bpddiy.co.id/lakupandaidiy/";
    //koneksi https

    // public static final String URL = "aHR0cHM6Ly9sYWt1cGFuZGFpLWJqdG0uYWtzZXNjaXB0YXNvbHVzaS5jb20vbGFrdXBhbmRhaS8=";

    //public static final String URL = "https://103.23.175.124:8442/lakupandaidiy/";


    public static String MESSAGE_OK= "Ok";
    public static String MESSAGE= "Pesan";
    public static String MESSAGE_ROOT = "Aplikasi tidak bisa digunakan bila Root Device Aktif";

    public static String MSISDN_AGEN="msisdn_agen";
    public static String RESET_PASS = "reset_pwd";

    //ALERT DIALOG
    public static String MESSAGE_LOGOUT = "Anda Yakin Ingin Keluar?";
    public static String MESSAGE_SMS = "Data Akan Dikirim Lewat SMS Karena Masalah Jaringan, Ijinkan?";
    public static String MESSAGE_YES = "Ya";
    public static String MESSAGE_NO = "Tidak";
    public static String MESSAGE_FAILED = "Gagal Mengirim Data Harap Dicoba Kembali";


    //JSON MUTASI
    public static String START_DATE = "start_date";
    public static String END_DATE = "end_date";
    public static String ACTION_MUTASI = "history_trx";
    public static String PAGE_JSON = "page";
    public static String EMPTY_TRANSACTION_RESULT = "Transaksi tidak ditemukan !";

    //JSON PEMBAYRAN/PEMBELIAN
    public static String KODE_PRODUK = "product_code";
    public static String ID_PELANGGAN = "id_pelanggan";
    public static String MSISDN_NASABAH = "hp_nasabah";
    public static String MSISDN_NASABAH_INQ = "msisdn_nasabah";
    public static String ACTION_PEMBAYARAN = "inquiry_payment";
    public static String ACTION_PEMBELIAN = "inquiry_prepaid";
    public static String ACTION_KONFIRMASI_PEMBELIAN = "posting_prepaid";
    public static String ACTION_KONFIRMASI_PEMBAYARAN = "posting_payment";
    public static String ACTION_GET_MENU = "get_menu";
    public static String PIN_PEMBAYARAN = "pin";
    public static String TIPE = "tipe";
    public static String DENOM = "denom";
    public static String LAYANAN;

    //ALERT TIMEOUT
    public static String MESSAGE_TIMEOUT = "Sesi Anda Telah Berakhir";
    public static final int DISPLAY_DATA = 1;
    public static final int TimeOutApps = 6000000;
    //public static final int TimeOutApps = 6000;

    //JSON REK BARU
    public static String NO_KTP_REK_BARU = "nik";
    public static String NAMA_REK_BARU = "nama";
    public static String TANGGAL_REK_BARU = "tanggal";
    public static String MSISDN_AGEN_REK_BARU = "msisdn_agen";
    public static String MSISDN_NASABAH_REK_BARU = "msisdn_nasabah";
    public static String PIN_REK_BARU = "pin";
    public static String ACTION_REK_BARU = "pendaftaran_rekening";
    public static String ACTION_GENERATE_TOKEN = "generate_token_activate_account";
    public static String CHECK_NIK = "check_nik";
    public static String MESSAGE_LOG_REK_BARU = "Mencoba Registrasi Rek Baru";
    public static String LOG_REK_BARU = "Request Aktivasi Rek Baru";
    public static String TOKEN_REK_BARU = "token";


    //JSON CEK SALDO NASABAH
    public static String MSISDN_AGEN_CEK_SALDO_NASABAH = "msisdn_agen";
    public static String MSISDN_NASABAH_CEK_SALDO_NASABAH = "msisdn_nasabah";
    public static String PIN_CEK_SALDO_NASABAH = "pin";
    public static String ACTION_CEK_SALDO_NASABAH = "saldo_nasabah";
    public static String MESSAGE_LOG_CEK_SALDO_NASABAH = "Mencoba Cek Saldo Nasabah";
    public static String LOG_CEK_SALDO_NASABAH = "Request Cek Saldo Agen";


    //JSON CEK STATUS TERAKHIR
    public static String MSISDN_AGEN_CEK_STATUS_TERAKHIR = "msisdn_agen";
    public static String MSISDN_NASABAH_CEK_STATUS_TERAKHIR = "msisdn_nasabah";
    public static String PIN_CEK_STATUS_TERAKHIR = "pin";
    public static String ACTION_CEK_STATUS_TERAKHIR = "last_trx";
    public static String MESSAGE_LOG_CEK_STATUS_TERAKHIR = "Mencoba Konfirmasi Tarik Tunai Token dan Pin";
    public static String LOG_CEKSTATUSTERAKHIR = "Request Konfirmasi Tarik Tunai Token dan Pin";

    //JSON SETOR TUNAI
    public static String MSISDN_AGEN_SETOR_TUNAI = "msisdn_agen";
    public static String MSISDN_NASABAH_SETOR_TUNAI = "msisdn_nasabah";
    public static String NOMINAL_SETOR_TUNAI = "nominal";
    public static String NO_REK_SETOR_TUNAI = "no_rekening";
    public static String TOKEN_SETOR_TUNAI = "token";
    public static String ACTION_SETOR_TUNAI = "request_setor_tunai";
    public static String MESSAGE_LOG_SETOR_TUNAI = "Mencoba Setor Tunai";
    public static String LOG_SETOR_TUNAI = "Request Setor Tunai";

    //JSON KONFIRMASI SETOR TUNAI
    public static String MSISDN_AGEN_KONF_SETOR_TUNAI = "msisdn_agen";
    public static String TOKEN_KONF_SETOR_TUNAI = "token";
    public static String PIN_KONF_SETOR_TUNAI = "pin";
    public static String ACTION_KONFIRMASI_SETOR_TUNAI = "posting_setor_tunai";
    public static String MESSAGE_LOG_KONF_SETOR_TUNAI = "Mencoba Konfirmasi Setor Tunai";
    public static String LOG_KONF_SETOR_TUNAI = "Request Konfirmasi Setor Tunai";

    //JSON TARIK TUNAI
    public static String MSISDN_NASABAH_TARIK_TUNAI = "msisdn_nasabah";
    public static String NOMINAL_TARIK_TUNAI = "nominal";
    public static String PIN_TARIK_TUNAI = "pin";
    public static String MSISDN_AGEN_TARIK_TUNAI = "msisdn_agen";
    public static String ACTION_TARIK_TUNAI = "request_tarik_tunai";
    public static String MESSAGE_LOG_TARIK_TUNAI = "Mencoba Tarik Tunai";
    public static String LOG_TARIK_TUNAI = "Request Tarik Tunai";


    //JSON KONFIRMASI TARIK TUNAI
    public static String MSISDN_AGEN_KONF_TARIK_TUNAI = "msisdn_agen";
    public static String TOKEN_KONF_TARIK_TUNAI = "token";
    public static String ACTION_KONFIRMASI_TARIK_TUNAI = "send_token_tarik_tunai";
    public static String MESSAGE_LOG_KONFIRMASI_TARIK_TUNAI = "Mencoba Konfirmasi Tarik Tunai";
    public static String LOG_KONFIRMASI_TARIK_TUNAI = "Request Konfirmasi Tarik Tunai";


    //JSON KONFIRMASI DETAIL TARIK TUNAI
    public static String MSISDN_AGEN_KONF_TARIK_TUNAI_DETAIL = "msisdn_agen";
    public static String TOKEN_KONF_TARIK_TUNAI_DETAIL = "token";
    public static String PIN_KONF_TARIK_TUNAI_DETAIL = "pin";
    public static String ACTION_KONFIRMASI_TARIK_TUNAI_DETAIL = "posting_tarik_tunai";
    public static String MESSAGE_LOG_KONFIRMASI_TARIK_TUNAI_DETAIL = "Mencoba Cek Status Terakhir";
    public static String LOG_KONFIRMASI_TARIK_TUNAI_DETAIL = "Request Cek Status Terakhir";


    //JSON NOTIFIKASI
    public static String ACTION_NOTIFIKASI = "notifikasi";
    public static String ACTION_RESEND_TOKEN = "resend_token";

    //JSON PEMBAYRAN/PEMBELIAN
    public static String OTP_PEMBAYARAN = "token";

    //JSON REQUEST TRANSFER
    public static String MSISDN_AGEN_REQUEST_TRANSFER = "msisdn_agen";
    public static String MSISDN_NASABAH_REQUEST_TRANSFER = "msisdn_nasabah";
    public static String MSISDN_TUJUAN_REQUEST_TRANSFER = "msisdn_tujuan";
    public static String NOMINAL_REQUEST_TRANSFER = "nominal";
    public static String NO_REK_REQUEST_TRANSFER = "no_rekening";
    public static String ACTION_REQUEST_TRANSFER = "request_transfer_on_us";
    public static String ACTION_LIST_BANK = "list_bank";
    public static String ACTION_REQUEST_TRANSFER_LAKUPANDAI = "request_transfer_on_us_lakupandai";
    public static String MESSAGE_LOG_REQUEST_TRANSFER = "Mencoba Request Transfer";
    public static String LOG_REQUEST_TRANSFER = "Request Transfer";
    public static String TOKEN_REQUEST_TRANSFER = "token";

    //JSON REQUEST TRANSFER KONFIRMASI
    public static String MSISDN_AGEN_REQUEST_TRANSFER_KONFIRMASI = "msisdn_agen";
    public static String PIN_REQUEST_TRANSFER_KONFIRMASI = "pin";
    public static String TOKEN_REQUEST_TRANSFER_KONFIRMASI = "token";
    public static String ACTION_REQUEST_TRANSFER_KONFIRMASI = "request_transfer_on_us_pin";
    public static String ACTION_REQUEST_TRANSFER_KONFIRMASI_LAKUPANDAI = "request_transfer_on_us_pin_lakupandai";
    public static String MESSAGE_LOG_REQUEST_TRANSFER_KONFIRMASI = "Mencoba Request Transfer Konfirmasi";
    public static String LOG_REQUEST_TRANSFER_KONFIRMASI = "Request Transfer Konfirmasi";

    //JSON TRANSFER KONFIRMASI
    public static String MSISDN_AGEN_TRANSFER_KONFIRMASI = "msisdn_agen";
    public static String TOKEN_TRANSFER_KONFIRMASI = "token";
    public static String ACTION_TRANSFER_KONFIRMASI = "send_token_transfer_on_us";
    public static String ACTION_TRANSFER_KONFIRMASI_LAKUPANDAI = "send_token_transfer_on_us_lakupandai";
    public static String MESSAGE_LOG_TRANSFER_KONFIRMASI = "Mencoba Konfirmasi Transfer";
    public static String LOG_TRANSFER_KONFIRMASI = "Request Konfirmasi Transfer";

    //JSON TRANSFER KONFIRMASI DETAIL
    public static String MSISDN_AGEN_TRANSFER_KONFIRMASI_DETAIL = "msisdn_agen";
    public static String TOKEN_TRANSFER_KONFIRMASI_DETAIL = "token";
    public static String PIN_TRANSFER_KONFIRMASI_DETAIL = "pin";
    public static String ACTION_TRANSFER_KONFIRMASI_DETAIL = "posting_transfer_on_us";
    public static String ACTION_TRANSFER_KONFIRMASI_DETAIL_LAKUPANDAI = "posting_transfer_on_us_lakupandai";
    public static String MESSAGE_LOG_TRANSFER_KONFIRMASI_DETAIL = "Mencoba Konfirmasi Transfer Detail";
    public static String LOG_TRANSFER_KONFIRMASI_DETAIL = "Request Konfirmasi Transfer Detail";

    //JSON NOTIFIKASI
    public static String NO_RESI = "resi";
    public static String ACTION_GET_LIST_NOTIF = "notifikasi_mobile";
    public static String ACTION_GET_DETAIL_TRX = "detail_notifikasi_mobile";
    public static String ACTION_GET_LIST_NOTIF_AGEN="agen";

}
