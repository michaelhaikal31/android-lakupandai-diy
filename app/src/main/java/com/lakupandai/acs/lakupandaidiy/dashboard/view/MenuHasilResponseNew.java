package com.lakupandai.acs.lakupandaidiy.dashboard.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.adapter.ResponAdapter;
import com.lakupandai.acs.lakupandaidiy.setortunai.model.Respon;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Erdy on 12/06/2018.
 */
public class MenuHasilResponseNew extends HomeActivity {
    private Button llHomeMenuHasilResponse, btShare;
    private ListView listRespon;
    private Context mContext;
    private TextView txtJudulMenuHasilResponse;
    private String rmResult, titleMenu;

    ArrayList<Respon> myListItems = new ArrayList<>();
    ResponAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_hasil_response_new);
        initUI();
    }

    private void initUI() {
        mContext = this;
        txtJudulMenuHasilResponse = (TextView) findViewById(R.id.txtJudulMenuHasilResponse);
        listRespon = findViewById(R.id.listRespon);
        Bundle bundle = getIntent().getExtras();
        rmResult = bundle.getString(ConstantTransaction.BUNDLE_RM);
        titleMenu = bundle.getString(ConstantTransaction.BUNDLE_TITLE);
        txtJudulMenuHasilResponse.setText(titleMenu);
        JSONObject jsonC;
        myListItems = new ArrayList<>();
        for (int a = 0; a < Constant.dataResult.length(); a++) {
            try {
                jsonC = Constant.dataResult.getJSONObject(a);
                myListItems.add(new Respon(jsonC.getString("header"), jsonC.getString("value")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter = new ResponAdapter(getApplicationContext(), myListItems);
        listRespon.setAdapter(adapter);

        llHomeMenuHasilResponse = (Button) findViewById(R.id.llHomeMenuHasilResponse);
        llHomeMenuHasilResponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btShare = (Button) findViewById(R.id.btShare);
        btShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, titleMenu);
//                sharingIntent.putExtra(Intent.EXTRA_TEXT, rmResult);
//                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                generateResponse();
            }
        });
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        final Intent intent = new Intent(this,MainMenu.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
        startActivity(intent);
    }

    private void generateResponse(){
        String result="";
        try{
            JSONArray jsonArray;
            if(getIntent().getExtras().getBoolean("notif")){
                jsonArray = new JSONObject(rmResult).getJSONArray("data");
            }else{
                jsonArray = Constant.dataResult;
            }
            System.out.println("asd jsonArray "+jsonArray);
            final int size = jsonArray.length();
            JSONObject jsonObject;
            for(int i=0;i<size;i++){
                jsonObject =jsonArray.getJSONObject(i);
                result = result + jsonObject.getString("header")+" : "+jsonObject.getString("value")+"\n";
            }
        }catch (Exception e){
            System.out.println("asd exception generateResponse [Menuhasilresponse] " +e.getMessage());
        }
        final Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, titleMenu);
        sharingIntent.putExtra(Intent.EXTRA_TEXT,result);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }
}
