package com.lakupandai.acs.lakupandaidiy.tariktunai.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MainMenu;

/**
 * Created by Erdy on 12/04/2018.
 */
public class MenuTarikTunai extends HomeActivity {
    private ImageView llHomeMenuTarikTunai, llKonfirmasiTarikTunaiMenuTarikTunai, llRequestTarikTunaiMenuTarikTunai;
    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_tarik_tunai);
        initUI();
    }

    private void initUI() {
        mContext = this;
        llHomeMenuTarikTunai = (ImageView) findViewById(R.id.llHomeMenuTarikTunai);
        llHomeMenuTarikTunai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        llRequestTarikTunaiMenuTarikTunai = (ImageView) findViewById(R.id.llRequestTarikTunaiMenuTarikTunai);
        llRequestTarikTunaiMenuTarikTunai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuTarikTunaiDetail.class);
                startActivity(i);
            }
        });
        llKonfirmasiTarikTunaiMenuTarikTunai = (ImageView) findViewById(R.id.llKonfirmasiTarikTunaiMenuTarikTunai);
        llKonfirmasiTarikTunaiMenuTarikTunai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, MenuKonfirmasiTarikTunai.class);
                startActivity(i);
            }
        });

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
