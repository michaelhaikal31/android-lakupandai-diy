package com.lakupandai.acs.lakupandaidiy.aktivasi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;
import com.lakupandai.acs.lakupandaidiy.sessions.DeviceSession;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantError;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONObject;

/**
 * Created by Erdy on 12/03/2018.
 */
public class MenuAktivasiRekBaruDetail extends HomeActivity {

    private static EditText etTanggalLahirMenuAktivasiRekBaru;
    private EditText etNomorKTPMenuAktivasiRekBaru, etNamaLengkapMenuAktivasiRekBaru, etNomorHPMenuAktivasiRekBaru;
    private Button btnKirimMenuAktivasiRekBaru;
    private ImageView llHomeMenuAktivasiRekBaru;
    private Activity activity;
    private String noKTP, nama, nomorHP, tglLahir, noHPAgen, resultDec, resultEnc, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult;
    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_aktivasi_rek_baru_detail);
        initUI();
    }

    private void initUI() {
        activity = this;
        //mHandler.sendEmptyMessageDelayed(Constant.DISPLAY_DATA, Constant.TimeOutApps);
        etNomorKTPMenuAktivasiRekBaru = (EditText) findViewById(R.id.etNomorKTPMenuAktivasiRekBaru);
        etNomorKTPMenuAktivasiRekBaru.setText(MenuAktivasiRekBaru.noKTP);
        etNamaLengkapMenuAktivasiRekBaru = (EditText) findViewById(R.id.etNamaLengkapMenuAktivasiRekBaru);
        etNamaLengkapMenuAktivasiRekBaru.setText(MenuAktivasiRekBaru.nama);
        etNomorHPMenuAktivasiRekBaru = (EditText) findViewById(R.id.etNomorHPMenuAktivasiRekBaru);

        etTanggalLahirMenuAktivasiRekBaru = (EditText) findViewById(R.id.etTanggalLahirMenuAktivasiRekBaru);
        etTanggalLahirMenuAktivasiRekBaru.setText(MenuAktivasiRekBaru.tanggalLahir);
        llHomeMenuAktivasiRekBaru = (ImageView) findViewById(R.id.llHomeMenuAktivasiRekBaru);
        llHomeMenuAktivasiRekBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnKirimMenuAktivasiRekBaru = (Button) findViewById(R.id.btnKirimMenuAktivasiRekBaru);
        btnKirimMenuAktivasiRekBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noKTP = etNomorKTPMenuAktivasiRekBaru.getText().toString();
                nama = etNamaLengkapMenuAktivasiRekBaru.getText().toString();
                nomorHP = etNomorHPMenuAktivasiRekBaru.getText().toString();
                tglLahir = etTanggalLahirMenuAktivasiRekBaru.getText().toString();
                if (nomorHP.isEmpty()) {
                    Function.showAlert(activity, ConstantError.ALERT_FIELD_EMPTY);
                } else if (nomorHP.length() < 10 && nomorHP.length() > 0) {
                    Function.showAlert(activity, ConstantError.ALERT_NUMBER_LESS_THAN_10);
                } else {
                    new executeAktivasiRekBaru(activity).execute();
                }

            }
        });
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    class executeAktivasiRekBaru extends AsyncTask<String, JSONObject, String[]> {
        Context context;

        executeAktivasiRekBaru(Context mContext) {
            this.context = mContext;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage(Constant.LOADING);
            progressDialog.setTitle(Constant.MESSAGE_PROGRESS);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        @Override
        protected String[] doInBackground(String... params) {
            JSONObject json = new JSONObject();
            final String[] temp =  new String[2];
            try {
                noHPAgen = Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, activity);
                json.put(ConstantTransaction.NO_KTP_REK_BARU, noKTP);
                json.put(ConstantTransaction.NAMA_REK_BARU, nama);
                json.put(ConstantTransaction.TANGGAL_REK_BARU, tglLahir);
                json.put(ConstantTransaction.MSISDN_NASABAH_REK_BARU, nomorHP);
                json.put(ConstantTransaction.MSISDN_AGEN_REK_BARU, noHPAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_GENERATE_TOKEN);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("Response Send " + resultEnc);
                System.out.println("Response Send Decrypt " + resultDec);
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuAktivasiRekBaruDetail.this);
                temp[0] = resultfromJson;
                temp[1] = resultEnc;
                return temp;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return temp;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            JSONObject jsonObject;
            if (result[0].contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_LOST);
            } else if (result[0].contains(ConstantTransaction.CONNECTION_ERROR)) {
                Function.showAlert(activity, ConstantTransaction.CONNECTION_ERROR);
            } else {
                try {
                    System.out.println("Response Receive " + result);
                    resultfromJsonDecrypt = tripleDES.decrypt(result[0]);
                    System.out.println("Response Receive Encrypt " + resultfromJsonDecrypt);
                    jsonObject = new JSONObject(resultfromJsonDecrypt);
                    rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                    rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);
                    if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                        DeviceSession.setTempRequestBody(getBaseContext(),result[1]);

                        Intent i = new Intent(activity, MenuAktivasiRekBaruResponse.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(ConstantTransaction.BUNDLE_NOKTP, noKTP);
                        bundle.putString(ConstantTransaction.BUNDLE_NAMA, nama);
                        bundle.putString(ConstantTransaction.BUNDLE_NOHP, nomorHP);
                        bundle.putString(ConstantTransaction.BUNDLE_TGL, tglLahir);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    } else {
                        Function.showAlert(activity, rmResult);
                    }
                } catch (Exception e) {
//                    e.printStackTrace();
                    Function.showAlert(activity, e.getMessage());
                }
            }
        }
    }

}
