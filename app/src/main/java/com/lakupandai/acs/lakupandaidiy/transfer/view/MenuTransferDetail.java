package com.lakupandai.acs.lakupandaidiy.transfer.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.HomeActivity;

/**
 * Created by Erdy on 12/07/2018.
 */
public class MenuTransferDetail extends HomeActivity {
    private Context mContexts;
    private TextView tvTitle;
    private ImageView llHomeMenuTransferDetail,llRequestTransferMenuTransferDetail,llKonfirmasiTransferDetailMenuTransferDetail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_transfer_detail);
        initUI();
    }

    private void initUI() {
        mContexts = this;
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText("TRANSFER ANTAR NASABAH BPDDIY");
        llHomeMenuTransferDetail = (ImageView) findViewById(R.id.llHomeMenuTransferDetail);
        llHomeMenuTransferDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        llRequestTransferMenuTransferDetail = (ImageView) findViewById(R.id.llRequestTransferMenuTransferDetail);
        llRequestTransferMenuTransferDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContexts, MenuRequestTransfer.class);
                startActivity(i);

            }
        });

        llKonfirmasiTransferDetailMenuTransferDetail = (ImageView) findViewById(R.id.llKonfirmasiTransferDetailMenuTransferDetail);
        llKonfirmasiTransferDetailMenuTransferDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContexts, MenuTransferKonfirmasi.class);
                startActivity(i);

            }
        });


    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
