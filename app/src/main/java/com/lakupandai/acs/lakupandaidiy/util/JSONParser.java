package com.lakupandai.acs.lakupandaidiy.util;

import android.content.Context;
import android.content.res.AssetManager;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import static com.lakupandai.acs.lakupandaidiy.util.Constant.ssl_crt;

public class JSONParser {

    public static String strResponse = "";

    public String HttpRequestPost2(String url, String json, int timeout, Context act) {
        String strResponse = "";
        try {
            DefaultHttpClient client = new DefaultHttpClient();
            HttpEntity entity = new StringEntity(json, "UTF-8");
            HttpPost post = new HttpPost(url);

            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
            HttpConnectionParams.setSoTimeout(httpParams, timeout);

            //set retry
            DefaultHttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(0, false); // 0 = retry, false = disable retry
            client.setHttpRequestRetryHandler(retryHandler);

            post.setParams(httpParams);
            post.setEntity(entity);
            post.setHeader("Authorization", Function.encodeBase64("android|" + Function.getdatetime()));
            post.setHeader("Content-type", "text/html");
            //System.out.println("SEND POST MESSAGE: " + json);

            BasicResponseHandler responseHandler = new BasicResponseHandler();
            strResponse = client.execute(post, responseHandler);
            //System.out.println("LENGTH DATA: " + strResponse);
            //System.out.println("LENGTH DATA: " + strResponse.length());
            int length = strResponse.length();

            for (int i = 0; i < length; i += 1024) {
                //System.out.println("I = " + i);
                //System.out.println("LENG: " + length);
                if (i + 1024 < length) {
                    //System.out.println("RESPONSE: " + strResponse.substring(i, i + 1024));
                } else {
                    //System.out.println("RESPONSE: " + strResponse.substring(i, length));
                }
            }
        } catch (UnsupportedEncodingException e) {
            System.out.println("asd e1 "+e.getMessage());
            strResponse = e.getMessage();
            e.printStackTrace();
            throw e;
        } catch (ClientProtocolException e) {
            System.out.println("asd e2 "+e.getMessage());
            strResponse = e.getMessage();
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            // e.getCause();
            System.out.println("asd e3 "+e.getMessage());
            e.printStackTrace();
            strResponse = ConstantTransaction.CONNECTION_LOST;
            throw e;
        } catch (Exception e) {
            System.out.println("asd e4 "+e.getMessage());
            e.printStackTrace();
            strResponse = ConstantTransaction.CONNECTION_ERROR;
            throw e;
        } finally {
            return strResponse;
        }
    }


    public String HttpRequestPostCert(String url, String json, int timeout, Context act) throws Exception {
        String strResponse = "";
        try {/*
             *  fix for
             *    Exception in thread "main" javax.net.ssl.SSLHandshakeException:
             *       sun.security.validator.ValidatorException:
             *           PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException:
             *               unable to find valid certification path to requested target
             */

            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        }

                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
//            HostnameVerifier allHostsValid = new HostnameVerifier() {
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            };

            HostnameVerifier allHostsValid = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            /*
             * end of the fix
             */


            URL urls = new URL(url);
            HttpsURLConnection connection = (HttpsURLConnection) urls.openConnection();
            connection.setSSLSocketFactory(sc.getSocketFactory());
            connection.setRequestMethod("POST");
            connection.setConnectTimeout(timeout);
            connection.setRequestProperty("Authorization",Function.encodeBase64("android|"+Function.getdatetime()));

//            InputStream is = connection.getInputStream();
//            System.out.println("is " + is);
            connection.setDoOutput(false);
//            connection.setDoOutput(true);
            DataOutputStream dataOutputStream = new DataOutputStream((connection.getOutputStream()));
            dataOutputStream.writeBytes(json);
            //System.out.println("Send POST Parameter: " + json);
            dataOutputStream.flush();
            dataOutputStream.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line = "";
            StringBuilder responseOutput = new StringBuilder();
            while ((line = br.readLine()) != null) {
                responseOutput.append(line);
            }
            br.close();
            strResponse = responseOutput.toString();
            return strResponse;

        } catch (SecurityException e) {
            e.printStackTrace();
            strResponse = e.getMessage();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            strResponse = e.getMessage();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Pesan Koneksi IOException"+e.getMessage());
            strResponse = e.getMessage();
           // strResponse = Constant.CONNECTION_LOST;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Pesan Koneksi Exception"+e.getMessage());
            strResponse = e.getMessage();
        }
        return strResponse;
    }

    public String HttpRequestPost(String url, String json, int timeout, Context act){

        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            AssetManager assManager = act.getAssets();
            InputStream is = null;
            try {
                is = assManager.open(ssl_crt);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("asd ssa error "+e.getMessage());
            }
            InputStream caInput = new BufferedInputStream(is);
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
            } finally {
                caInput.close();
            }

            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, tmf.getTrustManagers(), null);

            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());

            HostnameVerifier allHostsValid = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    HostnameVerifier hv =
                            HttpsURLConnection.getDefaultHostnameVerifier();
                    hv.verify("bb.bpddiy.co.id_ee", session);
                    return true;
                }
            };

            URL urls = new URL(url);
            HttpsURLConnection urlConnection = (HttpsURLConnection) urls.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Authorization",Function.encodeBase64("android|"+Function.getdatetime()));
            urlConnection.setConnectTimeout(timeout);
            urlConnection.setReadTimeout(timeout);
            urlConnection.setHostnameVerifier(allHostsValid);
            urlConnection.setSSLSocketFactory(context.getSocketFactory());
//            urlConnection.connect();
            urlConnection.setDoOutput(false);
            DataOutputStream dataOutputStream = new DataOutputStream((urlConnection.getOutputStream()));
            dataOutputStream.writeBytes(json);
            //System.out.println("Send POST Parameter: " + json);
            dataOutputStream.flush();
            dataOutputStream.close();

            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line = "";
            StringBuilder responseOutput = new StringBuilder();
            while ((line = br.readLine()) != null) {
                responseOutput.append(line);
            }
            br.close();
            strResponse = responseOutput.toString();
            return strResponse;

        } catch (SecurityException e) {
//            e.printStackTrace();
            System.out.println("asd e1 "+e.getMessage());
            strResponse = ConstantTransaction.CONNECTION_ERROR;
        } catch (ClientProtocolException e) {
//            e.printStackTrace();
            System.out.println("asd e2 "+e.getMessage());
            strResponse = ConstantTransaction.CONNECTION_ERROR;
        } catch (IOException e) {
//            e.printStackTrace();
//            System.out.println("Pesan Koneksi IOException"+e.getMessage());
            System.out.println("asd e3 "+e.getMessage());
            if ((e.getMessage()).equals("Connection timed out")) {
                strResponse = ConstantTransaction.CONNECTION_ERROR;
            }else{
                strResponse = ConstantTransaction.CONNECTION_ERROR;
            }
            // strResponse = Constant.CONNECTION_LOST;
        } catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("Pesan Koneksi Exception"+e.getMessage());
            System.out.println("asd e4 "+e.getMessage());
            strResponse = ConstantTransaction.CONNECTION_ERROR;
        }
        return strResponse;
    }
}
