package com.lakupandai.acs.lakupandaidiy.saldo.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.lakupandai.acs.lakupandaidiy.R;
import com.lakupandai.acs.lakupandaidiy.adapter.MutasiAdapter;
import com.lakupandai.acs.lakupandaidiy.dashboard.view.MenuHasilResponsePulsa;
import com.lakupandai.acs.lakupandaidiy.notifikasian.view.MenuNotifikasi;
import com.lakupandai.acs.lakupandaidiy.saldo.model.Mutasi;
import com.lakupandai.acs.lakupandaidiy.util.Constant;
import com.lakupandai.acs.lakupandaidiy.util.ConstantTransaction;
import com.lakupandai.acs.lakupandaidiy.util.Function;
import com.lakupandai.acs.lakupandaidiy.util.JSONParser;
import com.lakupandai.acs.lakupandaidiy.util.TripleDES;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.lakupandai.acs.lakupandaidiy.util.Constant.jsonB;
import static com.lakupandai.acs.lakupandaidiy.util.Constant.page;
import static com.lakupandai.acs.lakupandaidiy.util.Constant.rc;
import static com.lakupandai.acs.lakupandaidiy.util.Constant.totalPages;

/**
 * Created by Erdy on 12/04/2018.
 */
public class MenuMutasiRekeningDetail extends AppCompatActivity implements MutasiAdapter.MutasiListener {
    private Activity mContext;
    private String noHPAgen,resultEnc, resultfromJson, resultfromJsonDecrypt, rmResult, rcResult, resultDec;
    //    ProgressDialog progressDialog;
    TripleDES tripleDES;
    JSONParser jsonParser = new JSONParser();
    EditText etTanggalAwal, etTanggalAkhir;
    ImageView llHomeMenuMutasiRekening;
    ProgressBar progressBar;
    ArrayList<Mutasi> myListItems = new ArrayList<>();
    MutasiAdapter adapter;
    ListView listView;
    Calendar myCalendar = Calendar.getInstance();
    Calendar myCalendarAkhir = Calendar.getInstance();

    //it will tell us weather to load more items or not
    boolean loadingMore = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_log_transaksi);
        initUI();
    }

    private void initUI() {
        mContext = this;

        llHomeMenuMutasiRekening = findViewById(R.id.llHomeMenuMutasiRekening);
        llHomeMenuMutasiRekening.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        String dateDefault = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        etTanggalAwal = (EditText) findViewById(R.id.etTanggalAwal);
        //etTanggalAwal.setText(dateDefault);
        etTanggalAkhir = (EditText) findViewById(R.id.etTanggalAkhir);
        //etTanggalAkhir.setText(dateDefault);
        listView = (ListView) findViewById(R.id.listMutasi);
        progressBar = findViewById(R.id.progressBarMutasi);

        myListItems = new ArrayList<Mutasi>();
        myListItems.clear();

        //add the footer before adding the adapter, else the footer will not load!
//        View footerView = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.loading_view, null, false);
//        this.listView.addFooterView(footerView);

        //create and adapter
        adapter = new MutasiAdapter(this, android.R.layout.simple_list_item_1, myListItems,this);

        //setting adapter to list view
        listView.setAdapter(adapter);

        //setting  listener on scroll event of the list
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                int lastInScreen = firstVisibleItem + visibleItemCount;
                //is the bottom item visible & not loading more already? Load more!
                if ((lastInScreen == totalItemCount) && !(loadingMore) && page < totalPages && rc == 1 ) {
                    new executeMutasi(mContext,etTanggalAwal.getText().toString(),etTanggalAkhir.getText().toString()).execute();
                }

            }


        });



        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        final DatePickerDialog.OnDateSetListener dateAkhir = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendarAkhir.set(Calendar.YEAR, year);
                myCalendarAkhir.set(Calendar.MONTH, monthOfYear);
                myCalendarAkhir.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabelAkhir();
            }

        };

        etTanggalAwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                final DatePickerDialog dialoga = new DatePickerDialog(MenuMutasiRekeningDetail.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialoga.show();
                dialoga.setButton(DialogInterface.BUTTON_POSITIVE,
                        "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int   day  = dialoga.getDatePicker().getDayOfMonth();
                                int   month= dialoga.getDatePicker().getMonth();
                                int   year = dialoga.getDatePicker().getYear()-1900;

                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                String formatedDate = sdf.format(new Date(year, month, day));
                                etTanggalAwal.setText(formatedDate);

                                adapter.clear();
                                page=0;
                                rc = 1;
                            }
                        });

            }
        });

        etTanggalAwal.addTextChangedListener(new TextWatcher() {
            int len=0;
            @Override
            public void afterTextChanged(Editable s) {
                String str = etTanggalAwal.getText().toString();
                if(str.length()==2&& len <str.length() || str.length()==5&& len <str.length()){//len check for backspace
                    etTanggalAwal.append("-");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

                String str = etTanggalAwal.getText().toString();
                len = str.length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        etTanggalAkhir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                final DatePickerDialog dialogb = new DatePickerDialog(MenuMutasiRekeningDetail.this, dateAkhir, myCalendarAkhir
                        .get(Calendar.YEAR), myCalendarAkhir.get(Calendar.MONTH),
                        myCalendarAkhir.get(Calendar.DAY_OF_MONTH));
                dialogb.show();
                dialogb.setButton(DialogInterface.BUTTON_POSITIVE,
                        "OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int   day  = dialogb.getDatePicker().getDayOfMonth();
                                int   month= dialogb.getDatePicker().getMonth();
                                int   year = dialogb.getDatePicker().getYear()-1900;

                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                String formatedDate = sdf.format(new Date(year, month, day));
                                etTanggalAkhir.setText(formatedDate);

                                adapter.clear();
                                page=0;
                                rc = 1;
                            }
                        });

            }
        });

        etTanggalAkhir.addTextChangedListener(new TextWatcher() {
            int len=0;
            @Override
            public void afterTextChanged(Editable s) {
                String str = etTanggalAkhir.getText().toString();
                if(str.length()==2&& len <str.length() || str.length()==5&& len <str.length()){//len check for backspace
                    etTanggalAkhir.append("-");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

                String str = etTanggalAkhir.getText().toString();
                len = str.length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }

    private Runnable returnRes = new Runnable() {
        @Override
        public void run() {
            //Loop through the new items and add them to the adapter
            if (myListItems != null && myListItems.size() > 0) {

                for (int i = 0; i < myListItems.size(); i++)
                    adapter.add(myListItems.get(i));

            }

            int a = adapter.getCount();
            int b = page;
            int c;
            int d = page*10;
            int e = Constant.totalMutasi;
            if (adapter.getCount()!=page*10) {

                for (a = adapter.getCount(); a > ((b - 1) * 10); a--) {
                    c = ((adapter.getCount())-a)-Constant.sizePerPage;
//                    System.out.println((((b - 1) * 10))+c);
                    try{
                        adapter.remove(adapter.getItem(((b* 10))));
                    }catch (Exception f){}

                }

                for (e = Constant.totalMutasi; e < d; d--) {

                    try{
                        adapter.remove(adapter.getItem(e));
                    }catch (Exception f){

                    }
//                    System.out.println(e+" "+d);
                }

            }
            adapter.notifyDataSetChanged();
            //Done loading more.
            loadingMore = false;
        }



    };

    private void updateLabel() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

        etTanggalAwal.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateLabelAkhir() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

        etTanggalAkhir.setText(sdf.format(myCalendarAkhir.getTime()));
    }

    @Override
    public void onMutasiClicked(String resi) {
        System.out.println("asd resi "+resi);
        new executeGetDetailMutasi(resi).execute();
    }

    class executeMutasi extends AsyncTask<String, JSONObject, String> {
        Context context;
        String tglAwal,tglAkhir;

        private executeMutasi(Context mContext,String tglAwal,String tglAkhir) {
            this.context = mContext;
            this.tglAwal = tglAwal;
            this.tglAkhir = tglAkhir;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            startLoading(true);
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {

                String dateDefault = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                noHPAgen= Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN,mContext);
                json.put(ConstantTransaction.START_DATE, tglAwal);
                json.put(ConstantTransaction.END_DATE, tglAkhir);
                json.put(ConstantTransaction.PAGE_JSON, String.valueOf(page));
                json.put(ConstantTransaction.MSISDN_AGEN_LOGIN, noHPAgen);
                json.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_MUTASI);
                //Log.v(Constant.LOG_LOGIN, Constant.MESSAGE_LOG_LOGIN);
                tripleDES = new TripleDES();
                resultEnc = tripleDES.encrypt(json.toString());
                resultDec = tripleDES.decrypt(resultEnc);
                System.out.println("asd param [mutasi] "+json.toString());
                resultfromJson = jsonParser.HttpRequestPost(ConstantTransaction.URL, resultEnc, ConstantTransaction.TimeOutConnection, MenuMutasiRekeningDetail.this);
//                System.out.println("Result json: "+resultfromJson);
                resultfromJsonDecrypt = tripleDES.decrypt(resultfromJson);
//                System.out.println("RESPONSE DATA: " + resultfromJson);
//                System.out.println("RESPONSE DATA: " + resultfromJsonDecrypt);

                return resultfromJson;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultfromJson;
        }

        @SuppressLint("NewApi")
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            progressDialog.dismiss();
            startLoading(false);
            JSONObject jsonObject;
            resultfromJsonDecrypt = tripleDES.decrypt(result);
            System.out.println("asd hasil mutasi "+resultfromJsonDecrypt);
            if (result.contains(ConstantTransaction.CONNECTION_LOST)) {
                Function.showAlert(mContext, ConstantTransaction.CONNECTION_LOST);
            } else if (result.contains(ConstantTransaction.SERVER_ERROR)) {
                Function.showAlert(mContext, ConstantTransaction.SERVER_ERROR);
            } else if (result.contains(ConstantTransaction.CONNECTION_ERROR)) {
                //SendSms.splitSMS(resultEnc, mContext);
                Function.showAlert(mContext, ConstantTransaction.CONNECTION_ERROR);
            } else if(resultfromJsonDecrypt!=null){
//                System.out.println("rc "+rc);
                System.out.println("Response Receive [MenuMutasiRekeningDetail] "+ resultfromJsonDecrypt.toString());
                if (rc == 1 && !resultfromJsonDecrypt.contains("kosong")){
                    try {

//                        System.out.println("Nilai Result JSONParser Decrypt : " +resultfromJsonDecrypt.replaceAll("\\\\",""));
                        jsonObject = new JSONObject(resultfromJsonDecrypt);
                        rcResult = jsonObject.getString(ConstantTransaction.RC_RESULT);
                        rmResult = jsonObject.getString(ConstantTransaction.RM_RESULT);

                        //lastLogin = jsonObject.getString(Constant.LAST_LOGIN);
                        if (rcResult.contains(ConstantTransaction.SUCCESS_CODE)) {
                            JSONObject jsonA = new JSONObject(rmResult);
                            jsonB = new JSONArray();
                            jsonB = jsonA.getJSONArray("mutasi");
                            Constant.sizePerPage = jsonA.getInt("sizePerPage");
                            totalPages = jsonA.getInt("totalPages");
                            Constant.totalMutasi = jsonA.getInt("totalMutasi");
                            page = jsonA.getInt("page") + 1;
                            rc = jsonA.getInt("rc");
//                        System.out.println("JSONArray : "+ jsonB.toString());
//                        Intent i = new Intent(mContext, MenuMutasiRekeningDetail.class);
//                        startActivity(i);
//                        finish();
                            JSONObject jsonC;
                            double myNumber;
                            String formattedNumber;
                            NumberFormat formatter = new DecimalFormat("#,###");
                            //Get 10 new list items

                            myListItems = new ArrayList<>();
                            for (int a=0;a< jsonB.length(); a++){
                                try {
                                    jsonC = jsonB.getJSONObject(a);
                                    formattedNumber = Function.getRupiah(jsonC.getString("totalAmount"));
                                    myListItems.add(new Mutasi(jsonC.getString("nasabah"), jsonC.getString("crTime"), jsonC.getString("description"), formattedNumber,jsonC.getInt("id"),jsonC.getString("resi")));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            runOnUiThread(returnRes);
                        }else {
                        }
                    } catch (Exception e) {
                        System.out.println("asd exception result [MenuMutasi] "+e.getMessage());
                    }
                }else{
                    if(myListItems.isEmpty()){
                        System.out.println("asd masuk else");
                        Function.showAlert(mContext, ConstantTransaction.EMPTY_TRANSACTION_RESULT);
                    }
                                    }

            }
        }
    }

    private void startLoading(boolean loading){
     if(loading){
         //Toast.makeText(getApplicationContext(),"Mengambil data....",Toast.LENGTH_SHORT).show();
         progressBar.setVisibility(View.VISIBLE);
     }else{
         progressBar.setVisibility(View.INVISIBLE);
     }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    class executeGetDetailMutasi extends AsyncTask<String,Void,String>{
        private String resi;
        public executeGetDetailMutasi(String resi) {
            this.resi = resi;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            final JSONParser jsonParser = new JSONParser();
            final JSONObject jsonParam = new JSONObject();
            try {
                jsonParam.put(ConstantTransaction.MSISDN_AGEN_LOGIN, Function.getValueSharedPref(Constant.My_PREF_MSISDN_AGEN, MenuMutasiRekeningDetail.this));
                jsonParam.put(ConstantTransaction.ACTION, ConstantTransaction.ACTION_GET_DETAIL_TRX);
                jsonParam.put(ConstantTransaction.NO_RESI,resi);
                final TripleDES tripleDES = new TripleDES();
                System.out.println("asd data kirim [Mutasi] "+jsonParam);
                final String encryptedParam = tripleDES.encrypt(jsonParam.toString());
                final String response = jsonParser.HttpRequestPost(ConstantTransaction.URL, encryptedParam, ConstantTransaction.TimeOutConnection, MenuMutasiRekeningDetail.this);
                return response;
            }catch (Exception e){
                System.out.println("asd exception[menu notifikasi] "+e.getMessage());
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.INVISIBLE);
            try {
                result = new TripleDES().decrypt(result);
                System.out.println("asd response detail[menu mutasi] "+result);
                if (!result.isEmpty()) {
                    JSONObject jsonDATA = new JSONObject(result);
                    final JSONObject data = new JSONObject(jsonDATA.getString(ConstantTransaction.RM_RESULT));
                    Constant.layanan_title="Detail Mutasi";
                    Constant.dataResult = data.getJSONArray(ConstantTransaction.DATA);
                    Intent intent = new Intent(MenuMutasiRekeningDetail.this, MenuHasilResponsePulsa.class);
                    intent.putExtra("notif",false);
                    startActivity(intent);
                }
            }catch (Exception e){
                System.out.println("asd exception [MenuNotifikasi]"+e.getMessage());
            }
        }


    }

}
